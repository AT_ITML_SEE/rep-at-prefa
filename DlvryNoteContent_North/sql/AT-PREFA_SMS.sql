/******************************************************************************
NAME: AT-PREFA_SMS.rpt
SUBREPORT: -
DESC: Sending SMS with specific text

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		28.07.2022      DAKORNHA    IMI-2384        created
1.1			08.08.2022		DAKORNHA	IMI-2384		corrected some H�ceks
1.2         23.08.2022      DAKORNHA    IMI-2384        included COID and ACKSHIPDATE in text
1.3         22.05.2023      PBRUGGER    IMI-3400        only for country CZ


******************************************************************************/

select 'Va�e objedn�vka od firmy PREFA byla '
 ||COWORK.COID
 ||' pr�ve pripravena k odesl�n� a bude v�m '
 ||TO_CHAR(COWORK.ACKSHIPDATE, 'DD.MM.YYYY')
 ||'.' AS SMS_TEXT
from COWORK
INNER JOIN PARTY ON PARTY.PARTY_ID = COWORK.SHIPTOCUSID
    AND PARTY.PARTY_PROFILE = COWORK.COMPANY_ID
where COWORK.COID='{?COID_I}'
  AND COWORK.COMPANY_ID='{?COMPANY_ID_I}'
  AND COWORK.COSEQ={?COSEQ_I}
  AND COWORK.COSUBSEQ={?COSUBSEQ_I}
  AND PARTY.COUNTRYCODE = 'CZ'