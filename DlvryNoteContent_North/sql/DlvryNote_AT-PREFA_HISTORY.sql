/* Formatted on 30/09/2022 11:36:49 (QP5 v5.336) */
/******************************************************************************
NAME: DlvryNote_AT-PREFA-HISTORY.rpt
SUBREPORT: -
DESC: 


REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created
1.1         04.10.2022      DAKORNHA    IMI-2551        add NLP qualifier on COFTX
                                                        reformat whole sql
                                                        SUM qty and weight
1.2         13.10.2022      DAKORNHA    INC006084097    remove COSUBSEQ filter
1.3         17.10.2022      DAKORNHA    IMI-2737        replaced lof_get_corowftx_prefa with lof_get_corowftxtrc_prefa
1.4         24.10.2022      DAKORNHA    IMI-2769        NLP function was not available for TRC tables
                                                        Quantity was wrong when load carrier has been rejected in inbound 
                                                        (rcvcarlog.rcvcarstat != '90' -- 90 = Rejected)
1.5         03.11.2022      DAKORNHA    IMI-2588        add parameter as columns for linking of sub-reports
1.6         17.11.2022      DAKORNHA    INC006203423    changed possible RCVCARLOG.RCVCARTSTAT as some CARID stuck in status 10 = Planned

******************************************************************************/

WITH
    coftx_do AS (
        SELECT
            coid
            , coseq
            , company_id
            , text
            , NVL(REGEXP_SUBSTR(LTRIM(text), '^[^###]*'), '') AS aviso1
            , NVL(REPLACE(REGEXP_SUBSTR(LTRIM(text), '[^###]+', 1, 2), '###'), '') AS aviso2
            , NVL(REPLACE(REGEXP_SUBSTR(LTRIM(text), '[^###]+', 1, 3), '###'), '') AS aviso3
            , NVL(REPLACE(REGEXP_SUBSTR(LTRIM(text), '[^###]+', 1, 4), '###'), '') AS aviso4
            FROM
                coftxtrc
            WHERE
                ftx_qualifier = 'DO'
                AND company_id = 'AT-PREFA'
    )           
SELECT
    co.coid || '-' || co.coseq AS coid
    , art.artid
    , art.artname1
    , art.artname2
    , co.coseq
    , co.cosubseq
    , SUBSTR(co.coid, 4) AS auftrag2
    , NVL(pbrow.copos, corow.copos) AS copos
    , SUM(
        DECODE(PBROW.PAKID
            /*  PAKID = 'Rol' */
            , 'Rol', NVL(
                        CASE 
                            WHEN PBROW.PBTYPE = 'P'
                            THEN PBROW.PICKQTY
                            WHEN PBROW.PBTYPE = 'C'
                            THEN PBROW.PICKQTY
                        END
                        , 0)
            /*  PAKID != 'Rol' */
            , NVL(
                CASE 
                    WHEN PBROW.PBTYPE = 'P'
                    THEN PBROW.PICKQTY  
                    WHEN PBROW.PBTYPE = 'C'
                    THEN PBROW.PICKQTY
                END
                , 0 ) * TO_NUMBER(corowftxAMT.TEXT,'9999.999999')
        )
    ) AS pickqty
    , SUM(
        DECODE(PBROW.PAKID
            /*  PAKID = 'Rol' */
            , 'Rol', NVL(
                        CASE 
                            WHEN PBROW.PBTYPE = 'P'
                            THEN PBROW.ORDQTY  
                            WHEN PBROW.PBTYPE = 'C'
                            THEN PBROW.ORDQTY
                        END
                        , 0)
            /*  PAKID != 'Rol' */
            , NVL(
                CASE 
                    WHEN PBROW.PBTYPE = 'P'
                    THEN PBROW.ORDQTY    
                    WHEN PBROW.PBTYPE = 'C'
                    THEN PBROW.ORDQTY 
                END
                , COROW.ORDQTY) * TO_NUMBER(corowftxAMT.TEXT,'9999.999999')
        ) 
    ) AS ordqty
    , SUM(
        NVL(
            DECODE(PBROW.PAKID
                /*  PAKID = 'Rol' */
                , 'Rol', CASE 
                            WHEN PBROW.PBTYPE = 'C'
                            THEN RCVCARLOG.TOTWGT * PBROW.PICKQTY
                            ELSE RCVCARLOG.TOTWGT
                        END
                /*  PAKID != 'Rol' */
                , CASE 
                    WHEN PBROW.PBTYPE = 'P'
                    THEN PBROW.WEIGHT    
                    WHEN PBROW.PBTYPE = 'C'
                    THEN PBROW.WEIGHT * PBROW.PICKQTY 
                END)
            , 0
        )
    ) AS weight,   
    TO_NUMBER(corowftxAMT.TEXT,'9999.999999') AS amt,
    DECODE(PBROW.PAKID
        /*  PAKID = 'Rol' */
        , 'Rol', PBROW.PAKID
        /*  PAKID != 'Rol' */
        ,corowftxUNL.TEXT
    ) AS unl
    , SUBSTR(party_id, 1, INSTR(party_id, '_', 1) - 1) AS party_id
    , party.name1
    , party.adr1
    , party.adr2
    , party.phone
    , party.postcode
    , party.city
    , CASE
        WHEN party.countrycode = 'AT' 
        THEN '�sterreich'
        ELSE party.countrycode
    END AS countrycode
    , '' AS Abs_Konto
    , 'Prefa Aluminiumprodukte GmbH'    AS Abs_Name1
    , ''                                AS Abs_Name2
    , 'Lamarrstrasse 2'                 AS Abs_Stra�e
    , '3151'                            AS Abs_Plz
    , 'St. Georgen'                     AS Abs_Ort
    , '�sterreich'                      AS Abs_Land
    , ''                                AS Abs_Referenz
    , corow.fpshipdtm
    , CASE
        WHEN co.terms_of_delivery LIKE 'DAP%' 
        THEN 'DAP '|| party.city
        WHEN co.terms_of_delivery LIKE 'DDU%' 
        THEN 'DDU '|| party.city
        ELSE SUBSTR(co.terms_of_delivery, 1, 3)||' '||party.city
    END AS terms_of_delivery
    , artcod.artcode
    , CASE
        WHEN co.dlvrymeth_id = 'AT-PREFA-PICKUP' 
        THEN NULL
        WHEN co.dlvrymeth_id = 'AT-PREFA' 
        THEN 'Lieferung Schenker'
        ELSE co.dlvrymeth_id
    END AS speditur
    , NVL(loc_ps_language_at_prefa.lngcode, 'ENG')                              AS lngcode
    , NVL(loc_ps_language_at_prefa.customer_adress, 'GB')                       AS customer_adress
    , NVL(loc_ps_language_at_prefa.reprint, 'Reprint')                          AS reprint
    , NVL(loc_ps_language_at_prefa.page, 'Page')                                AS page
    , NVL(loc_ps_language_at_prefa.orignal, 'Orignal')                          AS orignal
    , NVL(loc_ps_language_at_prefa.delivery_note_no, 'Delivery note no.')       AS delivery_note_no
    , NVL(loc_ps_language_at_prefa.shiped_to, 'Shipped To')                     AS shiped_to
    , NVL(loc_ps_language_at_prefa.shipment_from, 'Shipment From')              AS shipment_from
    , NVL(loc_ps_language_at_prefa.aviso, 'Aviso')                              AS aviso_h
    , NVL(loc_ps_language_at_prefa.date1, 'Date')                               AS date1
    , NVL(loc_ps_language_at_prefa.forwarding_agent, 'Carrier/LSP')             AS forwarding_agent
    , NVL(loc_ps_language_at_prefa.customer_order, 'Customer Order')            AS customer_order
    , NVL(loc_ps_language_at_prefa.customer_order_pos
        , 'Line Order Information')                                         AS customer_order_pos
    , NVL(loc_ps_language_at_prefa.weight, 'Weight')                            AS weight_h
    , NVL(loc_ps_language_at_prefa.comm_code, 'CommCode')                       AS comm_code
    , NVL(loc_ps_language_at_prefa.quantity_shipped, 'Qty Shipped')             AS quantity_shipped
    , NVL(loc_ps_language_at_prefa.quantity_ordered, 'Qty Ordered')             AS quantity_ordered
    , NVL(loc_ps_language_at_prefa.unit, 'Unit')                                AS unit
    , NVL(loc_ps_language_at_prefa.delivery_terms, 'Delivery terms')            AS delivery_terms
    , NVL(loc_ps_language_at_prefa.inspection, 'Inspection')                    AS inspection
    , NVL(loc_ps_language_at_prefa.goods_accepted_in_order
        , 'Goods accepted in order')                                    AS goods_accepted_in_order
    , NVL(loc_ps_language_at_prefa.printed_name, 'Printed Name')                AS printed_name
    , NVL(loc_ps_language_at_prefa.rf_text1, '') AS rf_text1
    , NVL(loc_ps_language_at_prefa.rf_text2, '') AS rf_text2
    , NVL(loc_ps_language_at_prefa.rf_text3, '') AS rf_text3
    , NVL(loc_ps_language_at_prefa.rf_text4, '') AS rf_text4
    , NVL(loc_ps_language_at_prefa.rf_text5, '') AS rf_text5
    , NVL(loc_ps_language_at_prefa.pf_text1, '') AS pf_text1
    , NVL(loc_ps_language_at_prefa.pf_text2, '') AS pf_text2
    , NVL(loc_ps_language_at_prefa.pf_text3, '') AS pf_text3
    , NVL(loc_ps_language_at_prefa.pf_text4, '') AS pf_text4
    , NVL(loc_ps_language_at_prefa.items_on_backorder, '') AS items_on_backorder
    , SUBSTR(co.proid, LENGTH(co.proid) - 4) AS proid
    , deptrp.shipwsid || ' - ' || deptrp.shipwpadr AS ShipAreaAndAddress
    , MAX(
        SUBSTR(
            lof_get_corowftxtrc_prefa(
                company_id_i => 'AT-PREFA'
                , co_i => co.coid
                , coseq_i => co.coseq
                , copos_i => corow.copos
                , cosubpos_i => corow.cosubpos
                , ftx_qualifier_i => 'DO'
            )
            , 1
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
                , '###'
                , 1
                , 1
            ) - 1
        )
    ) AS short_text
    ,  MAX(
        SUBSTR(
            lof_get_corowftxtrc_prefa(
                company_id_i => 'AT-PREFA'
                , co_i => co.coid
                , coseq_i => co.coseq
                , copos_i => corow.copos
                , cosubpos_i => corow.cosubpos
                , ftx_qualifier_i => 'DO'
            )
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
                , '###'
                , 1
                , 1
            ) + 3
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
                , '###'
                , 1
                , 2
            ) - INSTR(
                    lof_get_corowftxtrc_prefa(
                        company_id_i => 'AT-PREFA'
                        , co_i => co.coid
                        , coseq_i => co.coseq
                        , copos_i => corow.copos
                        , cosubpos_i => corow.cosubpos
                        , ftx_qualifier_i => 'DO'
                    )
                    , '###'
                    , 1
            , 1) - 3
        )
    ) AS short1
    ,  MAX(
        SUBSTR(
            lof_get_corowftxtrc_prefa(
                company_id_i => 'AT-PREFA'
                , co_i => co.coid
                , coseq_i => co.coseq
                , copos_i => corow.copos
                , cosubpos_i => corow.cosubpos
                , ftx_qualifier_i => 'DO'
            )
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
                , '###'
                , 1
                , 2) + 3
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
            , '###'
            , 1
            , 3) - INSTR(
                    lof_get_corowftxtrc_prefa(company_id_i => 'AT-PREFA'
                        , co_i => co.coid
                        , coseq_i => co.coseq
                        , copos_i => corow.copos
                        , cosubpos_i => corow.cosubpos
                        , ftx_qualifier_i => 'DO'
                    )
                    , '###'
                    , 1
                    , 2
            ) - 3
        )
    ) AS short2
    ,  MAX(
        SUBSTR(
            lof_get_corowftxtrc_prefa(
                company_id_i => 'AT-PREFA'
                , co_i => co.coid
                , coseq_i => co.coseq
                , copos_i => corow.copos
                , cosubpos_i => corow.cosubpos
                , ftx_qualifier_i => 'DO'
            )
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
            , '###'
            , 1
            , 3) + 3
            , INSTR(
                lof_get_corowftxtrc_prefa(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , copos_i => corow.copos
                    , cosubpos_i => corow.cosubpos
                    , ftx_qualifier_i => 'DO'
                )
                , '###'
                , 1
                , 4) - INSTR(
                            lof_get_corowftxtrc_prefa(
                                company_id_i => 'AT-PREFA'
                                , co_i => co.coid
                                , coseq_i => co.coseq
                                , copos_i => corow.copos
                                , cosubpos_i => corow.cosubpos
                                , ftx_qualifier_i => 'DO'
                            )
                            , '###'
                            , 1
                            , 3
                        ) - 3
        )
    ) AS short3
    , coftx_do.aviso1
    , coftx_do.aviso2
    , coftx_do.aviso3
    , coftx_do.aviso4
    , TO_CHAR(co.dlvrynote_prtdtm, 'YYYY/MM/DD') AS "Print Date"
    , REPLACE(
        REPLACE(lof_get_coftxtrc(
                    company_id_i => 'AT-PREFA'
                    , co_i => co.coid
                    , coseq_i => co.coseq
                    , ftx_qualifier_i => 'NLP'
                    , default_txt_i => NULL)
                    , '###'
                    , chr(13) -- chr(13) = carriage return
                    )
        , '|'
        , chr(9)||chr(9)||chr(9)||chr(9) -- chr(9) = tabstop
    ) NLP
    , co.coid           AS param_coid
    , co.company_id     AS param_company_id
    , co.coseq          AS param_coseq          
    , co.cosubseq       AS param_cosubseq
FROM consignmenttrc consignment
INNER JOIN cotrc co ON
    consignment.coid = co.coid
    AND consignment.coseq = co.coseq
    AND consignment.company_id = co.company_id
INNER JOIN corowtrc corow ON
    co.coid = corow.coid
    AND co.company_id = corow.company_id
    AND co.coseq = corow.coseq
    AND co.cosubseq = corow.cosubseq
INNER JOIN party ON
    co.shiptopartyid = party.party_id
    AND co.shiptopartyqualifier = party.party_qualifier
    AND co.company_id = party.company_id
INNER JOIN art ON
    corow.artid = art.artid
    AND corow.company_id = art.company_id
LEFT JOIN pbrowlog pbrow ON
    corow.coid = pbrow.coid
    AND corow.company_id = pbrow.company_id
    AND corow.copos = pbrow.copos
    AND corow.coseq = pbrow.coseq
    AND corow.cosubpos = pbrow.cosubpos
    AND corow.cosubseq = pbrow.cosubseq
    AND corow.whid = pbrow.whid
INNER JOIN artcod ON
    corow.artid = artcod.artid
    AND corow.company_id = artcod.company_id
LEFT JOIN deplog dep ON 
    co.whid = dep.whid
    AND co.departure_id = dep.departure_id
LEFT JOIN deptrplog deptrp ON 
  dep.whid = deptrp.whid
   AND dep.departure_id = deptrp.departure_id
   AND deptrp.shipwsid IS NOT NULL
LEFT JOIN corowftxtrc corowftxamt ON
    corow.coid = corowftxamt.coid
    AND corow.coseq = corowftxamt.coseq
    AND corow.copos = corowftxamt.copos
    AND corow.cosubpos = corowftxamt.cosubpos
    AND corow.company_id = corowftxamt.company_id
    AND corowftxamt.ftx_qualifier = 'AMT'
LEFT JOIN corowftxtrc corowftxunl ON
    corow.coid = corowftxunl.coid
    AND corow.coseq = corowftxunl.coseq
    AND corow.copos = corowftxunl.copos
    AND corow.cosubpos = corowftxunl.cosubpos
    AND corow.company_id = corowftxunl.company_id
    AND corowftxunl.ftx_qualifier = 'UNL'
LEFT JOIN loc_ps_language_at_prefa ON
    party.countrycode = loc_ps_language_at_prefa.customer_adress
LEFT JOIN rcvcarlog ON
    pbrow.iteid = rcvcarlog.carid
    AND pbrow.whid = rcvcarlog.whid
    AND pbrow.company_id = rcvcarlog.company_id
    AND rcvcarlog.rcvcarstat in ('40', '50') -- 40 = Received, 50 = Processed
LEFT JOIN coftx_do ON
    co.coid = coftx_do.coid
    AND co.coseq = coftx_do.coseq
    AND co.company_id = coftx_do.company_id
WHERE
    party.party_qualifier = 'CU'
    AND artcod.olacod = 'HSC' 
    AND co.COID='{?COID_I}'
    AND co.COMPANY_ID='{?COMPANY_ID_I}'
    AND co.COSEQ={?COSEQ_I}
    AND co.COSUBSEQ={?COSUBSEQ_I}
    /* Parameter for tests and analyzes  */
    /*  AND co.COID = '01-305048302-01' 
        AND co.COMPANY_ID='AT-PREFA' 
        AND co.COSEQ = 1 */
GROUP BY
    co.coid
    , co.coseq
    , co.cosubseq
    , corow.copos
    , corow.cosubpos
    , art.artid
    , art.artname1
    , art.artname2
    , SUBSTR(co.coid, 4)
    , NVL(pbrow.copos, corow.copos)
    , TO_NUMBER(corowftxamt.text, '9999.999999')
    , DECODE(pbrow.pakid
        /* WHEN pbrow.pakid = 'Rol' */
        , 'Rol', pbrow.pakid
        /* WHEN pbrow.pakid != 'Rol' */
        , corowftxunl.text
    )
    , SUBSTR(party_id, 1, INSTR(party_id, '_', 1) - 1)
    , party.name1
    , party.adr1
    , party.adr2
    , party.phone
    , party.postcode
    , party.city
    , CASE
        WHEN party.countrycode = 'AT' 
        THEN '�sterreich'
        ELSE party.countrycode
    END
    , ''
    , 'Prefa Aluminiumprodukte GmbH'
    , ''
    , 'Lamarrstrasse 2'
    , '3151'
    , 'St. Georgen'
    , '�sterreich'
    , ''
    , corow.fpshipdtm
    , CASE
        WHEN co.terms_of_delivery LIKE 'DAP%' 
        THEN 'DAP '|| party.city
        WHEN co.terms_of_delivery LIKE 'DDU%' 
        THEN 'DDU '|| party.city
        ELSE SUBSTR(co.terms_of_delivery, 1, 3)||' '||party.city
    END
    , artcod.artcode
    , CASE
        WHEN co.dlvrymeth_id = 'AT-PREFA-PICKUP' 
        THEN NULL
        WHEN co.dlvrymeth_id = 'AT-PREFA' 
        THEN 'Lieferung Schenker'
        ELSE co.dlvrymeth_id
    END
    , NVL(loc_ps_language_at_prefa.lngcode, 'ENG')
    , NVL(loc_ps_language_at_prefa.customer_adress, 'GB')
    , NVL(loc_ps_language_at_prefa.reprint, 'Reprint')
    , NVL(loc_ps_language_at_prefa.page, 'Page')
    , NVL(loc_ps_language_at_prefa.orignal, 'Orignal')
    , NVL(loc_ps_language_at_prefa.delivery_note_no, 'Delivery note no.')
    , NVL(loc_ps_language_at_prefa.shiped_to, 'Shipped To')
    , NVL(loc_ps_language_at_prefa.shipment_from, 'Shipment From')
    , NVL(loc_ps_language_at_prefa.aviso, 'Aviso')
    , NVL(loc_ps_language_at_prefa.date1, 'Date')
    , NVL(loc_ps_language_at_prefa.forwarding_agent, 'Carrier/LSP')
    , NVL(loc_ps_language_at_prefa.customer_order, 'Customer Order')
    , NVL(loc_ps_language_at_prefa.customer_order_pos, 'Line Order Information')
    , NVL(loc_ps_language_at_prefa.weight, 'Weight')
    , NVL(loc_ps_language_at_prefa.comm_code, 'CommCode')
    , NVL(loc_ps_language_at_prefa.quantity_shipped, 'Qty Shipped')
    , NVL(loc_ps_language_at_prefa.quantity_ordered, 'Qty Ordered')
    , NVL(loc_ps_language_at_prefa.unit, 'Unit')
    , NVL(loc_ps_language_at_prefa.delivery_terms, 'Delivery terms')
    , NVL(loc_ps_language_at_prefa.inspection, 'Inspection')
    , NVL(loc_ps_language_at_prefa.goods_accepted_in_order, 'Goods accepted in order')
    , NVL(loc_ps_language_at_prefa.printed_name, 'Printed Name')
    , NVL(loc_ps_language_at_prefa.rf_text1, '')
    , NVL(loc_ps_language_at_prefa.rf_text2, '')
    , NVL(loc_ps_language_at_prefa.rf_text3, '')
    , NVL(loc_ps_language_at_prefa.rf_text4, '')
    , NVL(loc_ps_language_at_prefa.rf_text5, '')
    , NVL(loc_ps_language_at_prefa.pf_text1, '')
    , NVL(loc_ps_language_at_prefa.pf_text2, '')
    , NVL(loc_ps_language_at_prefa.pf_text3, '')
    , NVL(loc_ps_language_at_prefa.pf_text4, '')
    , NVL(loc_ps_language_at_prefa.items_on_backorder, '')
    , SUBSTR(co.proid, LENGTH(co.proid) - 4)
    , deptrp.shipwsid || ' - ' || deptrp.shipwpadr
    , coftx_do.aviso1
    , coftx_do.aviso2
    , coftx_do.aviso3
    , coftx_do.aviso4
    , TO_CHAR(co.dlvrynote_prtdtm, 'YYYY/MM/DD')
    , co.coid           
    , co.company_id     
    , co.coseq                  
    , co.cosubseq   
ORDER BY
    NVL(pbrow.copos, corow.copos)
    
/******************************************************************************
NAME: DlvryNote_AT-PREFA-HISTORY.rpt
SUBREPORT: LOAD_CARRIERS
DESC: 


REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.4         31.10.2022      DAKORNHA    IMI-2588        add load carriers

******************************************************************************/
   
SELECT  
    cartyp.cartypid
    , cartyp.carname
    , olacodnl.olacodtxt
    , COUNT(
        DISTINCT( 
            COALESCE(pbcar.merge_to_carid, pbcar.consolidation_to_carid, pbcar.carid)
            )
    ) AS num_carid
FROM pbrowlog pbrow
INNER JOIN party ON
    pbrow.shiptopartyid = party.party_id
    AND pbrow.shiptopartyqualifier = party.party_qualifier
INNER JOIN pbcarlog pbcar ON
    pbrow.pbcarid = pbcar.pbcarid
INNER JOIN cartrc car ON
    COALESCE(pbcar.merge_to_carid, pbcar.consolidation_to_carid, pbcar.carid) = car.carid
INNER JOIN cartyp ON
    car.cartypid = cartyp.cartypid
LEFT JOIN loc_countrycode_to_langcod ON
    party.COUNTRYCODE = loc_countrycode_to_langcod.COUNTRYCODE
    AND party.company_id = loc_countrycode_to_langcod.company_id
LEFT JOIN olacodnl ON
    cartyp.cartypid = olacodnl.olacod
    AND NVL(loc_countrycode_to_langcod.langcode, 'ENU') = olacodnl.nlangcod
WHERE 
    olacodnl.olaid = 'CARTYPID'
    AND party.party_qualifier = 'CU'
    AND pbrow.coid = '{?COID_I}'
    AND pbrow.company_id = '{?COMPANY_ID_I}'
    AND pbrow.coseq = {?COSEQ_I}
    AND pbrow.cosubseq = {?COSUBSEQ_I}
GROUP BY
    cartyp.cartypid
    , cartyp.carname
    , olacodnl.olacodtxt