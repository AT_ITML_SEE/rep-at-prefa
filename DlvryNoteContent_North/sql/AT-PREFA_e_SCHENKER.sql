/* Formatted on 22/11/2022 10:12:25 (QP5 v5.381) */
 /******************************************************************************
NAME: AT-PREFA_e_SCHENKER.rpt
SUBREPORT: -
DESC: eSchenker Report AT-PREFA

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		04.10.2021 		AQEASLAM     PREFA-27       created
1.1         25.05.2022      DAKORNHA                    changed WHERE - AND COWORK.COSUBSEQ to AND CONS.COSUBSEQ
1.2         11.07.2022      DAKORNHA     IMI-2375       change weight calculcation - same as on CMR and Loading Report
1.3         10.08.2022      AQEASLAM     IMI-2460       Added check (AND PBCAR.PBCARSTAT IN('C''L')) somebody has removed thats why report shows PBCARSTAT S = Consolidate also.
1.4         31.08.2022      DAKORNHA     IMI-2519       weight corrected from NETTO to BRUTTO (CAR.TOTWGT)
1.5         12.07.2022      DAHRUSKA     IMI-2320       change to one file per departure - parameter change to departure_id of consignment
1.6         25.11.2022      DAKORNHA     IMI-2320       weight corrected - same as on PS
1.7         22.12.2022      DAKORNHA     IMI-2320       corrected weight calculation to be the same on PS, CPL, eSchenker and Loading Report
1.8         29.12.2022      DAKORNHA     IMI-2320       weight calculation moved to Common Table Expression
1.9         11.01.2023      DAKORNHA     IMI-2345       exclude AT-PREFA-GLS method of shipments
2.0         08.03.2023      PBRUGGER     IMI-2320       added function to determine brutto/netto wgt
2.1         16.03.2023      PBRUGGER     IMI-2320       removed not necessary sql parts
2.2         16.03.2023      PBRUGGER     IMI-2320       added support for log tables                                       
******************************************************************************/

WITH 
    cu AS (
        SELECT 
            companywh.whid
            , companywh.company_id
            , party.party_id
            , party.name1
            , party.name2
            , party.name3
            , party.name4
            , party.name5
            , party.adr1
            , party.adr2
            , party.postcode
            , party.city
            , party.countrycode
            , party.country
            , party.phone
            , party.email
        FROM party party
        INNER JOIN companywh companywh ON 
            companywh.company_id = party.company_id
        WHERE party.party_qualifier = 'CU'
    )
    , consignment_a AS (
        SELECT 
         cons.departure_id
         , cons.whid
         , cons.consignment_id
         , cons.company_id
         , cons.shiptopartyid
         , cons.coid
         , cons.coseq
         , cons.terms_of_delivery
        FROM   consignment cons
        WHERE  cons.company_id = 'AT-PREFA'
        AND cons.departure_id = '{?DEPARTURE_ID}'
        UNION
        SELECT cons.departure_id
             , cons.whid
             , cons.consignment_id
             , cons.company_id
             , cons.shiptopartyid
             , cons.coid
             , cons.coseq
             , cons.terms_of_delivery
        FROM   consignmenttrc cons
        WHERE  cons.company_id = 'AT-PREFA'
        AND cons.departure_id = '{?DEPARTURE_ID}'
    )
    , dep_a AS (
        SELECT dep.departure_id, dep.whid, dep.dlvrymeth_id, dep.departure_dtm
        FROM   deplog dep
        WHERE  dep.whid = 'PRE1'
        AND dep.departure_id = '{?DEPARTURE_ID}'
        UNION
        SELECT dep.departure_id, dep.whid, dep.dlvrymeth_id, dep.departure_dtm
        FROM   dep
        WHERE  dep.whid = 'PRE1'
        AND dep.departure_id = '{?DEPARTURE_ID}'
    )
    , pbcar_a AS (
        SELECT pbcar.consignment_id
         , pbcar.whid
         , pbcar.company_id
         , pbcar.pbcarid
         , pbcar.consolidation_to_carid
         , pbcar.carid
        FROM   pbcar pbcar
        WHERE  whid = 'PRE1'
        UNION
        SELECT pbcar.consignment_id
         , pbcar.whid
         , pbcar.company_id
         , pbcar.pbcarid
         , pbcar.consolidation_to_carid
         , pbcar.carid
        FROM   pbcarlog pbcar
        WHERE  whid = 'PRE1'
    )
    , car_a AS (
        SELECT car.company_id
         , car.departure_id
         , car.whid
         , car.carid
         , car.carcarid
         , car.cartypid
         , car.user_length
         , car.user_width
         , car.user_height
        FROM   car
        WHERE  car.whid = 'PRE1'
        AND    car.company_id = 'AT-PREFA'
        AND    car.departure_id = '{?DEPARTURE_ID}'
        UNION
        SELECT car.company_id
             , car.departure_id
             , car.whid
             , car.carid
             , car.carcarid
             , car.cartypid
             , car.user_length
             , car.user_width
             , car.user_height
        FROM   cartrc car
        WHERE  car.whid = 'PRE1'
        AND    car.company_id = 'AT-PREFA'
        AND    car.departure_id = '{?DEPARTURE_ID}'
    )
    , coftx_a AS (
    SELECT coftx.company_id, coftx.coid, coftx.coseq, coftx.ftx_qualifier, coftx.text
    FROM   coftx
    WHERE  coftx.company_id = 'AT-PREFA'
    AND    coftx.ftx_qualifier = 'TRA'
    UNION
    SELECT coftx.company_id, coftx.coid, coftx.coseq, coftx.ftx_qualifier, coftx.text
    FROM   coftxtrc coftx
    WHERE  coftx.company_id = 'AT-PREFA'
    AND    coftx.ftx_qualifier = 'TRA'
    )
SELECT
    -- SENDER
    '999999999' AS Abs_Konto
    , 'PREFA Aluminiumprodukte GmbH' AS Abs_Name1
    , '' AS Abs_Name2
    , 'Werkstra�e 1' AS Abs_Stra�e
    , '3182' AS Abs_Plz
    , 'Marktl/Lilienfeld' AS Abs_Ort
    , 'AT' AS Abs_Land
    -- Workaround as long CONSIGNMENTS without COID and COSUBSEQ exist in IMI
    , cons.coid ||'-' || cons.coseq AS Abs_Referenz
    -- SHIP-TO
    , '' AS Empf_Konto
    , cu.name1 AS Empf_Name1
    , cu.name2 AS Empf_Name2
    , cu.adr1 AS Empf_Stra�e
    , cu.postcode AS Empf_Plz
    , cu.city AS Empf_Ort
    , cu.countrycode AS Empf_Land
    , dep.departure_id AS Empf_Referenz
    -- SHIP-FROM
    , 'PREFA Aluminiumprodukte GmbH' AS Abhol_Name1
    , '' AS Abhol_Name2
    , 'Lamarrstrasse 2' AS Abhol_Stra�e
    , '3151' AS Abhol_Plz
    , 'St. Georgen' AS Abhol_Ort
    , 'AT' AS Abhol_Land
    , cons.consignment_id AS Rechnungsempfaenger
    , TO_CHAR(dep.departure_dtm, 'YYYY-MM-DD') AS Abholdatum
    , TO_CHAR(dep.departure_dtm, 'HH24MM') AS AbholUhrzeit
    , '' AS Zustelldatum
    , '' AS ZustellUhrzeit
    , '' AS Produkte
    , '' AS Zollgut
    , CASE 
        WHEN cons.terms_of_delivery IS NULL
        THEN 'XXX'
        ELSE SUBSTR(cons.terms_of_delivery, 1, 3)
    END AS Incoterms
    , '' AS Lademeter
    , '' AS Warenwert
    , '' AS WaehrungWarenwert
    , '' AS Nachnahme
    , '' AS WaehrungNachnahme
    , '' AS CBM
    , CASE 
        WHEN coftx.ftx_qualifier = 'TRA'
        THEN REPLACE(coftx.text, ';', '')
        ELSE NULL
    END AS Vorschriften
    , cartyp.cartypid2  AS "Signo/Markierung"
    , '' AS "Anzahl Packst�cke"
    , '' AS "Art Packst�cke"
    , COUNT(DISTINCT NVL(pbcar.consolidation_to_carid, pbcar.carid)||car.cartypid) AS "Anzahl Lademittel"
    , CASE 
        WHEN cartyp.cartypid2 = 'G01' THEN 'BY'
        WHEN cartyp.cartypid2 = 'G02' THEN 'BY'
        WHEN cartyp.cartypid2 = 'G03' THEN 'BY'
        WHEN cartyp.cartypid2 = 'G04' THEN 'BY'
        WHEN cartyp.cartypid2 = 'G05' 
            AND cartyp.cartypid = 'X07' THEN 'BY'
        WHEN cartyp.cartypid2 = 'G05' 
            AND cartyp.cartypid = 'X05' THEN 'OP'
        WHEN cartyp.cartypid2 = 'EWP' THEN 'XP'
        WHEN cartyp.cartypid2 = 'BD3' THEN 'CO'
        WHEN cartyp.cartypid2 = 'BD6' THEN 'CO'
        WHEN cartyp.cartypid2 = 'KRT' THEN 'CT'
        WHEN cartyp.cartypid2 = 'KWA' THEN 'CT'
        WHEN cartyp.cartypid2 = 'WAMAS' THEN 'CT'
        WHEN cartyp.cartypid2 = 'K03' THEN 'CO'
        WHEN cartyp.cartypid2 = 'K04' THEN 'CO'
        ELSE 'CO'
    END AS "Art Lademittel"
    , 'Aluminiumprodukte' AS Inhalt
    , LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'Y',DEP.DEPARTURE_ID) AS Gewicht
    , CASE 
        WHEN car.user_length IS NULL
        THEN cartyp.LENGTH
        ELSE car.user_length
    END AS L
    , CASE 
        WHEN car.user_width IS NULL
        THEN cartyp.width
        ELSE car.user_width
    END AS B
    , CASE 
        WHEN car.user_height IS NULL
        THEN cartyp.height
        ELSE car.user_height
    END AS H
    , LPAD(NVL(pbcar.consolidation_to_carid, pbcar.carid), 20, 0) AS "Barcode Packstuecke"
    , '' AS Tarifart
    , '' AS Stapelbar
    , '' AS Selbstanlieferung
    , '' AS UNDGNumber
    , '' AS HazardLabelNumber1
    , '' AS HazardLabelNumber2
    , '' AS HazardLabelNumber3
    , '' AS ClassCode
    , '' AS DGGoodsText1
    , '' AS DGGoodsText2
    , '' AS NumberOfPackages
    , '' AS UNPackagingCode
    , '' AS UNVerpArt
    , '' AS netQuantity
    , '' AS grossWeight
    , '' AS netExplosiveQuantity
    , '' AS ExpectedQuantity
    , '' AS WasteID
    , '' AS EMSNumber
    , '' AS TunnelRestrCode
    , cu.phone AS Telefonnummer
    , REPLACE(cu.email, ';', '') AS "E-Mail"
FROM consignment_a cons
INNER JOIN dep_a dep ON
    cons.departure_id = dep.departure_id
    AND cons.whid = dep.whid
INNER JOIN pbcar_a pbcar ON
    cons.consignment_id = pbcar.consignment_id
    AND cons.whid = pbcar.whid
    AND cons.company_id = pbcar.company_id
--LEFT JOIN pbrow ON
--    pbcar.pbcarid = pbrow.pbcarid
INNER JOIN car_a car ON
    NVL(pbcar.consolidation_to_carid, pbcar.carid) = car.carid
    AND pbcar.whid = car.whid
    AND pbcar.company_id = car.company_id
    AND car.carcarid is null
    AND car.departure_id = dep.departure_id
LEFT JOIN cartyp cartyp ON
    cartyp.cartypid = car.cartypid
LEFT JOIN cu cu ON
    cons.whid = cu.whid
    AND cons.company_id = cu.company_id
    AND cons.shiptopartyid = cu.party_id
LEFT OUTER JOIN coftx_a coftx ON
    cons.coid  = coftx.coid
    AND cons.coseq = coftx.coseq
    AND cons.company_id = coftx.company_id
    AND coftx.ftx_qualifier = 'TRA'
WHERE cons.company_id = 'AT-PREFA'
  AND dep.dlvrymeth_id NOT IN ('AT-PREFA-PICKUP', 'AT-PREFA-GLS')
  AND cons.departure_id = '{?DEPARTURE_ID}'
GROUP BY
    cartyp.cartypid2
    , cartyp.cartypid
    , car.cartypid
    , cartyp.weight
    , car.user_length
    , cartyp.LENGTH
    , car.user_width
    , cartyp.width
    , car.user_height
    , cartyp.height
    , cu.name1
    , cu.name2
    , cu.adr1
    , cu.postcode
    , cu.city
    , cu.countrycode
    , cu.phone
    , cu.email
    , cons.coid
    , cons.coseq
    , dep.departure_dtm
    , cons.terms_of_delivery
    , coftx.text
    , coftx.ftx_qualifier
    , dep.departure_id
    , cons.consignment_id
    , NVL(pbcar.consolidation_to_carid, pbcar.carid)
ORDER BY 
    cons.coid
    , car.cartypid
