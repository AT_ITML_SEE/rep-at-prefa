/******************************************************************************
NAME: ITELBL_AT-PREFA.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT 
    ITE.ITEID, 
    CAR.CARCARID,
    ITE.COMPANY_ID, 
    ITE.ASNINID,
    ITE.POID, 
    ITE.POPOS, 
    ITE.ARRDATE, 
    ITE.BLOCKCOD, 
    ITE.CREATEEMPID, 
    ITE.ARTID, 
    ITE.ARTNAME, 
    ITE.STORQTY, 
    CAR.WSID, 
    CAR.WPADR, 
    ITE.OWNID, 
    ART.WWS_ENHANCED_SERIAL_OUT, 
    TRPORD.TOWSID, 
    TRPORD.TOWPADR, 
    PAK.WEIGHT, 
    PAK.VOLUME,
    EAN.EANDUN_ORIGINAL EAN
FROM   
    TRPORD,
    CAR,
    ART,
    PAK,
    ITE,
    EAN
WHERE   
    CAR.CARID=ITE.CARID and
    car.company_id=ite.company_id and
    ITE.ARTID=ART.ARTID AND 
    ITE.COMPANY_ID=ART.COMPANY_ID and
    art.basPAKID=PAK.PAKID AND 
    art.ARTID=PAK.ARTID and
    art.company_id=pak.company_id and
    ite.company_id=pak.company_id and
    CAR.CARID=TRPORD.CARID(+) AND 
    CAR.WHID=TRPORD.WHID(+) and
    car.company_id=trpord.company_id(+) and
    ART.ARTID=EAN.ARTID(+) AND 
    EAN.COMPANY_ID=car.company_id AND
    car.COMPANY_ID='AT-PREFA' and
    ite.iteid='{?ITEID_I}'
