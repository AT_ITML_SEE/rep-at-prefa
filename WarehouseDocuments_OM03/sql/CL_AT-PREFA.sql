/******************************************************************************
NAME: CL_AT-PREFA.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

  SELECT PBROW.PBHEADID,
         PBHEAD.EMPID,
         PARTY.NAME1,
         PARTY.COUNTRYCODE,
         PBCAR.CARID,
--         PBROW.COID,
         PBROW.COID||'-'||PBROW.COSEQ COID,
         COWORK.DLVRYMETH_ID,
         PBCAR.CUSCODE,
         PBCAR.PBCARID,
         CARTYP.CARNAME,
         COWORK.CONSIGNMENT_ID,
         to_char(Sysdate,'YYYY/MM/DD') Print_date,
         DEPTRP.SHIPWSID,
         (DEPTRP.SHIPWSID)||' - '||(DEPTRP.SHIPWPADR) as "ShipAreaAndAddress"         
--         ,    max(DEPTRP.SHIPWSID)||' - '||max(DEPTRP.SHIPWPADR) as "ShipAreaAndAddress"
    FROM OWUSER.PBROW PBROW
         INNER JOIN OWUSER.COWORK 
             ON     PBROW.COID = COWORK.COID
                AND PBROW.COSEQ = COWORK.COSEQ
                AND PBROW.COSUBSEQ = COWORK.COSUBSEQ
                AND PBROW.COMPANY_ID = COWORK.COMPANY_ID
         INNER JOIN OWUSER.PBHEAD  ON PBROW.PBHEADID = PBHEAD.PBHEADID
         INNER JOIN OWUSER.PBCAR  ON PBROW.PBCARID = PBCAR.PBCARID
         INNER JOIN OWUSER.PARTY 
             ON     PBROW.SHIPTOPARTYID = PARTY.PARTY_ID
                AND PBROW.SHIPTOPARTYQUALIFIER = PARTY.PARTY_QUALIFIER
                AND PBROW.COMPANY_ID = PARTY.COMPANY_ID
         LEFT JOIN OWUSER.CAR 
             ON     PBCAR.CARID = CAR.CARID
                AND PBCAR.WHID = CAR.WHID
                AND PBCAR.COMPANY_ID = CAR.COMPANY_ID
         LEFT JOIN OWUSER.CARTYP  ON CAR.CARTYPID = CARTYP.CARTYPID
         
         LEFT JOIN dep
            ON  cowork.WHID = dep.WHID and 
                cowork.DEPARTURE_ID = dep.DEPARTURE_ID
         LEFT JOIN deptrp
            ON  dep.WHID = DEPTRP.WHID and 
                dep.DEPARTURE_ID = DEPTRP.DEPARTURE_ID

   WHERE PBCAR.CARID = '{?CARID_I}'
--   WHERE PBCAR.CARID =   '008521140020580107'
   and DEPTRP.SHIPWSID is not null
ORDER BY PBCAR.CARID

/******************************************************************************
NAME: CL_AT-PREFA.rpt
SUBREPORT: SUP_PBHEAD
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

/***
NOTE:
There is no SQL command in the RPT file. This has been extracted from Crystal Reports via "Database" - "View SQL Query"

***/

 SELECT PBCAR.PBHEADID, PBCAR.CONSIGNMENT_ID, PBCAR.PZID
 FROM   OWUSER.PBCAR PBCAR
 WHERE  PBCAR.CONSIGNMENT_ID=''
 ORDER BY PBCAR.PZID


