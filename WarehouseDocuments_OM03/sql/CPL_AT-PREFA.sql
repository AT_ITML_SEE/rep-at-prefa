/* Formatted on 13.10.2022 11:34:58 (QP5 v5.336) */
/***************************************************************************************************************
   NAME: CPL_AT-PREFA.rpt (Kommando)
   DESC: CPL FOR AT-PREFA
   REVISIONS:
	Ver			Date			Player			Source						Description
	---------	----------		------------	---------------------		---------------------------------
	1.0			????/??/??		????????		implementation				initial version
	1.1			2021/11/19		UJABCZYS		cooperation					changed table joining method, modified formatting
	1.2			2022/04/06		DAKORNHA		PREFA-105					removed commented columns
                                                             added tables ART, COROWFT (AMT and UNL) and RCVCARLOG - incl. in select
	1.3			2022/04/13		DAKORNHA		PREFA-105					exclude grouped PBROWs
	1.4			2022/04/19		DAKORNHA		PREFA-111					SUM(PAK.WEIGHT) fixed with PAK.WEIGHT - too high weight when having multiple pick order lines for one product
	1.5			2022/04/25		DAKORNHA		PREFA-112					removed REPLACE(AMT.TEXT, '.', ',') around AMT.TEXT
	1.6        2022/04/26     DAKORNHA       PREFA-112                LEFT JOIN COROW.COPOS and COROW.COSUBPOS to PBROW.COPOS and PBROW.COSUBPOS
	1.7        2022/04/27     DAKORNHA       PREFA-115                COID and COSEQ missing on parent CAR. taken from CONSIGNMENT
	1.8        2022/05/19     DAKORNHA       INC005553144             when the ASN has been deleted after the configured time period and has been sent again
                                                             the item load ID was found twice in RCVCARLOG. Fixed with RCVCARLOG.DELID IS NOT NULL
    1.9			2022/05/08		DCOVALSC		IMI-2425					add load carrier type on label
	1.10       2022/08/16     AQEASLAM       IMI-2486                Tour Number is adjusted in the report.
	2.0        2022/08/22     DAKORNHA       IMI-2429                calc of weight on first page will be done based on PAKID, 
                                                             as weight for 'Rol' (parent/child) can only be taken from RCVCARLOG
                                                             added some joins for better performance
                                                             added to_number(AMT.TEXT, '99,99') for correct formatting
    2.1        2022/08/30     DAKORNHA       IMI-2429                change to_number(AMT.TEXT, '99,99')  back to AMT.TEXT only
    2.2        2022/08/30     DAKORNHA       IMI-2429                brutto calculation: SUM(CAR.TOTWGT)/COUNT(CAR.CARID)
    2.3        2022/10/13     DAKORNHA       IMI-2635                correct weight calculations 
    2.4        2022/11/11     DAKORNHA       IMI-2635                correct weight calculations (window function for TOT_CAR_WEIGHT) 
    2.5        2022/11/14     DAKORNHA       INC006191602            changed to_number(amt.text , '9999.999') to amt.text 
    2.3        2023/03/09     PBRUGGER       IMI-2320             added function to determine brutto/netto wgt
    2.?        2023/03/30     PBRUGGER       IMI-2320       fixed amtqty
    2.8         2023/04/05    DAKORNHA       IMI-2320           rolled back to previous amtqty calculation
    
****************************************************************************************************************/

WITH 
    loc_car_counter_consignment_v AS (
        SELECT DENSE_RANK() OVER(PARTITION BY consignment_id ORDER BY carid)    AS counter
             , carid
             , consignment_id
        FROM pbcar
        WHERE company_id = 'AT-PREFA'
           AND whid = 'PRE1'
           AND consolidation_to_carid IS NULL
           AND merge_to_carid IS NULL
    )
SELECT car.carid
    , coftx.text AS "Reference"
    /* Workaround regarding missing COID in PBCAR */
    , CASE
        WHEN pbrow.coid IS NOT NULL
        THEN pbcar.coid_single || '-' || pbcar.coseq_single
        WHEN pbrow.coid IS NULL
        THEN consignment.coid || '-' || consignment.coseq
    END AS coid
    , SUBSTR(pbcar.proid, LENGTH(pbcar.proid) - 4) AS proid
    , TO_CHAR(SYSDATE, 'YYYY/MM/DD') AS print_date
    , consignment.fre_consignment_id
    , dep.routname AS routname_old
    , 'T-' || SUBSTR(dep.routname, LENGTH(dep.routname) - 1) AS routname_last
    , DECODE(LENGTH(dep.routname)
          , 11, ('T-' || SUBSTR(dep.routname, LENGTH(dep.routname) - 2))
          , 10, ('T-' || SUBSTR(dep.routname, LENGTH(dep.routname) - 1))
          , 12, ('T-' || SUBSTR(dep.routname, LENGTH(dep.routname) - 2))
          , 18, ('T-' || SUBSTR(dep.routname, LENGTH(dep.routname) - 1))
          , 14, ('T-' || SUBSTR(dep.routname, LENGTH(dep.routname) - 4))
          , dep.routname)                                routname
    , dep.departure_dtm
    , pbcar.wws_dropcode_ship
    , car.cartypid
    , pbcar.pbtype
    , pbcar.for_consolidation
    , loc_car_counter_consignment_v.counter
    , consignment.consignment_id
    , car.company_id
    , SUM(car.user_weight) AS user_weight
    , SUM(pbrow.pickqty) AS pickqty
    , pak.weight AS pak_weight
    , TO_NUMBER(DECODE(pak.weight, NULL, 0, cartyp.weight)) AS cartyp_weight
--    , SUM(CASE
--        WHEN pak.pakid = 'Rol'                -- needed for page 1 weight
--        THEN SUM(rcvcarlog.totwgt) 
--        ELSE SUM(pbrow.pickqty * pak.weight)
--      END) OVER (PARTITION BY car.carid) + cartyp.weight AS tot_car_weight
    , LOP_AT_PREFA.CALC_CARWGT(CAR.CARID,'Y',DEP.DEPARTURE_ID) AS tot_car_weight 
    , party_pbcar.name1
    , party_pbcar.name2
    , party_pbcar.adr1
    , party_pbcar.countrycode
    , party_pbcar.postcode
    , party_pbcar.city
    , SUM(car.totwgt) AS totwgt
    , pbcar.towsid
    , pbcar.cuscode
    , dep.departure_id
    , pbcar.company_id
    , pbcar.pbcarstat
    , NVL(coftx.text, '-')
    , (SELECT LISTAGG(pzid, ';') WITHIN GROUP (ORDER BY 1)
        FROM (
            SELECT DISTINCT coid, pzid
            FROM pbrow pbrow2
            WHERE 
                company_id = 'AT-PREFA'
                AND pbrow2.coid = pbrow.coid
           )
        GROUP BY coid) AS pzids
    , (SELECT LISTAGG(wsid, ';') WITHIN GROUP (ORDER BY 1)
        FROM (
            SELECT DISTINCT coid, wsid
            FROM pbrow pbrow3
            WHERE 
                company_id = 'AT-PREFA'
                AND pbrow3.coid = pbrow.coid
           )
        GROUP BY coid) AS wsids
    , art.artid
    , art.artname1
    , SUM(pbrow.pickqty * pak.baseqty) AS baseqty
    , pak.pakid
    /*  page2 */
    , CASE
        WHEN pak.pakid = 'Rol' 
        THEN SUM(rcvcarlog.totwgt)
        ELSE SUM(pbrow.pickqty * amt.text)  --  SUM(PBROW.PICKQTY*PAK.WEIGHT)
    END AS amtqty
    --, LOP_AT_PREFA.CALC_CARWGT(CAR.CARID,'N',DEP.DEPARTURE_ID) AS amtqty     
    , CASE 
        WHEN pak.pakid = 'Rol' 
        THEN 'kg' 
        ELSE unl.text 
    END AS unlpakid
FROM car
INNER JOIN pbcar ON
    car.company_id = pbcar.company_id
    AND car.whid = pbcar.whid
    AND car.carid = NVL(pbcar.consolidation_to_carid, pbcar.carid)
LEFT JOIN dep ON
    car.whid = dep.whid
    AND car.departure_id = dep.departure_id
LEFT JOIN loc_car_counter_consignment_v ON
    car.carid = loc_car_counter_consignment_v.carid
INNER JOIN cartyp ON 
    car.cartypid = cartyp.cartypid
LEFT JOIN pbrow ON 
    pbcar.company_id = pbrow.company_id
    AND pbcar.pbcarid = pbrow.pbcarid
    AND pbrow.pbtype NOT IN ('G')
INNER JOIN consignment ON 
    pbcar.consignment_id = consignment.consignment_id
LEFT JOIN corow ON
    corow.coid = consignment.coid
    AND corow.coseq = consignment.coseq
    AND corow.copos = pbrow.copos
    AND corow.cosubpos = pbrow.cosubpos
    AND corow.company_id = 'AT-PREFA'
LEFT JOIN party party_pbcar ON
    pbcar.shiptopartyid = party_pbcar.party_id
       AND party_pbcar.party_qualifier = 'CU'
LEFT JOIN pak ON
    pbrow.company_id = pak.company_id
    AND pbrow.artid = pak.artid
    AND pbrow.pakid = pak.pakid
LEFT JOIN art ON
    pbrow.company_id = art.company_id
   AND pbrow.artid = art.artid
LEFT JOIN coftx ON
     coftx.company_id = consignment.company_id
       AND coftx.coid = car.coid
       AND coftx.coseq = car.coseq
       AND coftx.ftx_qualifier = 'RFB'
LEFT JOIN corowftx amt ON
    amt.company_id = consignment.company_id
    AND amt.coid = corow.coid
    AND amt.coseq = corow.coseq
    AND amt.copos = corow.copos
    AND amt.cosubpos = corow.cosubpos
    AND amt.ftx_qualifier = 'AMT'
LEFT JOIN corowftx unl ON
    unl.company_id = consignment.company_id
    AND unl.coid = corow.coid
    AND unl.coseq = corow.coseq
    AND unl.copos = corow.copos
    AND unl.cosubpos = corow.cosubpos
    AND unl.ftx_qualifier = 'UNL'
LEFT JOIN rcvcarlog ON
    rcvcarlog.carid = pbrow.iteid
    AND rcvcarlog.whid = car.whid
    AND rcvcarlog.company_id = car.company_id
    AND rcvcarlog.delid IS NOT NULL
WHERE car.company_id = 'AT-PREFA'
    AND car.whid = 'PRE1'
    AND NVL(car.carcarid, car.carid) = '{?CARID_I}'
    /* leave in here for test purpose */
--    AND NVL(CAR.CARCARID, CAR.CARID) IN ('008521140078004006')
GROUP BY 
    car.carid
    , coftx.text
    /*  Workaround regarding missing COID in PBCAR */
    , pbcar.coid_single
    , pbcar.coseq_single
    , pbrow.coid
    , consignment.coid || '-' || consignment.coseq
    , SUBSTR(pbcar.proid, LENGTH(pbcar.proid) - 4)
    , TO_CHAR(SYSDATE, 'YYYY/MM/DD')
    , consignment.fre_consignment_id
    , dep.routname
    , dep.departure_dtm
    , pbcar.wws_dropcode_ship
    , car.cartypid
    , pbcar.pbtype
    , pbcar.for_consolidation
    , loc_car_counter_consignment_v.counter
    , consignment.consignment_id
    , car.company_id
    , pak.weight
    , cartyp.weight
    , party_pbcar.name1
    , party_pbcar.name2
    , party_pbcar.adr1
    , party_pbcar.countrycode
    , party_pbcar.postcode
    , party_pbcar.city
    , pbcar.towsid
    , pbcar.cuscode
    , dep.departure_id
    , pbcar.company_id
    , pbcar.pbcarstat
    , NVL(coftx.text, '-')
    , art.artid
    , art.artname1
    , pak.baseqty
    , pak.pakid
    , unl.text
ORDER BY 
    pbcar.pbtype DESC
    , art.artid, art.artname1