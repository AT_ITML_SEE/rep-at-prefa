/******************************************************************************
NAME: AT-PREFA_View_Item.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT
    olacodnl.olacodtxt itestat,
   ite.company_id,
   ite.iteid,
   ite.artid,
   ite.artname,
   artgrp.artgrpname,
   ite.pakid,
   ite.storqty,
   car.whid,
   car.wsid,
   car.wpadr,
   ws.wsname,
   ite.arrdate,
   ite.prodlot,
   ite.storbat,
   ite.ownid,
   ite.last_consuming_date,
   ite.sernumb,
   ite.promotn,
   blockcod.blockcodnm,
   olacodtxt,
   last_consuming_date - TRUNC (SYSDATE) AS shelfdays,
   --(select itechgtxt from itechg where itechg.iteid = ite.iteid fetch first row only) txt
   (select itechgtxt from itechg where itechg.iteid = ite.iteid and itechg.itechgcodid = 'BL' order by TO_CHAR(upddtm, 'YYYY-MM-DD HH24:MI:SS') desc fetch first row only) BLOCKtxt,
   (select itechgtxt from itechg where itechg.iteid = ite.iteid and itechg.itechgcodid <> 'BL' order by TO_CHAR(upddtm, 'YYYY-MM-DD HH24:MI:SS') desc fetch first row only) OTHERtxt
FROM ite,
   car,
   blockcod,
   art,
   artgrp,
   olacodnl,
   ws
WHERE     car.carid = ite.carid
   AND blockcod.blockcod = ite.blockcod
   AND art.company_id = ite.company_id
   AND art.artid = ite.artid
   AND artgrp.company_id = art.company_id
   AND artgrp.artgroup = art.artgroup
   AND OLACOD = ITESTAT
   AND OLAID = 'ITESTAT'
   AND NLANGCOD = 'ENU'
   AND ws.whid = car.whid
   AND ws.wsid = car.wsid
   AND ite.company_id = 'AT-PREFA'
   AND car.company_id = 'AT-PREFA'