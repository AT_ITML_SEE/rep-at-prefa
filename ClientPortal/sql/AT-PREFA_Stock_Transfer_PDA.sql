/* Formatted on  */
/******************************************************************************
NAME: AT-PREFA_Stock_Transfer_PDA
DESC: Stock transfer from Location to PDA/MIS Location and the way back
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description

1.0 		23.05.2022 		ASLAQEEL     IMI-2223 	    created
1.1.        11.07.2022      DCOVALSC     IMI-2223       removed PIKBLOCK in it.BLOCKCOD2
1.2 		20.07.2022 		DCOVALSC     IMI-2223	    exchanged ORDER BY 1 with ORDER BY it.UPDDTM DESC
1.3         31.07.2022      DCOVALSC     IMI-2223       added join ITE (show current MIS and 00, with hold code change past 24h)


******************************************************************************/

select 
it.ARTID as "Product Number",
it.ITEID "Item Load",
it.STORQTY "Quantity",
it.WSID||'/'|| it.WPADR "Location", 
it.BLOCKCOD2 "Hold Code New",
it.EMPID "User",
it.UPDDTM 
from ITECHG it 
join ITE on ITE.COMPANY_ID = it.COMPANY_ID
and it.ITEID = ite.ITEID
and it.BLOCKCOD2 = ite.BLOCKCOD
where it.COMPANY_ID='AT-PREFA'
and it.BLOCKCOD <> it.BLOCKCOD2
--and it.WSID='PDA'
--AND TO_CHAR (it.UPDDTM, 'yyyy-mm-dd') = TO_CHAR (SYSDATE, 'yyyy-mm-dd')  
and  to_char(it.UPDDTM,'YYYYMMDDHH24MISS') >= to_char(sysdate - 1,'YYYYMMDD') || '130000' AND to_char(it.UPDDTM,'YYYYMMDDHH24MISS') < to_char(sysdate,'YYYYMMDD') || '130000'
--AND TO_CHAR (it.UPDDTM, 'yyyy-mm-dd') = '2022-05-30'
and it.BLOCKCOD2 in ('00','MIS') --filter for IMI-2223
--AND it.ITEID = '390082980011676363'
order by it.UPDDTM DESC, it.ARTID --order by 1 removed