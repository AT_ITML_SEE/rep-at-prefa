/******************************************************************************
NAME: AT-PREFA_Weight_of_roll.rpt
SUBREPORT: -
DESC: The correct balance of BASPAKID = 'Rol' products are fetched from
        RCVCAR/RCVCARLOG.TOTWGT

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		04.11.2022      DAKORNHA    IMI-2626        created

******************************************************************************/

SELECT rcvrow.artid
    , rcvcar.carid
    , rcvcar.totwgt
    , rcvhead.shipdtm AS arrdtm
FROM rcvrow  rcvrow
    INNER JOIN rcvcar rcvcar ON rcvrow.rcvcarid = rcvcar.rcvcarid
    INNER JOIN rcvhead rcvhead ON rcvrow.rcvheadid = rcvhead.rcvheadid
WHERE  rcvcar.company_id = 'AT-PREFA'
    AND rcvrow.baspakid = 'Rol'
    AND rcvcar.rcvcarstat != 90
    AND rcvhead.shipdtm BETWEEN TRUNC({?SHIPDTM_FROM}) AND TRUNC({?SHIPDTM_TO})+1
    AND rcvrow.artid LIKE '{?ARTID}'
UNION
SELECT rcvrow.artid
    , rcvcar.carid
    , rcvcar.totwgt
    , rcvhead.shipdtm AS arrdtm
FROM rcvrowlog  rcvrow
    INNER JOIN rcvcarlog rcvcar ON rcvrow.rcvcarid = rcvcar.rcvcarid
    INNER JOIN rcvheadlog rcvhead ON rcvrow.rcvheadid = rcvhead.rcvheadid
WHERE  rcvcar.company_id = 'AT-PREFA'
    AND rcvrow.baspakid = 'Rol'
    AND rcvcar.rcvcarstat != 90
    AND rcvhead.shipdtm BETWEEN TRUNC({?SHIPDTM_FROM}) AND TRUNC({?SHIPDTM_TO})+1
    AND rcvrow.artid LIKE '{?ARTID}'