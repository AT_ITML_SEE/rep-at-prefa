/***************************************************************************************************************
NAME:    AT-PREFA_Data-Transfer_to_eSchenker.rpt
DESC:    Check which Customer Orders (COID & COSEQ) were exported and send to e-Schenker (Basis for Check is DB-Table LOC_FTP_TRACE)

REVISIONS:
Ver			Date			Player			Source						Description
---------	----------		------------	---------------------		---------------------------------
1.0			2022/01/25		MARIEMEL		implementation				initial version (Request & Definition by Prefa Project Team)
1.1         2022/02/05      MARIEMEL        improvment (IT driven)      Date-Comparisons were adjusted to 'YYYYMMDD' Format in where-clauses
1.2         2022/02/10      MARIEMEL        improvment                  Adding PPOSTCODE, CITY, ADR1, NAME1 and DEPARTURE_ID (requested by Ph. Hauser)
1.3         2022/04/07      MARIEMEL        re-design                   Re-Design was required because we changed the basis of report from CONSIGNMENT to COID&COSEQ based
   
****************************************************************************************************************/


with
partie as
    (
    select party_id, countrycode, POSTCODE, CITY, ADR1, NAME1 from party
    ),
    sql as 
    (
    select
    COID || '-' || COSEQ,
    shiptopartyid
    --DEPARTURE_ID
    from CONSIGNMENT
    left join party on consignment.shiptopartyid = party.party_id
        where  COID || '-' || COSEQ in 
            (
            select  COID || '-' || COSEQ from LOC_DLVRYNOTE_PDF2
            where ID in
                (
                    select DELIVERYNOTE_ID from LOC_FTP_TRACE
                    where JOBID = 'AT_PREFA_ESCH_P'
                    and DONE is not null
                    and FILENAME like 'ESCH%'
                    and to_char(DONE, 'YYYYMMDD') = to_char(to_date({?Date}), 'YYYYMMDD')
                ) 
                AND CONFIG_ID = 'AT_PREFA_ESCH_P'
                group by  COID || '-' || COSEQ
            )
            group by COID || '-' || COSEQ, shiptopartyid
    
    UNION
    
    select
    COID || '-' || COSEQ,
    shiptopartyid
    --DEPARTURE_ID
    from CONSIGNMENTTRC
    where  COID || '-' || COSEQ in (
        select   COID || '-' || COSEQ from LOC_DLVRYNOTE_PDF2
        where ID in
            (
                select DELIVERYNOTE_ID from LOC_FTP_TRACE
                where JOBID = 'AT_PREFA_ESCH_P'
                and DONE is not null
                and FILENAME like 'ESCH%'
                and to_char(DONE, 'YYYYMMDD') = to_char(to_date({?Date}), 'YYYYMMDD') 
            )
            AND CONFIG_ID = 'AT_PREFA_ESCH_P'
            group by  COID || '-' || COSEQ
        )
    group by COID || '-' || COSEQ, shiptopartyid
    )

    select *
    from
    sql
    inner join partie on partie.party_id = sql.shiptopartyid
    order by party_id, countrycode, POSTCODE, CITY, ADR1 asc
