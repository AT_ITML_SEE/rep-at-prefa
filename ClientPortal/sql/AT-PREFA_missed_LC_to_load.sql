/* Formatted on 01/03/2022 13:02:21 (QP5 v5.336) */
/******************************************************************************
NAME: AT-PREFA_missed_LC_to_load.rpt
DESC: shows not loaded and not fully picked load carriers for departure


REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		12.05.2022 		DAKORNHA     PREFA-117 	    created
1.1 		17.05.2022 		DAKORNHA     PREFA-117 	    added DEPARTURE_PICKSTAT and CARNAME
                                                        removed DEPARTURE_PICKSTAT in ('H')

******************************************************************************/

SELECT TRANSPORT.COID_COSEQ,
    TRANSPORT.CONSIGNMENT_ID,
    TRANSPORT.DEPARTURE_DATE,
    TRANSPORT.SHIP_TO_PARTY_NAME,
    TRANSPORT.SHIP_TO_PARTY_COUNTRYCODE,
    TRANSPORT.SHIP_TO_PARTY_ZIP,
    TRANSPORT.SHIP_TO_PARTY_CITY,
    TRANSPORT.SHIP_TO_PARTY_ADR1,
    SUM(TRANSPORT.SUM_LC) SUM_LC,
    TRANSPORT.LC_DESC,
    TRANSPORT.CARTYPID,
    TRANSPORT.CARNAME,
    SUM(TRANSPORT.LDM) LDM,
    SUM(TRANSPORT.WEIGHT) WEIGHT,
    TRANSPORT.LENGTH,
    TRANSPORT.WIDTH,
    SUM(TRANSPORT.VOL) VOL,
    TRANSPORT.INFO,
    TRANSPORT.DEPARTURE_ID,
    TRANSPORT.ACKSHIPDATE,
    TRANSPORT.DEPARTURE_DTM,
    TRANSPORT.DEP_PICKSTAT,
    COUNT(DISTINCT 
        CASE WHEN PBCARSTAT = 'L'
            THEN CARID
            ELSE NULL
        END) LC_loaded,
    COUNT(DISTINCT 
        CASE WHEN PBCARSTAT != 'L'
            THEN CARID
            ELSE NULL
        END) LC_not_loaded,
    COUNT(DISTINCT 
        CASE WHEN PBCARSTAT = 'C'
            THEN CARID
            ELSE NULL
        END) LC_closed,
    SUM(DISTINCT
        CASE WHEN PBCARSTAT NOT IN ('C', 'L')
            THEN COPOS
            ELSE NULL
        END) POS_open,
    LISTAGG(CASE WHEN PBCARSTAT != 'L'
                THEN CARID
                ELSE NULL
            END, ', ') 
        WITHIN GROUP (ORDER BY CARID) SSCC_not_loaded,
    LISTAGG(CASE WHEN PBCARSTAT != 'L'
                THEN WSID
                ELSE NULL
            END, ', ') 
        WITHIN GROUP (ORDER BY CARID) AREA_not_loaded
FROM (
    SELECT 
        NVL(CO.COID, COWORK.COID)
            ||'-'||NVL(CO.COSEQ, COWORK.COSEQ) COID_COSEQ,
        COWORK.CONSIGNMENT_ID CONSIGNMENT_ID,
        TRUNC(DEP.DEPARTURE_DTM) DEPARTURE_DATE,
        PARTY.NAME1 SHIP_TO_PARTY_NAME,
        PARTY.COUNTRYCODE SHIP_TO_PARTY_COUNTRYCODE,
        PARTY.POSTCODE SHIP_TO_PARTY_ZIP,
        PARTY.CITY SHIP_TO_PARTY_CITY,
        PARTY.ADR1 SHIP_TO_PARTY_ADR1,
        CASE WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) < 0.01 THEN 0.01
            ELSE SUM(COROW.ORDQTY / PAK.PALLET_CALC)
        END SUM_LC,
        CARTYP.CARTYPID,
        CARTYP.CARNAME,
        DECODE (CARTYP.CARTYPID2
                   , 'G01'  , 'BE'
                   , 'G02'  , 'BE'
                   , 'G03'  , 'BE'
                   , 'G04'  , 'BE'
                   , 'G05x' , 'EW'
                   , 'EWP'  , 'XP'
                   , 'BD3'  , 'CO'
                   , 'BD6'  , 'CO'
                   , 'KRT'  , 'KT'
                   , 'K04'  , 'KT'
                   , 'K03'  , 'KT'
                   , 'WAMAS', 'KT'
                , 'PA') LC_DESC,
        CASE WHEN
                CASE 
                    WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.25 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.2 * CARTYP.LOADING_METER
                    WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.25 
                        AND SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.5 * CARTYP.LOADING_METER
                    WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * CARTYP.LOADING_METER
                END  < 0.01
            THEN 0.01
            ELSE 
                CASE 
                    WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.25 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.2 * CARTYP.LOADING_METER
                    WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.25 
                        AND SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.5 * CARTYP.LOADING_METER
                    WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * CARTYP.LOADING_METER
                END
        END LDM,
        SUM(DECODE(COROW.ESTWEIGHT, 0, (PAK.BASPAKWEIGHT*COROW.ORDQTY), COROW.ESTWEIGHT)) WEIGHT,
        CARTYP.LENGTH LENGTH,
        CARTYP.WIDTH WIDTH,
        CASE WHEN SUM(COROW.ESTVOLUME) < 0.01 THEN 0.01
            ELSE SUM(COROW.ESTVOLUME)
        END VOL,
        CASE WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%GFGSAUM%' THEN 'ADR, SAUM' 
            WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%GFG%' THEN 'ADR' 
            WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%SRI%' THEN 'SAUM'
            ELSE NULL 
        END INFO,
        DEP.DEPARTURE_ID,
        MAX(COROW.ACKSHIPDATE) ACKSHIPDATE,
        DEP.DEPARTURE_DTM,
        DECODE(DEP.DEPARTURE_PICKSTAT, 'C', 'Not started',
                                'O', 'Open',
                                'F', 'Finished',
                                'H', 'On Hold',
                                'R', 'Ready',
                            DEP.DEPARTURE_PICKSTAT) DEP_PICKSTAT,
        PBCAR.PBCARSTAT,
        NVL(NVL(PBCAR.MERGE_TO_CARID, PBCAR.CONSOLIDATION_TO_CARID), PBCAR.CARID) CARID,
        COUNT(DISTINCT PBROW.COPOS) COPOS,
        PBROW.WSID
    FROM DEP
    JOIN PBCAR
        ON PBCAR.DEPARTURE_ID = DEP.DEPARTURE_ID
        AND PBCAR.WHID = DEP.WHID
    JOIN PBROW
        ON PBROW.PBCARID = PBCAR.PBCARID
        AND PBROW.WHID = PBCAR.WHID
    JOIN COWORK -- NVL due to bug with Parent-Child LC
        ON COWORK.DEPARTURE_ID = DEP.DEPARTURE_ID
        AND COWORK.WHID = DEP.WHID
        AND COWORK.COMPANY_ID = PBCAR.COMPANY_ID
    JOIN CO -- NVL due to bug with Parent-Child LC
        ON CO.COID = COWORK.COID
            AND CO.COSEQ = COWORK.COSEQ
            AND CO.COMPANY_ID = COWORK.COMPANY_ID     
    JOIN COROW -- NVL due to bug with Parent-Child LC
        ON COROW.COID = COWORK.COID
            AND COROW.COSEQ = COWORK.COSEQ
            AND COROW.COMPANY_ID = COWORK.COMPANY_ID
            AND COROW.COID = PBROW.COID
            AND COROW.COSEQ = PBROW.COSEQ
            AND COROW.COPOS = PBROW.COPOS
            AND COROW.COSUBPOS = PBROW.COSUBPOS
    JOIN PARTY
           ON PARTY.PARTY_QUALIFIER = COWORK.SHIPTOPARTYQUALIFIER
            AND PARTY.PARTY_ID = COWORK.SHIPTOPARTYID                    
            AND PARTY.COMPANY_ID = COWORK.COMPANY_ID
    LEFT JOIN ART
        ON ART.ARTID = COROW.ARTID
        AND ART.COMPANY_ID = COROW.COMPANY_ID
        AND (ART.ARTGROUP = 'GFG'
            OR UPPER(ART.ARTNAME2) = 'SRI')
    JOIN (SELECT PAK.ARTID,
                PAK.PAKID,
                PAK.PAKNAME,
                BASPAK.PAKID BASPAKPAKID,
                BASPAK.BASEQTY BASPAKBASEQTY,
                BASPAK.WEIGHT BASPAKWEIGHT,
                PAK.BASEQTY,
                PAK.WEIGHT,
                DECODE(PAK.PAKID
                        , 'g'   , 200
                        , 'mm'  , 200
                        , 'Stk' , 200
                    , PAK.BASEQTY) PALLET_CALC,
                DECODE(PAK.PAKID
                        , 'Box' , 'G05x'
                        , 'g'   , 'G05x'
                        , 'KDV' , 'G05x'
                        , 'K06' , 'G05x'
                        , 'K07' , 'G05x'
                        , 'mm'  , 'G05x'
                        , 'PAL' , 'G05x'
                        , 'Rol' , 'G05x'
                        , 'Stk' , 'G05x'
                    , PAK.PAKID) CARTYPID,
                RANK () OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) AS LVL
            FROM PAK
            JOIN (SELECT ARTID,
                        PAKID,
                        COMPANY_ID,
                        BASEQTY,
                        WEIGHT
                    FROM PAK
                    WHERE PAK.COMPANY_ID = 'AT-PREFA') BASPAK
                ON BASPAK.ARTID = PAK.ARTID
                AND BASPAK.COMPANY_ID = PAK.COMPANY_ID               
                AND BASPAK.PAKID = PAK.BASPAKID
            WHERE PAK.COMPANY_ID = 'AT-PREFA') PAK
        ON PAK.ARTID = COROW.ARTID
            AND PAK.LVL = 1
    JOIN CARTYP
        ON CARTYP.CARTYPID2 = PAK.CARTYPID
    WHERE COWORK.NO_RESTROWS <> COWORK.NOROWS -- no full cancelled orders
        AND DEP.WHID = 'PRE1'
        AND DEP.DEPARTURE_PICKSTAT not in ('C')
        AND COWORK.COSTATID in ({?COSTATID})
        AND DEP.DLVRYMETH_ID <> 'AT-PREFA-PICKUP'
        AND COWORK.COMPANY_ID = 'AT-PREFA'
        AND COROW.CANCELDTM IS NULL
        AND COROW.ORDQTY > 0
        AND TRUNC(DEP.DEPARTURE_DTM) BETWEEN TRUNC({?DATE_FROM}) AND TRUNC({?DATE_TO})+1
    GROUP BY NVL(CO.COID, COWORK.COID)
            ||'-'||NVL(CO.COSEQ, COWORK.COSEQ),
        COWORK.CONSIGNMENT_ID,
        TRUNC(DEPARTURE_DTM),
        PARTY.NAME1,
        PARTY.COUNTRYCODE,
        PARTY.POSTCODE,
        PARTY.CITY,
        PARTY.ADR1,
        CARTYP.CARTYPID,
        CARTYP.CARNAME,
        DECODE (CARTYP.CARTYPID2
                   , 'G01'  , 'BE'
                   , 'G02'  , 'BE'
                   , 'G03'  , 'BE'
                   , 'G04'  , 'BE'
                   , 'G05x' , 'EW'
                   , 'EWP'  , 'XP'
                   , 'BD3'  , 'CO'
                   , 'BD6'  , 'CO'
                   , 'KRT'  , 'KT'
                   , 'K04'  , 'KT'
                   , 'K03'  , 'KT'
                   , 'WAMAS', 'KT'
                , 'PA'),
        CARTYP.LOADING_METER,
        CARTYP.LENGTH,
        CARTYP.WIDTH,
        ART.ARTGROUP,
        ART.ARTNAME2,
        DEP.DEPARTURE_ID,
        DEP.DEPARTURE_PICKSTAT,
        PARTY.PHONE,
        CO.CO_REF,
        DEP.DEPARTURE_DTM,
        PBCAR.PBCARSTAT,
        NVL(NVL(PBCAR.MERGE_TO_CARID, PBCAR.CONSOLIDATION_TO_CARID), PBCAR.CARID),
        PBROW.WSID
    ORDER BY PARTY.COUNTRYCODE,
        PARTY.POSTCODE,
        PARTY.NAME1,
        NVL(CO.COID, COWORK.COID)
            ||'-'||NVL(CO.COSEQ, COWORK.COSEQ),
        TRUNC(DEP.DEPARTURE_DTM)
    ) TRANSPORT
GROUP BY TRANSPORT.COID_COSEQ,
    TRANSPORT.CONSIGNMENT_ID,
    TRANSPORT.DEPARTURE_DATE,
    TRANSPORT.SHIP_TO_PARTY_NAME,
    TRANSPORT.SHIP_TO_PARTY_COUNTRYCODE,
    TRANSPORT.SHIP_TO_PARTY_ZIP,
    TRANSPORT.SHIP_TO_PARTY_CITY,
    TRANSPORT.SHIP_TO_PARTY_ADR1,
    TRANSPORT.LC_DESC,
    TRANSPORT.CARTYPID,
    TRANSPORT.CARNAME,
    TRANSPORT.LENGTH,
    TRANSPORT.WIDTH,
    TRANSPORT.INFO,
    TRANSPORT.DEPARTURE_ID,
    TRANSPORT.ACKSHIPDATE,
    TRANSPORT.DEPARTURE_DTM,
    TRANSPORT.DEP_PICKSTAT
ORDER BY DEPARTURE_DATE,
    COID_COSEQ