/******************************************************************************
NAME: AT-PREFA_-_Trace_Single_Item_Load_via_PPTRC_for_Stock_Compare.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select insertdtm, artid, storqty_old, storqty_new, empid, iteid, coid, coseq, cosubseq, copos, corowkitpos from pptrc where iteid = '{?iteid}'
order by insertdtm desc