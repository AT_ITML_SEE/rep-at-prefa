/* Formatted on 20/12/2022 13:02:56 (QP5 v5.381) */
/******************************************************************************
NAME: AT-PREFA_On_the_fly_inventory.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		20.12.2022      DAKORNHA    IMI-3015        created

******************************************************************************/

SELECT stbat.stordid                                               AS audit_number
    , stbatrow.empid
    , stbat.whid
    , stbat.wsid
    , stbatrow.wpadr
    , stbatrow.artid
    , stbatrow.company_id
    , stbatrow.wws_baspakid                                       AS pakid
    , stbatite.iteid
    , stbatite.storqty
    , NVL(stbatite.stqty, stbatite.storqty)                       AS auditedqty
    , NVL(stbatite.stqty, stbatite.storqty) - stbatite.storqty    AS diffqty
    , DECODE(stbatrow.empty_wp, '1', 'Yes', 'No')                 AS empty_loc
    , DECODE(
        stbat.stbatsta
        , '00', 'Registered.'
        , '10', 'Started.'
        , '20', 'Finished no Errors Found.'
        , '30', 'Finished Errors Found.'
        , '90', 'Acknowledged.'
    )                                                             AS batch_status
    , DECODE(
        stbatite.stitesta
        , '00', 'Registered - Default value when a Line is created.'
        , '10', 'Started - The audit for the Line is started.'
        , '20', 'Finished no Errors Found - The Line is audited and no errors.'
        , '31', 'Item Load Missing - The user has specified that the Item Load is missing on the actual Location.'
        , '32', 'Quantity Changed - The user has specified a divergent balance.'
        , '33', 'New Item Load - The user has specified an Item Load that not was expected on the Location that was audited.'
        , '34', 'Item Load Moved - The Item Load has been moved before the Batch was selected for auditing.'
        , '35', 'Temporarily Held - Not accessible for Inventory Audit.'
        , '36', 'Cancelled - Not audited because of cancelled.'
        , '90', 'Finished - The Inventory Audit Batch is approved.'
    )                                                             AS ite_status
FROM   stord
LEFT JOIN stbat ON
    stord.stordid = stbat.stordid
LEFT JOIN stbatrow ON
    stord.stordid = stbatrow.stordid
LEFT JOIN stbatite ON
    stord.stordid = stbatite.stordid
WHERE  stord.whid = 'PRE1'
AND    stbat.stmethod LIKE 4