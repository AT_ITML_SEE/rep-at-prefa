/******************************************************************************
NAME: AT-PREFA_Order_Overview.rpt
DESC: will be used to identify the status of the orders incl planned ourbound date from Prefa
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description


1.0 		14.07.2022 		DAKORNHA     IMI-2428 	    created

******************************************************************************/

SELECT 
    COWORK.COID||'-'||COWORK.COSEQ COID_COSEQ,
    DECODE (COWORK.COSTATID, '00', '00 - Registered',
                            '05', '05 - Departure Requested',
                            '10', '10 - Departure Connected',
                            '19', '19 - Departure Connection Failure',
                            '20', '20 - Pick Generated',
                            '28', '28 - Error at Balance Check',
                            '29', '29 - Pick Generation Failure',
                            '30', '30 - Pick Started',
                            '40', '40 - Pick Completed',
                            '50', '50 - Pick Finished',
                            '80', '80 - Pick Reported',
                        COWORK.COSTATID) CUSTOMER_ORDER_STATUS,
    DEP.DEPARTURE_ID,
    TRUNC(CO.INSERTDTM) CREATION_DATE,
    TRUNC(COROW.ACKSHIPDATE) ACKSHIPDATE,
    PARTY.NAME1 SHIP_TO_PARTY_NAME,
    PARTY.COUNTRYCODE SHIP_TO_PARTY_COUNTRYCODE,
    PARTY.POSTCODE SHIP_TO_PARTY_ZIP,
    PARTY.ADR1 SHIP_TO_PARTY_ADR1,
    PARTY.CITY SHIP_TO_PARTY_CITY,
    MIN(PBHEAD.STARTDTM),
    MAX(PBHEAD.ENDDTM),
    TRUNC(DEP.LOADFINISHDTM) LOADFINISHDTM
FROM DEP
JOIN COWORK
    ON COWORK.DEPARTURE_ID = DEP.DEPARTURE_ID
    AND COWORK.WHID = DEP.WHID
JOIN CO
    ON CO.COID = COWORK.COID
        AND CO.COSEQ = COWORK.COSEQ
        AND CO.COMPANY_ID = COWORK.COMPANY_ID     
JOIN COROW
    ON COROW.COID = COWORK.COID
        AND COROW.COSEQ = COWORK.COSEQ
        AND COROW.COMPANY_ID = COWORK.COMPANY_ID
JOIN PARTY
       ON PARTY.PARTY_QUALIFIER = COWORK.SHIPTOPARTYQUALIFIER
        AND PARTY.PARTY_ID = COWORK.SHIPTOPARTYID                    
        AND PARTY.COMPANY_ID = COWORK.COMPANY_ID
LEFT JOIN PBROW
    ON PBROW.COID = COWORK.COID
    AND PBROW.COSEQ = COWORK.COSEQ
    AND PBROW.COSUBSEQ = COWORK.COSUBSEQ
LEFT JOIN PBHEAD
    ON PBHEAD.PBHEADID = PBROW.PBHEADID
WHERE DEP.WHID = 'PRE1'
    AND COWORK.COMPANY_ID = 'AT-PREFA'
    AND DEP.DEPARTURE_DTM BETWEEN TRUNC({?DEP_DATE_FROM}) AND TRUNC({?DEP_DATE_TO})+1
    AND DECODE('{?COSTATID}', 'ALL', '1', COWORK.COSTATID) = DECODE('{?COSTATID}', 'ALL', '1', '{?COSTATID}')
GROUP BY COWORK.COID||'-'||COWORK.COSEQ,
    DECODE (COWORK.COSTATID, '00', '00 - Registered',
                            '05', '05 - Departure Requested',
                            '10', '10 - Departure Connected',
                            '19', '19 - Departure Connection Failure',
                            '20', '20 - Pick Generated',
                            '28', '28 - Error at Balance Check',
                            '29', '29 - Pick Generation Failure',
                            '30', '30 - Pick Started',
                            '40', '40 - Pick Completed',
                            '50', '50 - Pick Finished',
                            '80', '80 - Pick Reported',
                        COWORK.COSTATID),
    DEP.DEPARTURE_ID,
    TRUNC(CO.INSERTDTM),
    TRUNC(COROW.ACKSHIPDATE),
    PARTY.NAME1,
    PARTY.COUNTRYCODE,
    PARTY.POSTCODE,
    PARTY.ADR1,
    PARTY.CITY,
    TRUNC(DEP.LOADFINISHDTM) 
UNION
SELECT 
    CO.COID||'-'||CO.COSEQ COID_COSEQ,
    '80 - Pick Reported' CUSTOMER_ORDER_STATUS,
    DEP.DEPARTURE_ID,
    TRUNC(CO.INSERTDTM) CREATION_DATE,
    TRUNC(COROW.ACKSHIPDATE) ACKSHIPDATE,
    PARTY.NAME1 SHIP_TO_PARTY_NAME,
    PARTY.COUNTRYCODE SHIP_TO_PARTY_COUNTRYCODE,
    PARTY.POSTCODE SHIP_TO_PARTY_ZIP,
    PARTY.ADR1 SHIP_TO_PARTY_ADR1,
    PARTY.CITY SHIP_TO_PARTY_CITY,
    MIN(PBHEAD.STARTDTM),
    MAX(PBHEAD.ENDDTM),
    TRUNC(DEP.LOADFINISHDTM) LOADFINISHDTM
FROM DEPLOG DEP
JOIN PBROWLOG PBROW
    ON PBROW.DEPARTURE_ID = DEP.DEPARTURE_ID  
JOIN COTRC CO
    ON PBROW.COID = CO.COID
    AND PBROW.COSEQ = CO.COSEQ
    AND PBROW.COSUBSEQ = CO.COSUBSEQ 
    AND PBROW.COMPANY_ID = CO.COMPANY_ID    
JOIN COROWTRC COROW
    ON COROW.COID = CO.COID
    AND COROW.COSEQ = CO.COSEQ
    AND COROW.COMPANY_ID = CO.COMPANY_ID
JOIN PARTY
    ON PARTY.PARTY_QUALIFIER = CO.SHIPTOPARTYQUALIFIER
    AND PARTY.PARTY_ID = CO.SHIPTOPARTYID                    
    AND PARTY.COMPANY_ID = CO.COMPANY_ID
JOIN PBHEADLOG PBHEAD
    ON PBHEAD.PBHEADID = PBROW.PBHEADID
WHERE DEP.WHID = 'PRE1'
    AND CO.COMPANY_ID = 'AT-PREFA'
    AND DEP.DEPARTURE_DTM BETWEEN TRUNC({?DEP_DATE_FROM}) AND TRUNC({?DEP_DATE_TO})+1
    AND DECODE('{?COSTATID}', 'ALL', '1'
                            , '80', '1'
                            , '2') 
            = '1'
GROUP BY CO.COID||'-'||CO.COSEQ,
    DEP.DEPARTURE_ID,
    TRUNC(CO.INSERTDTM),
    TRUNC(COROW.ACKSHIPDATE),
    PARTY.NAME1,
    PARTY.COUNTRYCODE,
    PARTY.POSTCODE,
    PARTY.ADR1,
    PARTY.CITY,
    TRUNC(DEP.LOADFINISHDTM) 
ORDER BY DEPARTURE_ID,
    COID_COSEQ

