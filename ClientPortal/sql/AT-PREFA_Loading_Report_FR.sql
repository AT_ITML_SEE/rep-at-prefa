/******************************************************************************
NAME: AT-PREFA_Loading_Report_FR.rpt
SUBREPORT: -
DESC: Customized loadin report for Hebting supplier

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		14.11.2022      DAKORNHA    IMI-2693        created

******************************************************************************/

SELECT 
    pbrow.coid
    , pbrow.coseq
    , dep.departure_id
    , party.name1
    , party.name2
    , party.adr1
    , party.adr2
    , party.postcode
    , party.city
    , cartyp.cartypid
    , cartyp.cartypid2
    , car.tothgt*100    AS height
    , cartyp.width*100  AS width
    , cartyp.length*100 AS length
    , COUNT(DISTINCT NVL(pbcar.consolidation_to_carid, pbcar.carid)) AS count_LC
    , SUM(
        CASE
            WHEN pak.pakid = 'Rol'
            THEN rcvcarlog.totwgt
            ELSE pbrow.pickqty * pak.weight
        END
        )  + cartyp.weight AS weight
FROM dep
INNER JOIN pbrow ON
    dep.departure_id = pbrow.departure_id
INNER JOIN pbcar ON
    pbrow.pbcarid = pbcar.pbcarid
INNER JOIN car ON
    NVL(pbcar.consolidation_to_carid, pbcar.carid) = car.carid
    AND dep.whid = car.whid
    AND pbrow.company_id = car.company_id
INNER JOIN party ON
    pbrow.shiptopartyid = party.party_id
    AND pbrow.shiptopartyqualifier = party.party_qualifier
INNER JOIN pak ON
    pbrow.artid = pak.artid
    AND pbrow.pakid = pak.pakid
    AND pbrow.company_id = pak.company_id
INNER JOIN cartyp ON
    car.cartypid = cartyp.cartypid
LEFT JOIN rcvcarlog ON
    pbrow.iteid = rcvcarlog.carid
    AND dep.whid = rcvcarlog.whid
WHERE
    dep.whid = 'PRE1'
    AND pbrow.company_id = 'AT-PREFA'
    AND party.party_qualifier = 'CU'
    AND party.countrycode = 'FR'
    AND dep.departure_id = '{?DEPARTURE_ID}'
GROUP BY 
    pbrow.coid
    , pbrow.coseq
    , dep.departure_id
    , party.name1
    , party.name2
    , party.adr1
    , party.adr2
    , party.postcode
    , party.city
    , cartyp.cartypid
    , cartyp.cartypid2
    , car.tothgt
    , cartyp.width
    , cartyp.length
    , cartyp.weight
UNION
SELECT 
    pbrow.coid
    , pbrow.coseq
    , dep.departure_id
    , party.name1
    , party.name2
    , party.adr1
    , party.adr2
    , party.postcode
    , party.city
    , cartyp.cartypid
    , cartyp.cartypid2
    , cartyp.height*100 AS height
    , cartyp.width*100  AS width
    , cartyp.length*100 AS length
    , COUNT(DISTINCT NVL(pbcar.consolidation_to_carid, pbcar.carid)) AS count_LC
    , SUM(
        CASE
            WHEN pak.pakid = 'Rol'
            THEN rcvcarlog.totwgt
            ELSE pbrow.pickqty * pak.weight
        END
        )  + cartyp.weight AS weight
FROM deplog dep
INNER JOIN pbrowlog pbrow ON
    dep.departure_id = pbrow.departure_id
INNER JOIN pbcarlog pbcar ON
    pbrow.pbcarid = pbcar.pbcarid
INNER JOIN cartrc car ON
    NVL(pbcar.consolidation_to_carid, pbcar.carid) = car.carid
    AND dep.whid = car.whid
    AND dep.departure_id = car.departure_id
    AND pbrow.company_id = car.company_id
INNER JOIN party ON
    pbrow.shiptopartyid = party.party_id
    AND pbrow.shiptopartyqualifier = party.party_qualifier
INNER JOIN pak ON
    pbrow.artid = pak.artid
    AND pbrow.pakid = pak.pakid
    AND pbrow.company_id = pak.company_id
INNER JOIN cartyp ON
    car.cartypid = cartyp.cartypid
LEFT JOIN rcvcarlog ON
    pbrow.iteid = rcvcarlog.carid
    AND dep.whid = rcvcarlog.whid
WHERE
    dep.whid = 'PRE1'
    AND pbrow.company_id = 'AT-PREFA'
    AND party.party_qualifier = 'CU'
    AND party.countrycode = 'FR'
    AND dep.departure_id = '{?DEPARTURE_ID}'
    AND NOT EXISTS 
        (SELECT 'X' 
            FROM PBCAR PB 
            WHERE PB.COMPANY_ID = 'AT-PREFA' 
                AND PB.PBCARID = PBCAR.PBCARID)
GROUP BY 
    pbrow.coid
    , pbrow.coseq
    , dep.departure_id
    , party.name1
    , party.name2
    , party.adr1
    , party.adr2
    , party.postcode
    , party.city
    , cartyp.cartypid
    , cartyp.cartypid2
    , cartyp.height
    , cartyp.width
    , cartyp.length
    , cartyp.weight