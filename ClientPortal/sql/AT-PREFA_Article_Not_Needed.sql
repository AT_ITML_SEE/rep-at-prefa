/******************************************************************************
NAME: AT-PREFA_Article_Not_Needed.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select stor.wpadr "Location",stor.artid "ArtikelNo", stor.Amount "Amount" ,orders.artid from
(select wpadr,artid , count( wpadr) Amount from car, ite 
  where car.company_id = 'AT-PREFA' 
    and car.whid = 'PRE1' 
    and wsid IN ('PLP','PRP') -- Long goods = 'PLP', High racks: 'PRP'
    and noitems <> 0 
    and ite.carid = car.carid
  Group by artid, wpadr) 
   stor,
    (select artid from
      (select artid from corow where company_id = 'AT-PREFA' )
    group by artid) orders
where orders.artid (+) = stor.artid
and orders.artid is null
order by wpadr