/* Formatted on 23/03/2022 07:20:51 (QP5 v5.336) */
/******************************************************************************
NAME: AT-PREFA_Error_at_balance_check.rpt
DESC: 
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		22.03.2022 		DAKORNHA     PREFA-99 	    created

******************************************************************************/
SELECT coid       Auftragsnr,
        coseq      Seq,
        copos      Posnr,
        artid      ArtNr,
        ordqty     Bestellmenge,
        pakid      Einheit
    FROM corow
    WHERE company_id = 'AT-PREFA' 
        AND corowstat = 'E'
ORDER BY 1