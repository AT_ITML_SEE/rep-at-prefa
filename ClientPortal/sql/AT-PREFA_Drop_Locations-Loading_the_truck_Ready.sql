/******************************************************************************
NAME: AT-PREFA - Drop Locations / Loading the truck.rpt
DESC: 
REVISIONS:

Ver 		Date      Player       SN/JIRA 		Description

 
1.0 	2021/12/06	 DAROGALS     PREFA-64 		initial version
1.1 	2022/01/05	 UJABCZYS     PREFA-64 		corrected Party join, added carid to rank method, excluded child LC's from the report, displying Master LC instead, joined CAR to main query to match CARTYPID, added consolidated PBTYPE


******************************************************************************/


SELECT A.RANKING, A.COID, A."Ship-to Name", A."Ship-to postal code", A."Ship-to City", C.CARTYPID "Load Carrier Type",  A.SSCC,  A."Outbound Gate Location",  A."Drop location",  A."Departure ID" 

FROM

(
SELECT DISTINCT
ROW_NUMBER () OVER (PARTITION BY PBC.COID_SINGLE, PBC.CARID /*1.1*/ ORDER BY PBC.CARID DESC) RANKING, 
 
PBR.COID,
P.NAME1  || P.NAME2  "Ship-to Name",
P.POSTCODE "Ship-to postal code",
P.CITY "Ship-to City",
CAR.CARTYPID "Load Carrier Type",
CASE WHEN CAR.CARCARID IS NOT NULL THEN CAR.CARCARID ELSE PBC.CARID END "SSCC", /*1.1*/
PBC.TOWPADR "Outbound Gate Location",
PBC.WWS_DROPCODE_SHIP "Drop location",
PBC.DEPARTURE_ID "Departure ID"


FROM


PBCAR PBC
LEFT JOIN PARTY P ON P.COMPANY_ID = PBC.COMPANY_ID AND P.PARTY_QUALIFIER = PBC.SHIPTOPARTYQUALIFIER AND P.PARTY_ID = PBC.SHIPTOPARTYID  /*1.1*/
INNER JOIN PBROW PBR ON PBR.COMPANY_ID = PBC.COMPANY_ID AND PBR.PBCARID = PBC.PBCARID
INNER JOIN CAR ON CAR.COMPANY_ID = PBC.COMPANY_ID AND CAR.CARID = PBC.CARID
INNER JOIN DEP ON DEP.WHID = PBC.WHID AND DEP.DEPARTURE_ID = PBC.DEPARTURE_ID AND DEPARTURE_PICKSTAT in ( 'F',  'R') 
AND DEP.DEPARTURE_ID LIKE  '%' ||  '{?DEPARTURE_ID}' || '%'
AND TO_CHAR(DEP.DEPARTURE_DTM,'YYYY-MM-DD') LIKE   '%' ||  '{?DATE_A}' || '%' 
--AND DEP.DEPARTURE_ID LIKE '%' ||  '380803' || '%'
--AND TO_CHAR(DEP.DEPARTURE_DTM) LIKE '%' || '' || '%' 
WHERE PBC.COMPANY_ID = 'AT-PREFA'   AND PBC.PIKWEIGHT > 0 AND PBC.PBTYPE IN ('C', 'P', 'S')  /*1.1*/

) A 
INNER JOIN CAR C ON A.SSCC = C.CARID  /*1.1*/
WHERE A.RANKING = '1'

ORDER BY 5 ASC