/******************************************************************************
NAME: AT-PREFA_Authorized_Employee_for_cancellations.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		28.09.2022      ANDREWEB 		            created
1.1         27.10.2022      DAKORNHA    IMI-2672        improvements

******************************************************************************/

SELECT 
    pbrow.pbheadid
    , pbrow.coid
    , pbrow.coseq
    , pbrow.copos
    , pbrow.artid
    , pbrow.wpadr
    , pbrow.wpadr2
    , pbrow.iteid
    , corow.ordqty
    , pbrow.ordqty
    , pbrow.pickqty
    , pbrow.empid
    , pbrow.wws_authorized_user
    , pbrow.pickdtm
FROM pbrow
INNER JOIN corow ON
    pbrow.coid = corow.coid
    AND pbrow.coseq = corow.coseq
    AND pbrow.copos = corow.copos
    AND pbrow.company_id = corow.company_id
    AND pbrow.whid = corow.whid
WHERE 
    pbrow.company_id = 'AT-PREFA'
    AND pbrow.whid = 'PRE1'
    AND pbrow.pickdtm BETWEEN TRUNC({?PICKDTM_FROM}) AND TRUNC({?PICKDTM_TO})+1
    AND pbrow.wws_authorized_user IS NOT NULL
UNION ALL
SELECT 
    pbrow.pbheadid
    , pbrow.coid
    , pbrow.coseq
    , pbrow.copos
    , pbrow.artid
    , pbrow.wpadr
    , pbrow.wpadr2
    , pbrow.iteid
    , corow.ordqty
    , pbrow.ordqty
    , pbrow.pickqty
    , pbrow.empid
    , pbrow.wws_authorized_user
    , pbrow.pickdtm
FROM pbrowlog pbrow
INNER JOIN corowtrc corow ON
    pbrow.coid = corow.coid
    AND pbrow.coseq = corow.coseq
    AND pbrow.copos = corow.copos
    AND pbrow.company_id = corow.company_id
    AND pbrow.whid = corow.whid
WHERE 
    pbrow.company_id = 'AT-PREFA'
    AND pbrow.whid = 'PRE1'
    AND pbrow.pickdtm BETWEEN TRUNC({?PICKDTM_FROM}) AND TRUNC({?PICKDTM_TO})+1
    AND pbrow.wws_authorized_user IS NOT NULL