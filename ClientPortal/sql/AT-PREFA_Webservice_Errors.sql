/******************************************************************************
NAME: AT-PREFA_Webservice_Errors.rpt
SUBREPORT: -
DESC: shows specific webservice errors 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		01.06.2022 		DAKORNHA    IMI-2247        created

******************************************************************************/

SELECT HAPITRANS.HAPITRANS_ID,
    HAPI_SHIPMENT_REPORT_LINE.CUSTOMERORDERNUMBER,
    HAPI_SHIPMENT_REPORT_LINE.CUSTOMERORDERSEQUENCE,
    HAPITRANS.LASTSNDDTM,
    HAPITRANS.HAPIERRMSG
  FROM HAPITRANS
  JOIN HAPI_SHIPMENT_REPORT_LINE
    ON HAPI_SHIPMENT_REPORT_LINE.HAPITRANS_ID = HAPITRANS.HAPITRANS_ID
 WHERE HAPITRANS.COMPANY_ID = 'AT-PREFA' 
    AND (HAPITRANS.HAPIERRMSG LIKE '%endpoint%'
       OR HAPITRANS.HAPIERRMSG LIKE '%ResourceException during doBdeCall:%'
       OR HAPITRANS.HAPIERRMSG LIKE '%has exceeded the allotted timeout of%')
       group by 
    HAPITRANS.HAPITRANS_ID,
    HAPI_SHIPMENT_REPORT_LINE.CUSTOMERORDERNUMBER,
    HAPI_SHIPMENT_REPORT_LINE.CUSTOMERORDERSEQUENCE,
    HAPITRANS.LASTSNDDTM,
    HAPITRANS.HAPIERRMSG