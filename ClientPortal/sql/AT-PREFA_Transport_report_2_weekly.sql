/* Formatted on 01/03/2022 13:02:21 (QP5 v5.336) */
/******************************************************************************
NAME: AT-PREFA_Transport_Report_2_weekly.rpt
DESC: will be used to plan transports between Land transport and Logistics
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description


1.0 		22.03.2022 		DAKORNHA     PREFA-97 	    created
1.1 		24.03.2022 		DAKORNHA     PREFA-97 	    added COROW.CANCELDTM IS NULL / ORDQTY > 0
                                                        when SUM(ESTWEIGHT) = 0 then weight shall be calculated from BASEPAKID
                                                        ADR renamed to INFO - if dangerous goods then 'ADR' (instead of 'x')
                                                                            - if ART.ARTNAME2 = 'SRI' then 'SAUM'
                                                                            - in case both is true, show 'ADR' first 
1.2 		31.03.2022 		DAKORNHA     PREFA-97 	    added DEP.DEPARTURE_PICKSTAT 'H' (On Hold)
1.3 		21.04.2022 		DAKORNHA     PREFA-97 	    added MAX(COROW.ACKSHIPDATE) / refactoring
1.4 		01.06.2022 		DAKORNHA     IMI-2202 	    add translation for load carrier types BD2(=CO) and 6MP(=OP)
1.5         18.07.2022      DCOVALSC     IMI-2433       add (ART.ARTGROUP in ('TRA', 'GFG')/expanding WHEN function ARTNAME = TRA or TRASAUM
1.6         31.10.2022      DAKORNHA     IMI-2788       created out ot AT-PREFA_Transport_Report_2.rpt
1.7         11.01.2023      DAKORNHA     IMI-2345       exclude AT-PREFA-GLS method of shipments
1.8         30.01.2023      PBRUGGER     IMI-2886       Change of DEPARTURE_DTM to CREATION_DTM

******************************************************************************/

SELECT 
    NVL(CO.COID, COWORK.COID)
        ||'-'||NVL(CO.COSEQ, COWORK.COSEQ) COID_COSEQ,
    COWORK.CONSIGNMENT_ID CONSIGNMENT_ID,
    TRUNC(CO.INSERTDTM) CREATION_DTM,
    PARTY.NAME1 SHIP_TO_PARTY_NAME,
    PARTY.COUNTRYCODE SHIP_TO_PARTY_COUNTRYCODE,
    PARTY.POSTCODE SHIP_TO_PARTY_ZIP,
    PARTY.CITY SHIP_TO_PARTY_CITY,
    PARTY.ADR1 SHIP_TO_PARTY_ADR1,
    CASE WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) < 0.01 THEN 0.01
        ELSE SUM(COROW.ORDQTY / PAK.PALLET_CALC)
    END SUM_LC,
    DECODE (CARTYP.CARTYPID2
               , 'G01'  , 'BE'
               , 'G02'  , 'BE'
               , 'G03'  , 'BE'
               , 'G04'  , 'BE'
               , 'G05x' , 'EW'
               , 'EWP'  , 'XP'
               , 'BD3'  , 'CO'
               , 'BD6'  , 'CO'
               , 'KRT'  , 'KT'
               , 'K04'  , 'KT'
               , 'K03'  , 'KT'
               , 'WAMAS', 'KT'
               , 'BD2', 'CO'
               , '6MP', 'OP'
            , 'PA') LC_DESC,
    CASE WHEN
            CASE 
                WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.25 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.2 * CARTYP.LOADING_METER
                WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.25 
                    AND SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.5 * CARTYP.LOADING_METER
                WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * CARTYP.LOADING_METER
            END  < 0.01
        THEN 0.01
        ELSE 
            CASE 
                WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.25 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.2 * CARTYP.LOADING_METER
                WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.25 
                    AND SUM(COROW.ORDQTY / PAK.PALLET_CALC) <= 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * 0.5 * CARTYP.LOADING_METER
                WHEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) > 0.5 THEN SUM(COROW.ORDQTY / PAK.PALLET_CALC) * CARTYP.LOADING_METER
            END
    END LDM,
    SUM(DECODE(COROW.ESTWEIGHT, 0, (PAK.BASPAKWEIGHT*COROW.ORDQTY), COROW.ESTWEIGHT)) WEIGHT,
    CARTYP.LENGTH LENGTH,
    CARTYP.WIDTH WIDTH,
    CASE WHEN SUM(COROW.ESTVOLUME) < 0.01 THEN 0.01
        ELSE SUM(COROW.ESTVOLUME)
    END VOL,
    CASE WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%GFGSAUM%' THEN 'ADR, SAUM' 
        WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%GFG%' THEN 'ADR' 
        WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%SRI%' THEN 'SAUM'
        WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%TRASAUM%' THEN 'TRA, SAUM' 
        WHEN ART.ARTGROUP||ART.ARTNAME2 LIKE '%TRA%' THEN 'TRA' 
        ELSE NULL 
    END INFO,
    DEP.DEPARTURE_ID,
    PARTY.PHONE CONTACT,
    CO.CO_REF REFERENCE,
    MAX(COROW.ACKSHIPDATE) ACKSHIPDATE
FROM DEP
JOIN COWORK -- NVL due to bug with Parent-Child LC
    ON COWORK.DEPARTURE_ID = DEP.DEPARTURE_ID
    AND COWORK.WHID = DEP.WHID
JOIN CO -- NVL due to bug with Parent-Child LC
    ON CO.COID = COWORK.COID
        AND CO.COSEQ = COWORK.COSEQ
        AND CO.COMPANY_ID = COWORK.COMPANY_ID     
JOIN COROW -- NVL due to bug with Parent-Child LC
    ON COROW.COID = COWORK.COID
        AND COROW.COSEQ = COWORK.COSEQ
        AND COROW.COMPANY_ID = COWORK.COMPANY_ID
JOIN PARTY
       ON PARTY.PARTY_QUALIFIER = COWORK.SHIPTOPARTYQUALIFIER
        AND PARTY.PARTY_ID = COWORK.SHIPTOPARTYID                    
        AND PARTY.COMPANY_ID = COWORK.COMPANY_ID
LEFT JOIN ART
    ON ART.ARTID = COROW.ARTID
    AND ART.COMPANY_ID = COROW.COMPANY_ID
    AND (ART.ARTGROUP in ('TRA', 'GFG')
        OR UPPER(ART.ARTNAME2) = 'SRI')
JOIN (SELECT PAK.ARTID,
            PAK.PAKID,
            PAK.PAKNAME,
            BASPAK.PAKID BASPAKPAKID,
            BASPAK.BASEQTY BASPAKBASEQTY,
            BASPAK.WEIGHT BASPAKWEIGHT,
            PAK.BASEQTY,
            PAK.WEIGHT,
            DECODE(PAK.PAKID
                    , 'g'   , 200
                    , 'mm'  , 200
                    , 'Stk' , 200
                , PAK.BASEQTY) PALLET_CALC,
            DECODE(PAK.PAKID
                    , 'Box' , 'G05x'
                    , 'g'   , 'G05x'
                    , 'KDV' , 'G05x'
                    , 'K06' , 'G05x'
                    , 'K07' , 'G05x'
                    , 'mm'  , 'G05x'
                    , 'PAL' , 'G05x'
                    , 'Rol' , 'G05x'
                    , 'Stk' , 'G05x'
                , PAK.PAKID) CARTYPID,
            RANK () OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) AS LVL
        FROM PAK
        JOIN (SELECT ARTID,
                    PAKID,
                    COMPANY_ID,
                    BASEQTY,
                    WEIGHT
                FROM PAK
                WHERE PAK.COMPANY_ID = 'AT-PREFA') BASPAK
            ON BASPAK.ARTID = PAK.ARTID
            AND BASPAK.COMPANY_ID = PAK.COMPANY_ID               
            AND BASPAK.PAKID = PAK.BASPAKID
        WHERE PAK.COMPANY_ID = 'AT-PREFA') PAK
    ON PAK.ARTID = COROW.ARTID
        AND PAK.LVL = 1
JOIN CARTYP
    ON CARTYP.CARTYPID2 = PAK.CARTYPID
WHERE COWORK.NO_RESTROWS <> COWORK.NOROWS -- no full cancelled orders
    AND DEP.WHID = 'PRE1'
    AND DEP.DEPARTURE_PICKSTAT in ('C', 'H')
    AND DEP.DLVRYMETH_ID NOT IN ('AT-PREFA-PICKUP', 'AT-PREFA-GLS')
    AND COWORK.COMPANY_ID = 'AT-PREFA'
    AND COROW.CANCELDTM IS NULL
    AND COROW.ORDQTY > 0
    AND TRUNC(DEP.DEPARTURE_DTM) BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+7)
GROUP BY NVL(CO.COID, COWORK.COID)
        ||'-'||NVL(CO.COSEQ, COWORK.COSEQ),
    COWORK.CONSIGNMENT_ID,
    TRUNC(CO.INSERTDTM),
    PARTY.NAME1,
    PARTY.COUNTRYCODE,
    PARTY.POSTCODE,
    PARTY.CITY,
    PARTY.ADR1,
    DECODE (CARTYP.CARTYPID2
               , 'G01'  , 'BE'
               , 'G02'  , 'BE'
               , 'G03'  , 'BE'
               , 'G04'  , 'BE'
               , 'G05x' , 'EW'
               , 'EWP'  , 'XP'
               , 'BD3'  , 'CO'
               , 'BD6'  , 'CO'
               , 'KRT'  , 'KT'
               , 'K04'  , 'KT'
               , 'K03'  , 'KT'
               , 'WAMAS', 'KT'
               , 'BD2', 'CO'
               , '6MP', 'OP'
            , 'PA'),
    CARTYP.LOADING_METER,
    CARTYP.LENGTH,
    CARTYP.WIDTH,
    ART.ARTGROUP,
    ART.ARTNAME2,
    DEP.DEPARTURE_ID,
    PARTY.PHONE,
    CO.CO_REF
ORDER BY PARTY.COUNTRYCODE,
    PARTY.POSTCODE,
    PARTY.NAME1,
    NVL(CO.COID, COWORK.COID)
        ||'-'||NVL(CO.COSEQ, COWORK.COSEQ),
    TRUNC(CO.INSERTDTM)