/******************************************************************************
NAME: AT-PREFA_Product_History.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT 
        a.company_id Company,
        'Inbound' Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1 ARTNAME,
          A.ASNINID OrderNumber,
          '' Item_Load,
          A.CREATEDTM Transaction_Date,
          '' Storage_Location,
 'Inb' Transaction_type_short,
          'Open' Transaction_Comment,
          A.ORDQTY,
          a.baspakid,
          A.ARRQTY original_quantity,
          0 updated_quantity,
          SUPID PART,
          '' PARTNAME,
          A.BLOCKCOD Holdcode,
          a.ownid OWNID,
         '' user1
FROM      RCVROW A,
          ART C,
          COMPANY e
WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND a.artid = c.artid
          and c.artid ='{?ARTID}'
          and e.company_id='AT-PREFA'
          
UNION ALL          
--WE LOG

SELECT 
        a.company_id Company,
        'Inbound' Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1 ARTNAME,
          A.ASNINID OrderNumber,
          A.ITEID Item_Load,
          A.ARRDTM Transaction_Date,
          'PIN-PIN' Storage_Location,
		  'Inb' Transaction_type_short,
          'Inbound Received' Transaction_Comment,
          A.DELQTY STORQTY,
          a.pakid,
          0 original_quantity,
          A.DELQTY updated_quantity,
          SUPID PART,
          '' PARTNAME,
          A.BLOCKCOD Holdcode,
          a.ownid OWNID,
          a.proid user1
FROM      DELLOGROW A,
          ART C,
          COMPANY e
WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND a.artid = c.artid
          and c.artid ='{?ARTID}'
          and e.company_id='AT-PREFA'
          
Union ALL
 --WA         
SELECT 
        a.company_id Company,
        'Outbound' Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1 ARTNAME,
          A.COID || ' - ' || A.COSEQ OrderNumber,
          A.ITEID Item_Load,
          A.PICKDTM Transaction_Date,
          CASE WHEN A.WPADR2 IS NULL THEN A.WSID || ' ' || A.WPADR ELSE A.WSID || ' ' || A.WPADR2 END Storage_Location,
		  'WA' Transaction_type_short,
          A.PBHEADID Transaction_Comment,
          CASE WHEN a.pakid = b.BASPAKID THEN a.pickqty ELSE a.pickqty * NVL (b.baseqty, 0) END STORQTY,
          b.BASPAKID PAKID,
          (a.pickqty)  original_quantity,
        CASE WHEN a.pakid = b.BASPAKID THEN a.pickqty  ELSE a.pickqty * NVL (b.baseqty, 0) END * -1 updated_quantity,
          a.SHIPTOPARTYID PART,
          a.SHIPTOPARTY_NAME1 PARTNAME,
          a.BLOCKCOD Holdcode,
          a.ownid OWNID,
          a.EMPID user1
FROM      PBROWLOG A,
          ART C,
          PAK B,
          COMPANY e
WHERE     A.COMPANY_ID = e.COMPANY_ID
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ARTID = B.ARTID(+)
          AND A.COMPANY_ID = B.COMPANY_ID(+)
          AND a.pakid = b.pakid(+)
          AND C.artid ='{?ARTID}'
          and e.company_id='AT-SEISENB'
Union ALL          
   --Item Change

SELECT 
        a.company_id Company,
          'ItemChange' Transaction_type,
          A.ARTID Article_No,
          C.ARTNAME1 ARTNAME,
		  '' OrderNumber,
		  A.ITEID Item_Load,
		  A.UPDDTM Transaction_Date,
		  CASE WHEN A.PPKEY IS NULL THEN '' ELSE NVL (B.WSID || ' ' || B.WPADR, 'PP RADERAD') END Storage_Location,
		  D.ITECHGCODNAME Transaction_type_short,
		  A.ITECHGTXT Transaction_Comment,
		  NVL (A.STORQTY2, 0) - NVL (A.STORQTY, 0) STORQTY,
          a.pakid pakid,
          NVL (a.storqty, 0) original_quantity,
		  NVL (a.storqty2, 0) updated_quantity,
		  '' PART,
		  '' PARTNAME,
		  A.BLOCKCOD2 Holdcode,
		  a.OWNID OWNID,
		  A.EMPID user1          
FROM      ITECHG A,
          ART C,
          PP B,
          ITECHGCOD D,
          COMPANY e
WHERE A.COMPANY_ID = e.COMPANY_ID
          AND A.PPKEY = B.PPKEY(+)
          AND A.ARTID = C.ARTID
          AND A.COMPANY_ID = C.COMPANY_ID
          AND A.ITECHGCODID = D.ITECHGCODID
          And A.artid ='{?ARTID}'
          and e.company_id='AT-PREFA'
UNION ALL
     --Movement
SELECT 
    T.company_id Company,
    'Movement' Transaction_type ,
    I.artid Article_No,
    I.artname ARTNAME,
    --T.TRPORDID 
    '' OrderNumber,
    I.ITEID Item_Load,
    T.UPDDTM Transaction_Date,
    'FROM: '||T.FRMWPADR Storage_Location,
    'Movement' Transaction_type_short,
    'TO: '||T.TOWPADR Transaction_Comment ,
    T.ITESTORQTY STORQTY,
    I.PAKID PAKID,
    T.ITESTORQTY original_quantity,
    0 updated_quantity,
    '' PART,
    '' PARTNAME,
    I.BLOCKCOD Holdcode,
    T.ownid OWNID,
    T.EMPID user1
FROM
    trplog T, 
    itetrc I 
WHERE
    T.carid =I.carid and 
    T.company_id = 'AT-PREFA' and 
    I.artid ='{?ARTID}'
    

UNION ALL
     --Stock
     

SELECT 
    I.company_id Company,
    'Balance' Transaction_type ,
    I.artid Article_No,
    I.artname ARTNAME,
    --T.TRPORDID 
    '' OrderNumber,
    I.ITEID Item_Load,
    I.UPDDTM Transaction_Date,
    T.WPADR Storage_Location,
    'Balance' Transaction_type_short,
    'Area: '||T.WSID Transaction_Comment ,
    I.STORQTY STORQTY,
    '' PAKID,
    I.STORQTY original_quantity,
    0 updated_quantity,
    '' PART,
    '' PARTNAME,
    I.BLOCKCOD Holdcode,
    I.ownid OWNID,
    T.EMPID user1
FROM
    car T, 
    ite I 
WHERE
    T.carid =I.carid and 
    T.company_id = 'AT-PREFA' and 
    I.artid ='{?ARTID}'

ORDER BY
 Transaction_date