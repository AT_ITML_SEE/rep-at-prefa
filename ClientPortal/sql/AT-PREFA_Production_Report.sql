/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/


  SELECT DTM,
         TIMME,
         SUM (PLOCKRADER) "TOT PLOCKRADER",
         SUM (BPPLOCK) "BPPLOCK",
         SUM (EXPPLOCK) "EXPPLOCK",
         SUM (L�NGPLOCK) "L�NGPLOCK",
         SUM (EHANDEL) "EHANDEL",
         SUM (LBA) "LBA",
         SUM (KUNDORDER) "KUNDORDER",
         SUM (KUNDORDERRADER) "KUNDORDERRADER"
    FROM (  /***************************************************************************
                                          Start TOTALT
             ***************************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   COUNT (TOTALT) "PLOCKRADER",
                   0 "BPPLOCK",
                   0 "EXPPLOCK",
                   0 "L�NGPLOCK",
                   0 "EHANDEL",
                   0 "LBA",
                   0 "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( /**********************************
                             TOTALT fr�n historik
                     ***********************************/
                    SELECT DISTINCT A.PBROWID "TOTALT",
                                    0 "BPPLOCK",
                                    0 "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                    -- GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --        TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /**********************************
                         TOTALT fr�n aktiv tabell
                    ***********************************/

                    SELECT DISTINCT A.PBROWID "TOTALT",
                                    0 "BPPLOCK",
                                    0 "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}' 
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd'))
          GROUP BY DTM, TIMME
          -- GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --      TO_CHAR (A.PICKDTM, 'HH24')


          /*********************************************************************
                                        Slut TOTALT
          **********************************************************************/

          UNION ALL
            /*********************************************************************
                                          Start DGR
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   COUNT (BPPLOCK) "BPPLOCK",
                   0 "EXPPLOCK",
                   0 "L�NGPLOCK",
                   0 "EHANDEL",
                   0 "LBA",
                   0 "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( /******************************
                              LSC fr�n historik
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    A.PBROWID "BPPLOCK",
                                    0 "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPADR'
                           --AND a.WSID = 'HP1'
                    --  GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --         TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /******************************
                          LSC fr�n aktiv tabell
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    A.PBROWID "BPPLOCK",
                                    0 "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')  
                           AND A.PZID = 'PPADR'
                           --AND a.WSID = 'HP1'
						   )
          GROUP BY DTM, TIMME
          -- GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --    TO_CHAR (A.PICKDTM, 'HH24')

          /*********************************************************************
                                        Slut ADR
          *********************************************************************/

          UNION
            /*********************************************************************
                                          Start Wamas
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   0 "BPPLOCK",
                   COUNT (EXPPLOCK) "EXPPLOCK",
                   0 "L�NGPLOCK",
                   0 "EHANDEL",
                   0 "LBA",
                   0 "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( /******************************
                            LMS fr�n historik
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "BPPLOCK",
                                    A.PBROWID "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPW'
                           --AND a.WSID = 'HP1'
                           AND a.PBCARID NOT IN (SELECT PBCARID
                                                   FROM LOC_LM_ZONZON_PBCARID_V2C_V_T)
                    --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --       TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /******************************
                         LMS fr�n aktiv tabell
                    ******************************/


                    SELECT DISTINCT 0 "TOTALT",
                                    0 "BPPLOCK",
                                    A.PBROWID "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPW'
                           --AND a.WSID = 'HP1'
                           /*AND a.PBCARID NOT IN (SELECT PBCARID
                                                   FROM LOC_LM_ZONZON_PBCARID_V2C_V)*/)
          GROUP BY DTM, TIMME
          --  GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --         TO_CHAR (A.PICKDTM, 'HH24')

          /*********************************************************************
                                        Slut Wamas
          *********************************************************************/

          UNION
            /*********************************************************************
                                          Start Long
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   0 "BPPLOCK",
                   0 "EXPPLOCK",
                   COUNT (L�NGPLOCK) "L�NGPLOCK",
                   0 "EHANDEL",
                   0 "LBA",
                   0 "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( /******************************
                            ZON fr�n historik
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "BPPLOCK",
                                    0 "EXPPLOCK",
                                    A.PBROWID "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPL' 
                           --AND a.WSID = 'HLG'
                           /*AND a.PBCARID IN (SELECT PBCARID
                                               FROM LOC_LM_ZONZON_PBCARID_V2C_V_T)*/
                    --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --       TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /******************************
                         ZON fr�n aktiv tabell
                    ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "BPPLOCK",
                                    0 "EXPPLOCK",
                                    A.PBROWID "L�NGPLOCK",
                                    0 "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPL'
                           --AND a.WSID = 'HLG'
                           /*AND a.PBCARID IN (SELECT PBCARID
                                               FROM LOC_LM_ZONZON_PBCARID_V2C_V)*/)
          GROUP BY DTM, TIMME
          --  GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --         TO_CHAR (A.PICKDTM, 'HH24')

          /*********************************************************************
                                        Slut Long
          *********************************************************************/

          UNION
            /*********************************************************************
                                          Start Regal
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   0 "BPPLOCK",
                   0 "EXPPLOCK",
                   0 "L�NGPLOCK",
                   COUNT (EHANDEL) "EHANDEL",
                   0 "LBA",
                   0 "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( /******************************
                           LBZ fr�n historik
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "LSC",
                                    0 "LMS",
                                    0 "ZON",
                                    A.PBROWID "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPR' 
                    --  GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --         TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /******************************
                         LBZ fr�n aktiv tabell
                    ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "BPPLOCK",
                                    0 "EXPPLOCK",
                                    0 "L�NGPLOCK",
                                    A.PBROWID "EHANDEL",
                                    0 "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}' 
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'PPR')
          GROUP BY DTM, TIMME
          --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --       TO_CHAR (A.PICKDTM, 'HH24')
          /*********************************************************************
                                        Slut LBZ
          *********************************************************************/

          UNION
            /*********************************************************************
                                          Start LBA
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   0 "LCS",
                   0 "LMS",
                   0 "ZON",
                   0 "LBZ",
                   COUNT (LBA) "LBA",
                   0 "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( /******************************
                           LBA fr�n historik
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "LSC",
                                    0 "LMS",
                                    0 "ZON",
                                    0 "LBZ",
                                    A.PBROWID "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'LBA'
                    --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --       TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /******************************
                         LBA fr�n aktiv tabell
                    ******************************/

                    SELECT DISTINCT 0 "TOTALT",
                                    0 "LSC",
                                    0 "LMS",
                                    0 "ZON",
                                    0 "LBZ",
                                    A.PBROWID "LBA",
                                    0 "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                           AND A.PZID = 'LBA')
          GROUP BY DTM, TIMME
          --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --       TO_CHAR (A.PICKDTM, 'HH24')
          /*********************************************************************
                                        Slut LBA
          *********************************************************************/

          UNION
                       /*********************************************************************
                                        Start KUNDORDER
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   0 "LCS",
                   0 "LMS",
                   0 "ZON",
                   0 "LBZ",
                   0 "LBA",
                   COUNT (KUNDORDER) "KUNDORDER",
                   0 "KUNDORDERRADER"
              FROM ( 
                    select distinct 
                    0 "TOTALT",
                    0 "LSC",
                    0 "LMS",
                    0 "ZON",
                    0 "LBZ",
                    0 "LBA",
                    KUNDORDER,
                    KUNDORDERRADER,
                    Max(DTM) "DTM",
                    max(TIMME) "TIMME"
                    from(/******************************
                         KUNDORDER fr�n historik
                     ******************************/
                    SELECT DISTINCT 0 "TOTALT",
                                    0 "LSC",
                                    0 "LMS",
                                    0 "ZON",
                                    0 "LBZ",
                                    0 "LBA",
                                    A.COID || A.COSEQ "KUNDORDER",
                                    0 "KUNDORDERRADER",
                                    TO_CHAR (A.INSERTDTM, 'yyyy-mm-dd') "DTM",
                                    TO_CHAR (A.INSERTDTM, 'HH24') "TIMME"
                      FROM COWORKLOG a
                     WHERE     A.COMPANY_ID = 'AT-PREFA'
                           AND TO_CHAR (A.INSERTDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                                  AND A.TEXT = 'New Status: 40'
                    --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --       TO_CHAR (A.PICKDTM, 'HH24')

                    )                                  
                                                    
                                  GROUP BY TOTALT,LSC,LMS,ZON,LBZ,LBA,KUNDORDER,KUNDORDERRADER
                                  )
          GROUP BY DTM, TIMME
          --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
          --       TO_CHAR (A.PICKDTM, 'HH24')

          /*********************************************************************
                                     Slut KUNDORDER
          *********************************************************************/


          UNION
            /*********************************************************************
                                    Start KUNDORDERRADER
            *********************************************************************/
            SELECT DTM "DTM",
                   TIMME "TIMME",
                   0 "TOT PLOCKRADER",
                   0 "LCS",
                   0 "LMS",
                   0 "ZON",
                   0 "LBZ",
                   0 "LBA",
                   0 "KUNDORDER",
                   COUNT (KUNDORDERRADER) "KUNDORDERRADER"
              FROM ( /******************************
                       KUNDORDERRADER fr�n historik
                     ******************************/
                    SELECT DISTINCT
                           0 "TOTALT",
                           0 "LSC",
                           0 "LMS",
                           0 "ZON",
                           0 "LBZ",
                           0 "LBA",
                           0 "KUNDORDER",
                           A.COID || A.COSEQ || A.COPOS "KUNDORDERRADER",
                           TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                           TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROWLOG a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                    --GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
                    --       TO_CHAR (A.PICKDTM, 'HH24')

                    UNION
                    /******************************
                    KUNDORDERRADER fr�n aktiv tabell
                    ******************************/

                    SELECT DISTINCT
                           0 "TOTALT",
                           0 "LSC",
                           0 "LMS",
                           0 "ZON",
                           0 "LBZ",
                           0 "LBA",
                           0 "KUNDORDER",
                           A.COID || A.COSEQ || A.COPOS "KUNDORDERRADER",
                           TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') "DTM",
                           TO_CHAR (A.PICKDTM, 'HH24') "TIMME"
                      FROM PBROW a
                     WHERE     A.COMPANY_ID = '{?company_id}'
                           AND A.PICKDTM IS NOT NULL
                           AND TO_CHAR (A.PICKDTM, 'yyyy-mm-dd') =
                                  TO_CHAR (SYSDATE, 'yyyy-mm-dd'))
          GROUP BY DTM, TIMME)
--GROUP BY TO_CHAR (A.PICKDTM, 'yyyy-mm-dd'),
--       TO_CHAR (A.PICKDTM, 'HH24'))
/*********************************************************************
                        Slut KUNDORDERRADER
*********************************************************************/
GROUP BY DTM, TIMME
ORDER BY TIMME

/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: IO
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0        	2020-04-06  	DAELIASS         			Created this SQL.

******************************************************************************/


  SELECT TO_CHAR (dellog.wws_snddtm, 'HH24')     hour,
         0                                       putaways,
         SUM (dellogrow.delqty)                  sum_of_delqty
    FROM dellog
         INNER JOIN dellogrow
             ON     dellogrow.company_id = dellog.company_id
                AND dellog.delid = dellogrow.delid
   WHERE     dellog.company_id = '{?company_id}'
         AND TRUNC (dellog.enddtm) = TRUNC (SYSDATE)
         AND dellog.deltypid <> 'LR'
GROUP BY TO_CHAR (dellog.wws_snddtm, 'HH24')
UNION ALL
  SELECT TO_CHAR (tpl.leaveackdtm, 'HH24')     hour,
         COUNT (tpl.carid)                     putaways,
         0                                     sum_of_delqty
    FROM trpasslog tpl
   WHERE     tpl.trptypid = def.trptypid_storage
         AND tpl.company_id = '{?company_id}'
         AND TRUNC (tpl.leaveackdtm) = TRUNC (SYSDATE)
GROUP BY TO_CHAR (tpl.leaveackdtm, 'HH24')

/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: Returer
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select
    A.DELID,
    count(distinct(B.POID||B.POSEQ||B.POPOS)) "IO_RADER",
    A.ENDDTM,
    to_char(A.ENDDTM,'yyyy-mm-dd'),
    to_char(A.ENDDTM,'HH24'),
    C.EDISTAT
from
    DELLOG a,
    DELLOGROW b,
    EDIOUT c
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and A.DELID = B.DELID
    and to_char(A.ENDDTM,'yyyy-mm-dd') = to_char(sysdate -1,'yyyy-mm-dd')
    and A.WWS_EDIOUTID = C.EDIOUTID
    and A.DELTYPID = 'LR'
group by
    A.DELID,
    A.ENDDTM,
    C.EDISTAT
	
/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: Order i systemet
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select
    to_char(C.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    0 "EXP",
    0 "L�NG",
    count(distinct(c.COID||c.COSEQ||c.COPOS)) "BP",
    0 "EHANDEL",
    0 "Orderrader",
    0 "Orders" ,
    0 "EJ_GENERERAT"
from
    CO a,
    COWORK b,
    COROW c,
    pbrow d
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and C.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COID = C.COID
    and A.COSEQ = C.COSEQ
    and c.coid=d.coid
    and c.coseq=d.coseq
    and c.copos=d.copos
    and d.pzid = 'PPW'
    and c.corowstat < '5'
group by
    to_char(C.FPSHIPDTM,'yyyy-mm-dd')
Union all
select
    to_char(C.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    count(distinct(c.COID||c.COSEQ||c.COPOS)) "EXP",
    0 "L�NG",
    0 "BP",
    0 "EHANDEL",
    0 "Orderrader",
    0 "Orders",
    0 "EJ_GENERERAT"
from
    CO a,
    COWORK b,
    COROW c,
    pbrow d
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and C.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COID = C.COID
    and A.COSEQ = C.COSEQ
    and c.coid=d.coid
    and c.coseq=d.coseq
    and c.copos=d.copos
    and d.pzid = 'PPADR'
    and c.corowstat < '5'
    --and d.PBCARID not in (select PBCARID from LOC_LM_ZONZON_PBCARID_V2_V)
group by
    to_char(C.FPSHIPDTM,'yyyy-mm-dd')
Union All
    select
    to_char(C.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    0 "EXP",
    count(distinct(c.COID||c.COSEQ||c.COPOS)) "L�NG",
    0 "BP",
    0 "EHANDEL",
    0 "Orderrader",
    0 "Orders",
    0 "EJ_GENERERAT"
from
    CO a,
    COWORK b,
    COROW c,
    pbrow d
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and C.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COID = C.COID
    and A.COSEQ = C.COSEQ
    and c.coid=d.coid
    and c.coseq=d.coseq
    and c.copos=d.copos
    and d.pzid = 'PPL'
    and c.corowstat < '5'
group by
    to_char(C.FPSHIPDTM,'yyyy-mm-dd')    
Union all
select
    to_char(C.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    0 "EXP",
    0 "L�NG",
    0 "BP",
    count(distinct(c.COID||c.COSEQ||c.COPOS)) "EHANDEL",
    0 "Orderrader",
    0 "Orders",
    0 "EJ_GENERERAT"
from
    CO a,
    COWORK b,
    COROW c,
    pbrow d
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and C.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COID = C.COID
    and A.COSEQ = C.COSEQ
    and c.coid=d.coid
    and c.coseq=d.coseq
    and c.copos=d.copos 
    and d.pzid = 'PPR'
    and c.corowstat < '5'
group by
    to_char(C.FPSHIPDTM,'yyyy-mm-dd')
    Union all
select
    to_char(C.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    0 "LMS",
    0 "ZONZON",
    0 "LMB",
    0 "LMC",
    count(distinct(c.COID||c.COSEQ||c.COPOS)) "Orderrader",
    0 "Orders",
    0 "EJ_GENERERAT"
from
    CO a,
    COWORK b,
    COROW c,
    pbrow d
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and C.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COID = C.COID
    and A.COSEQ = C.COSEQ
    and c.coid=d.coid
    and c.coseq=d.coseq
    and c.copos=d.copos
    and c.corowstat < '5'
group by
    to_char(C.FPSHIPDTM,'yyyy-mm-dd')
Union all
    select
    to_char(C.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    0 "LMS",
    0 "ZONZON",
    0 "LMB",
    0 "LMC",
    0 "Orderrader",
    count(distinct(A.COID||A.COSEQ)) "Orders",
    0 "EJ_GENERERAT"
from
    CO a,
    COWORK b,
    COROW c,
    pbrow d
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and C.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COID = C.COID
    and A.COSEQ = C.COSEQ
    and c.coid=d.coid
    and c.coseq=d.coseq
    and c.copos=d.copos
    and c.corowstat < '5'
group by
    to_char(C.FPSHIPDTM,'yyyy-mm-dd')
Union all
select
    to_char(A.FPSHIPDTM,'yyyy-mm-dd') "Orderdatum",
    0 "LMS",
    0 "ZONZON",
    0 "LMB",
    0 "LMC",
    0 "Orderrader",
    0 "Orders",
    COUNT (DISTINCT (A.COID || A.COSEQ || A.COPOS))
                      "EJ_GENERERAT"
              FROM COROW a, co b
             WHERE     A.COMPANY_ID = 'AT-PREFA' and
             a.company_id=b.company_id and
             a.coid=b.coid and
             a.coseq=b.coseq
                   AND a.corowstat in('E', '0')
group by
    to_char(A.FPSHIPDTM,'yyyy-mm-dd')

/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: Refills
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0        	2020-04-06  	DAELIASS        			Created this SQL.
1.1        	2021-10-12  	AHEDBLAD        			Updated EH picks to count number of EXP picked

******************************************************************************/

WITH
    refills
    AS
        (SELECT TO_CHAR (trpasslog.leaveackdtm, 'HH24')     "hour",
                1 "PlockatEH",
--                rolog.RFLQTY,
--                PAK.BASEQTY,
--                PAK.PAKID,          
                trpasslog.trpordid roid,                 
                'EH'                               "TYPE"
           FROM trpasslog

          
          WHERE     trpasslog.company_id = 'AT-PREFA' and trpasslog.trptypid = '1'
                AND TRUNC (trpasslog.leaveackdtm) = TRUNC (SYSDATE)
         
                )
  SELECT "hour",
   SUM   ("PlockatEH"),
   COUNT (roid),
    TYPE
    
    FROM refills
GROUP BY "hour", TYPE
ORDER BY "hour", TYPE

/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: BP_loadcarriers_departured
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0        	2020-04-06  	DAELIASS         			Created this SQL.

******************************************************************************/

/*
--SAVED FOR REFERENCE:
SELECT TRUNC (dep.loadfinishdtm) "DATE", pbcar.pbcarid
  FROM pbcar INNER JOIN dep ON dep.departure_id = pbcar.departure_id
 WHERE     pbcar.company_id = '{?company_id}'
       AND pbcar.pzid IN ('HZ1',
                          'HZ1S',
                          'HZ4',
                          'HZ4S')
       AND TRUNC (dep.loadfinishdtm) = TRUNC (SYSDATE)
UNION ALL
SELECT TRUNC (dep.loadfinishdtm), pbcar.pbcarid
  FROM pbcar INNER JOIN dep ON dep.departure_id = pbcar.departure_id
 WHERE     pbcar.company_id = '{?company_id}'
       AND pbcar.carid IN
               (SELECT DISTINCT pbcar.consolidation_to_carid
                  FROM pbcar
                       INNER JOIN dep
                           ON dep.departure_id = pbcar.departure_id
                 WHERE     pbcar.company_id = '{?company_id}'
                       AND pbcar.pzid IN ('HZ1',
                                          'HZ1S',
                                          'HZ4',
                                          'HZ4S')
                       AND TRUNC (dep.loadfinishdtm) = TRUNC (SYSDATE))
       AND TRUNC (dep.loadfinishdtm) = TRUNC (SYSDATE)
*/

SELECT TO_CHAR(dep.loadfinishdtm, 'YYYY-MM-DD') "DATE", pbcar.pbcarid
  FROM pbcar LEFT JOIN dep ON dep.departure_id = pbcar.departure_id
 WHERE     pbcar.company_id = '{?company_id}'
       AND pbcar.pzid IN ('HZ1',
                          'HZ1S',
                          'HZ4',
                          'HZ4S',
                          'HZ9')
       AND dep.loadfinishdtm IS NOT NULL
	   
/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: KonsKvar
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT 
    TO_CHAR (co.insertdtm, 'YYYY-MM-DD') "ORDERDATUM", 
    COUNT(DISTINCT(PBCAR.SHIPTOPARTYID)) AS VALUE,
count(pbcar.pbcarid)
FROM 
    pbcar 
INNER JOIN co ON co.shiptopartyid = pbcar.shiptopartyid
WHERE     
    pbcar.company_id = 'SE-HEMTEX'
    AND pbcar.for_consolidation = 1
    AND carid IN (SELECT carid FROM car WHERE company_id = 'SE-HEMTEX' AND CONSOLIDATION_DTM IS NULL)
GROUP BY 
    TO_CHAR (co.insertdtm, 'YYYY-MM-DD')
ORDER BY 
    TO_CHAR (co.insertdtm, 'YYYY-MM-DD') ASC
	
/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: Order in
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select
    to_char(A.INSERTDTM,'HH24') "TIMME",
    count(distinct(A.COID||a.coseq)) "ORDER",
    COUNT(DISTINCT(B.COID||B.COSEQ||B.COPOS)) "RADER"
from
    CO a,
    COROW b
where
    A.COMPANY_ID = 'AT-PREFA'
    and B.COMPANY_ID = A.COMPANY_ID
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and b.corowtype<>'I'
    and to_char(A.INSERTDTM,'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd')
group by
    to_char(A.INSERTDTM,'HH24')
	
/******************************************************************************
NAME: AT-PREFA_Production_Report.rpt
SUBREPORT: LM_Noll_Plockade_Orderradar_1745
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select
    A.COID "ORDER",
    A.ARTID "ARTID",
    A.ARTNAME1 "ARTNAME",
    C.LOADFINISHDTM "DATUM",
    to_char(B.INSERTDTM,'yyyy-mm-dd') "INSERTDTM",
    A.ORDQTY "ORDQTY",
    A.PICKQTY "PICKQTY",
    A.RESTQTY "RESTQTY"
from
    COROWTRC a,
    COTRC b,
    DEP c
where
    A.COMPANY_ID = 'LEKMER'
    and A.COID = B.COID
    and A.COSEQ = B.COSEQ
    and A.COSUBSEQ = B.COSUBSEQ
    and A.COROWTYPE <> 'I'
    and A.RESTQTY <> 0
    and B.DEPARTURE_ID = C.DEPARTURE_ID
    and to_char(C.LOADFINISHDTM ,'yyyy-mm-dd') = to_char(sysdate-1,'yyyy-mm-dd')