/******************************************************************************
NAME: AT-PREFA_Planning_Tool
SUBREPORT: -
DESC: Shown only Departures with pick status Not started and Open 
All Methode of Shipment except AT-PREFA-GLS. Reportfilter AT-PREFA ( Live table only) 								

REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		13.03.2023      PBRUGGER     IMI-3416       created

******************************************************************************/

WITH
    cusor
    AS
        (SELECT   cowork.departure_id
                , LISTAGG(
                      DISTINCT party.countrycode, ';'
                  )
                  WITHIN GROUP (ORDER BY 1)    AS country
                , count(coid||coseq) as cusor
         FROM     cowork
                  INNER JOIN party
                      ON party.party_id = cowork.shiptopartyid
                      AND party.party_qualifier = 'CU'
                      AND party.company_id = cowork.company_id
         WHERE    cowork.company_id = 'AT-PREFA'
         GROUP BY cowork.departure_id),
    open_pick_orders
    AS
        (SELECT   departure_id
                , COUNT(DISTINCT (CASE WHEN pzid = 'PPR' THEN pbheadid END))    AS ppr_open_pick_orders
                , COUNT(DISTINCT (CASE WHEN pzid = 'PPL' THEN pbheadid END))    AS ppl_open_pick_orders
                , COUNT(DISTINCT (CASE WHEN pzid = 'PPW' THEN pbheadid END))    AS ppw_open_pick_orders
         FROM     pbrow
         WHERE    pbrow.ownid = 'AT-PREFA'
         AND      pbrow.whid = 'PRE1'
         AND      pbrowstat <= '3'
         GROUP BY departure_id),
    open_lines
    AS
        (SELECT   departure_id
                , departure_pickstat
                , routid
                , NVL(SUM(ppr_open), 0)     AS ppr_open
                , NVL(SUM(ppl_open), 0)     AS ppl_open
                , NVL(SUM(ppw_open), 0)     AS ppw_open
         FROM     (SELECT dep.departure_id
                        , dep.routid
                        , dep.departure_pickstat
                        , CASE
                              WHEN pbcardep_vw.pzid = 'PPR' THEN
                                  TO_CHAR(NVL(pbcardep_vw.all_norows, 0))
                                  - TO_CHAR(NVL(pbcardep_vw.picked_norows, 0))
                          END    AS ppr_open
                        , CASE
                              WHEN pbcardep_vw.pzid = 'PPL' THEN
                                  TO_CHAR(NVL(pbcardep_vw.all_norows, 0))
                                  - TO_CHAR(NVL(pbcardep_vw.picked_norows, 0))
                          END    AS ppl_open
                        , CASE
                              WHEN pbcardep_vw.pzid = 'PPW' THEN
                                  TO_CHAR(NVL(pbcardep_vw.all_norows, 0))
                                  - TO_CHAR(NVL(pbcardep_vw.picked_norows, 0))
                          END    AS ppw_open
                   FROM   dep
                          INNER JOIN pbcardep_vw
                              ON pbcardep_vw.departure_id = dep.departure_id
                          INNER JOIN pz ON pz.pzid = pbcardep_vw.pzid
                   WHERE  dep.whid = 'PRE1'
                   AND    pbcardep_vw.departure_id = dep.departure_id
                   AND    pz.pzid = pbcardep_vw.pzid
                   AND    pz.whid = 'PRE1'
                   AND    dep.dlvrymeth_id != 'AT-PREFA-GLS'
                   AND    dep.departure_pickstat IN ('C', 'O'))
         GROUP BY departure_id, routid, departure_pickstat)
SELECT 
    OPEN_LINES.DEPARTURE_ID
    , OPEN_LINES.ROUTID
    , CUSOR.COUNTRY
    , CUSOR.CUSOR
    , OPEN_LINES.PPR_OPEN
    , OPEN_PICK_ORDERS.PPR_OPEN_PICK_ORDERS
    , OPEN_LINES.PPL_OPEN
    , OPEN_PICK_ORDERS.PPL_OPEN_PICK_ORDERS
    , OPEN_LINES.PPW_OPEN
    , OPEN_PICK_ORDERS.PPW_OPEN_PICK_ORDERS
    , CASE WHEN OPEN_LINES.DEPARTURE_PICKSTAT = 'C' THEN 'Not started'
            WHEN OPEN_LINES.DEPARTURE_PICKSTAT = 'O' THEN 'Open'
            END AS STATUS
FROM   open_lines
       INNER JOIN cusor ON cusor.departure_id = open_lines.departure_id
       INNER JOIN open_pick_orders
           ON open_pick_orders.departure_id = open_lines.departure_id
ORDER BY 1 DESC