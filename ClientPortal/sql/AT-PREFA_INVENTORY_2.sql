/******************************************************************************
NAME: AT-PREFA_Inventory_Report_2.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		22.07.2022      DAKORNHA                    created
1.1         29.07.2022      DAKORNHA    IMI-2449        added GROUP BY and SUM(STORQTY). Sub-Select needed

******************************************************************************/

SELECT COMPANY_ID,
    WSID,
    WPADR,
    ARTID,
    SUM(STORQTY) STORQTY,
    PAKID,
    L2_BASEQTY,
    L2_PAKID,
    CARID 
FROM (
    SELECT C.COMPANY_ID,
        C.WSID,
        C.WPADR,
        CASE WHEN I.ARTID IS NULL 
            THEN CT.ARTID 
            ELSE I.ARTID 
        END ARTID,
        CASE WHEN I.STORQTY IS NULL 
            THEN CT.STORQTY 
            ELSE I.STORQTY 
        END STORQTY,
        CASE WHEN I.PAKID IS NULL 
            THEN CT.PAKID 
            ELSE I.PAKID 
        END PAKID,
        P2.BASEQTY L2_BASEQTY,
        P2.PAKID L2_PAKID,
        CASE WHEN COUNT(DISTINCT C.CARID) OVER (PARTITION BY C.WSID, C.WPADR) > 1 
            THEN 'More than 1 LC'
            ELSE C.CARID 
        END CARID -- multiple SSCC will not be shown in report design
    FROM CAR C 
    LEFT JOIN 
        (SELECT C.CARCARID,
                I.ARTID,
                SUM (I.STORQTY) STORQTY,
                I.PAKID
            FROM CAR C
                LEFT JOIN ITE I 
                ON C.COMPANY_ID = I.COMPANY_ID 
                AND C.CARID = I.CARID
                AND C.WHID = I.WHID
            WHERE C.COMPANY_ID = 'AT-PREFA'
        GROUP BY C.CARCARID, I.ARTID, I.PAKID
        ) CT 
        ON C.CARID = CT.CARCARID
    LEFT JOIN ITE I 
        ON C.COMPANY_ID = I.COMPANY_ID 
        AND C.CARID = I.CARID
        AND C.WHID = I.WHID
    LEFT JOIN --next layer join
        (SELECT COMPANY_ID, 
                DENSE_RANK() OVER(PARTITION BY ARTID ORDER BY BASEQTY) "RANK", 
                ARTID, 
                PAKID, 
                BASEQTY
            FROM PAK
            WHERE COMPANY_ID = 'AT-PREFA'
        ) P2 
        ON CASE WHEN I.ARTID IS NULL 
                THEN CT.ARTID 
                ELSE I.ARTID 
            END = P2.ARTID 
        AND P2."RANK" = 2
    WHERE C.COMPANY_ID='AT-PREFA'
        AND C.WHID = 'PRE1'
        AND C.CARCARID IS NULL
        AND C.WPADR IS NOT NULL
        AND C.CARID IN
        (SELECT CARID 
            FROM CAR
            WHERE COMPANY_ID='AT-PREFA'
                AND WHID = 'PRE1')
    --AND CT.CARCARID = '390082980000129504'
    )
GROUP BY COMPANY_ID,
    WSID,
    WPADR,
    ARTID,
    PAKID,
    L2_BASEQTY,
    L2_PAKID,
    CARID 
ORDER BY 1,
        2,
        3