/* Formatted on 03/03/2022 09:02:57 (QP5 v5.336) */
/******************************************************************************
NAME: AT-PREFA-Transport Report v2.rpt
DESC: NO_PARAM
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description


1.0 		08.12.2021 		DAROGALS     PREFA-67	   new sql code for original version
1.1 		22.12.2021 		UJABCZYS     PREFA-67	   added ASN Level 4 handling, corrected search criteria
1.2 		10.01.2022 		UJABCZYS     PREFA-67	   modified Picked LC section - changed joined table from PBROW to PBROWLOG, added Countrycode
1.3 		10.01.2022 		UJABCZYS     PREFA-67      code for Picked LC section rebuild from scratch
1.4 		18.01.2022 		UJABCZYS     PREFA-67      modified PAK join for PLANNED section, taking biggest in number package, when 'Rol' is biggest, count it as 'PAL'. Added COSEQ after COID in main query results
1.5         03.03.2022      DAKORNHA     PREFA-67      added COSEQ to all PARTITION BY clauses
1.6         16.03.2022      DAKORNHA     PREFA-89      added CW.CONSIGNMENT_ID (COWORK.CONSIGNMENT_ID)


******************************************************************************/



SELECT *
  FROM (SELECT ROW_NUMBER ()
                   OVER (PARTITION BY MAIN.COID, MAIN.COSEQ, MAIN.LC
                         ORDER BY MAIN.LC DESC)
                   RANKING4,
               MAIN.COID || '-' || MAIN.COSEQ
                   "Order Number",
               CO.CO_REF
                   "Reference",
               CW.CONSIGNMENT_ID
                   "Consignment ID",
               DEP.DEPARTURE_ID
                   "Departure ID",
               CASE
                   WHEN DEP.DEPARTURE_PICKSTAT = 'O' THEN 'Open'
                   WHEN DEP.DEPARTURE_PICKSTAT = 'R' THEN 'Ready'
                   WHEN DEP.DEPARTURE_PICKSTAT = 'H' THEN 'Hold'
                   WHEN DEP.DEPARTURE_PICKSTAT = 'C' THEN 'Not started'
                   WHEN DEP.DEPARTURE_PICKSTAT = 'F' THEN 'Finished'
                   ELSE ''
               END
                   "Depature Status",
               DEP.DEPARTURE_DTM
                   "Loading Date",
               ''
                   "Arrival Date",
               P.NAME1
                   "Consignee Name",
               P.COUNTRYCODE
                   "Consignee Country Code",
               P.POSTCODE
                   "Consignee Zip Code",
               P.CITY
                   "Consignee City",
               P.ADR1
                   "Consignee Address",
               P.PHONE
                   "Consignee Contact",
               SUM (MAIN.TOTWGT) OVER (PARTITION BY MAIN.COID, MAIN.COSEQ, MAIN.LC)
                   "Weight",
               MAIN.LENGTH
                   "Lenght",
               MAIN.WIDTH
                   "Widht",
               SUM (MAIN.USER_HEIGHT) OVER (PARTITION BY MAIN.COID, MAIN.COSEQ, MAIN.LC)
                   "Height",
               SUM (MAIN.TOTVOL) OVER (PARTITION BY MAIN.COID, MAIN.COSEQ, MAIN.LC)
                   "Volume",
               MAIN.LC
                   "Load Carrier Type",
               CASE
                   WHEN CARTYPID2 = 'G01' THEN 'BE'
                   WHEN CARTYPID2 = 'G02' THEN 'BE'
                   WHEN CARTYPID2 = 'G03' THEN 'BE'
                   WHEN CARTYPID2 = 'G04' THEN 'BE'
                   WHEN LC = 'X05' THEN 'EW'
                   WHEN CARTYPID2 = 'EWP' THEN 'XP'
                   WHEN CARTYPID2 = 'BD3' THEN 'CO'
                   WHEN CARTYPID2 = 'BD6' THEN 'CO'
                   WHEN CARTYPID2 = 'KRT' THEN 'KT'
                   WHEN CARTYPID2 = 'K04' THEN 'KT'
                   WHEN CARTYPID2 = 'K03' THEN 'KT'
                   WHEN CARTYPID2 = 'WAMAS' THEN 'KT'
                   ELSE 'PA'
               END
                   "Excel an LV",
               SUM (MAIN.PLANNED) OVER (PARTITION BY MAIN.COID, MAIN.COSEQ, MAIN.LC)
                   "Planned",
               SUM (MAIN.PICKED) OVER (PARTITION BY MAIN.COID, MAIN.COSEQ, MAIN.LC)
                   "Picked",
               CASE WHEN DANG.COID IS NOT NULL THEN 'x' ELSE NULL END
                   DANGER_GOODS
          FROM (                                                  --MAIN QUERY
                SELECT                                     -- PLANNED LC BEGIN
                       COMPANY_ID,
                       COID,
                       PLANNED,
                       CARTYPID      LC,
                       ESTWEIGHT     TOTWGT,
                       ESTVOLUME     TOTVOL,
                       NULL          USER_HEIGHT,
                       NULL          PICKED,
                       LENGTH,
                       WIDTH,
                       CARTYPID2,
                       COSEQ,
                       SHIPTOPARTYID,
                       SHIPTOPARTYQUALIFIER
                  FROM (SELECT COR.ORDQTY,
                               COR.ARTID,
                               COR.COID,
                               COR.COMPANY_ID,
                               PAK.PAKID
                                   LC,
                               PAK.BASEQTY,
                               ROUND (COR.ORDQTY / PAK.BASEQTY, 4),
                               SUM ((COR.ORDQTY / PAK.BASEQTY))
                                   OVER (PARTITION BY COR.COID, COR.COSEQ, PAK.PAKID)
                                   PLANNED,
                               ROW_NUMBER ()
                                   OVER (PARTITION BY COR.COID, COR.COSEQ, PAK.PAKID
                                         ORDER BY PAK.PAKID DESC)
                                   RANKING,
                               CT.CARTYPID,
                               CT.LENGTH,
                               CT.WIDTH,
                               CT.CARTYPID2,
                               COR.COSEQ,
                               COR.SHIPTOPARTYID,
                               COR.SHIPTOPARTYQUALIFIER,
                               SUM (COR.ESTVOLUME)
                                   OVER (PARTITION BY COR.COID, COR.COSEQ, PAK.PAKID)
                                   ESTVOLUME,
                               SUM (COR.ESTWEIGHT)
                                   OVER (PARTITION BY COR.COID, COR.COSEQ, PAK.PAKID)
                                   ESTWEIGHT
                          FROM COROW  COR
                               INNER JOIN --not a standard PAK table, to connect Rol package if it's the highest available package for product.
                               (SELECT *
                                  FROM (SELECT DENSE_RANK ()
                                                   OVER (
                                                       PARTITION BY ARTID
                                                       ORDER BY BASEQTY DESC)
                                                   "RANK",
                                               ARTID,
                                               PAKID,
                                               PAKNAME,
                                               PAKNAME2,
                                               BASPAKID,
                                               BASEQTY,
                                               CONSUMERQTY,
                                               WEIGHT,
                                               HEIGHT,
                                               LENGTH,
                                               WIDTH,
                                               VOLUME,
                                               HNDLCOD,
                                               PRICE1,
                                               PRICE2,
                                               CARTYPID,
                                               CAR_CARTYPID,
                                               NOPAKS_CARCAR,
                                               MAXTLOAD,
                                               PALPICK,
                                               ABALDIFF_PLUS,
                                               ABALDIFF_MINUS,
                                               DEPOSIT_TYPE,
                                               COMPANY_ID,
                                               PMTYPID_IN,
                                               PMTYPID_OUT,
                                               PALPAK,
                                               DLVRYPAK,
                                               VOLUME_FILL_PERCENT,
                                               LAYERPAK,
                                               EANDUN,
                                               BARCODETYPE,
                                               PROID,
                                               UPDDTM,
                                               NET_WEIGHT,
                                               WWS_REPL_CARTYPID,
                                               LAYERCREATION,
                                               WWS_SERIALNUMBERTYPE_ID
                                          FROM PAK
                                         WHERE COMPANY_ID = 'AT-PREFA'--AND PAKID IN ('PAL', 'G01', 'G02', 'G03', 'G04', 'G05', 'G06')
                                                                      --AND ARTID IN ('040100','040110','040300','040310')
                                                                      )
                                 WHERE "RANK" = 1) PAK
                                   ON     PAK.COMPANY_ID = COR.COMPANY_ID
                                      AND PAK.ARTID = COR.ARTID --AND PAK.PAKID IN ('PAL', 'G01', 'G02', 'G03', 'G04', 'G05', 'G06')
                               LEFT JOIN CARTYP CT
                                   ON     CASE
                                              WHEN CT.CARTYPID = 'X05'
                                              THEN
                                                  CT.CARTYPID
                                              ELSE
                                                  CT.CARTYPID2
                                          END =
                                          CASE
                                              WHEN PAK.PAKID IN
                                                       ('PAL', 'Rol')
                                              THEN
                                                  'X05'
                                              ELSE
                                                  PAK.PAKID
                                          END /*exception for 'PAL' PAK, linking with 'X05' CARTYPE, and with Rol if Rol ih biggest package*/
                                      AND CT.CARTYPID IN ('X01',
                                                          'X02',
                                                          'X02',
                                                          'X03',
                                                          'X04',
                                                          'X05',
                                                          'X06')
                         WHERE     COR.COMPANY_ID = 'AT-PREFA'
                               AND COR.RESTQTY = 0
                               AND COR.COROWSTAT NOT IN ('8', '5')--AND COR.COID = '01-303081584-01'
                                                                  )
                 WHERE RANKING = '1'
                UNION
                SELECT                                            -- PICKED LC
                       COMPANY_ID,
                       COID,
                       NULL     PLANNED,
                       LC,
                       TOTWGT,
                       TOTVOL,
                       USER_HEIGHT,
                       PICKED,
                       LENGTH,
                       WIDTH,
                       CARTYPID2,
                       COSEQ,
                       SHIPTOPARTYID,
                       SHIPTOPARTYQUALIFIER
                  FROM (SELECT ROW_NUMBER ()
                                   OVER (PARTITION BY CARTYPID, COID, COSEQ
                                         ORDER BY COID DESC)
                                   RANKING3,
                               CARID,
                               COID,
                               CARTYPID
                                   LC,
                               SUM (TOTWGT)
                                   OVER (PARTITION BY COID, COSEQ, CARTYPID)
                                   TOTWGT,
                               SUM (TOTVOL)
                                   OVER (PARTITION BY COID, COSEQ, CARTYPID)
                                   TOTVOL,
                               SUM (USER_HEIGHT)
                                   OVER (PARTITION BY COID, COSEQ, CARTYPID)
                                   USER_HEIGHT,
                               COUNT (CARID)
                                   OVER (PARTITION BY COID, COSEQ, CARTYPID)
                                   PICKED,
                               COMPANY_ID,
                               LENGTH,
                               WIDTH,
                               CARTYPID2,
                               COSEQ,
                               SHIPTOPARTYID,
                               SHIPTOPARTYQUALIFIER
                          FROM (SELECT DISTINCT
                                       PC.CARID,
                                       CAR.CARCARID,
                                       PC.COMPANY_ID,
                                       PC.PBCARID,
                                       PC.DEPARTURE_ID,
                                       CASE
                                           WHEN PR.COID IS NULL
                                           THEN
                                               PC.COID_SINGLE
                                           ELSE
                                               PR.COID
                                       END    COID,             /*1.1 change*/
                                       CAR.CARTYPID,
                                       CAR.TOTWGT,
                                       CAR.TOTVOL,
                                       CAR.USER_HEIGHT,
                                       CT.LENGTH,
                                       CT.WIDTH,
                                       CT.CARTYPID2,
                                       CASE
                                           WHEN PR.COSEQ IS NULL
                                           THEN
                                               PC.COSEQ_SINGLE
                                           ELSE
                                               PR.COSEQ
                                       END    COSEQ,            /*1.1 change*/
                                       PC.SHIPTOPARTYID,
                                       PC.SHIPTOPARTYQUALIFIER
                                  FROM PBROW  PR
                                       INNER JOIN
                                       (SELECT PBCARID,
                                               NULL    CONSOLIDATION_TO_PBCARID
                                          FROM PBCAR PBC
                                         WHERE     PBC.COMPANY_ID =
                                                   'AT-PREFA'
                                               AND CONSOLIDATION_TO_PBCARID
                                                       IS NULL
                                        MINUS
                                        (SELECT DISTINCT
                                                CONSOLIDATION_TO_PBCARID
                                                    PBCARID,
                                                NULL
                                                    CONSOLIDATION_TO_PBCARID
                                           FROM PBCAR PBC
                                          WHERE     PBC.COMPANY_ID =
                                                    'AT-PREFA'
                                                AND CONSOLIDATION_TO_PBCARID
                                                        IS NOT NULL)
                                        UNION
                                        SELECT PBCARID,
                                               CONSOLIDATION_TO_PBCARID --SLAVY I MASTERY (ASN4)
                                          FROM PBCAR PBC
                                         WHERE     PBC.COMPANY_ID =
                                                   'AT-PREFA'
                                               AND CONSOLIDATION_TO_PBCARID
                                                       IS NOT NULL) R
                                           ON PR.PBCARID = R.PBCARID
                                       INNER JOIN PBCAR PC
                                           ON     PR.COMPANY_ID =
                                                  PC.COMPANY_ID
                                              AND PC.PBCARID =
                                                  CASE
                                                      WHEN R.CONSOLIDATION_TO_PBCARID
                                                               IS NOT NULL
                                                      THEN
                                                          R.CONSOLIDATION_TO_PBCARID
                                                      ELSE
                                                          R.PBCARID
                                                  END
                                       INNER JOIN CAR
                                           ON     CAR.CARID = PC.CARID
                                              AND CAR.COMPANY_ID =
                                                  PC.COMPANY_ID
                                       INNER JOIN CARTYP CT
                                           ON CT.CARTYPID = CAR.CARTYPID
                                 WHERE     PR.PBROWSTAT IN ('4', '5')
                                       AND PR.PBTYPE IN ('P', 'C', 'S')
                                       AND PC.PBCARSTAT <> 'R'--AND PC.COID_SINGLE = '01-303081584-01'
                                                              ))) MAIN
               INNER JOIN COWORK CW
                   ON     CW.COID = MAIN.COID
                      AND MAIN.COSEQ = CW.COSEQ
                      AND CW.COMPANY_ID = MAIN.COMPANY_ID
                      AND CW.DLVRYMETH_ID <> 'AT-PREFA-PICKUP' --AND CW.COID = '01-307057466-02'
               INNER JOIN CO
                   ON     CO.COID = MAIN.COID
                      AND MAIN.COSEQ = CO.COSEQ
                      AND CO.COMPANY_ID = MAIN.COMPANY_ID
               LEFT JOIN PARTY P
                   ON     P.PARTY_ID = MAIN.SHIPTOPARTYID
                      AND P.PARTY_QUALIFIER = MAIN.SHIPTOPARTYQUALIFIER
               INNER JOIN DEP
                   ON     DEP.WHID = CW.WHID
                      AND DEP.DEPARTURE_ID = CW.DEPARTURE_ID
               LEFT JOIN
               (SELECT DISTINCT CR.COID
                  FROM COROW  CR
                       INNER JOIN ART A
                           ON     CR.COMPANY_ID = A.COMPANY_ID
                              AND CR.ARTID = A.ARTID
                              AND A.ARTGROUP = 'GFG'
                 WHERE CR.COMPANY_ID = 'AT-PREFA') DANG
                   ON MAIN.COID = DANG.COID
         WHERE     dep.departure_dtm >= SYSDATE
               AND dep.departure_dtm < SYSDATE + 1--WHERE DEP.DEPARTURE_DTM  BETWEEN {?DATE_A} AND {?DATE_B}+1
                                                  --WHERE DEP.DEPARTURE_DTM BETWEEN TO_DATE('2022-01-10','YYYY-MM-DD') AND TO_DATE('2022-01-15','YYYY-MM-DD')+1

                                                  )
 WHERE RANKING4 = 1