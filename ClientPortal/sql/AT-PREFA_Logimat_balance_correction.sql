/* Formatted on 04/11/2022 17:31:35 (QP5 v5.381) */
/******************************************************************************
NAME: AT-PREFA_Logimat_balance_correction.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		04.11.2022      DAKORNHA    IMI-2789        created

******************************************************************************/

SELECT 'Logimat Balance Correction'     AS transaction_comment
    , 'ITEM Change'                     AS transaction_type
    , iteevent_vw.iteid
    , iteevent_vw.empid
    , iteevent_vw.storqty
    , iteevent_vw.event_qty                         AS qty_update
    , iteevent_vw.event_dtm
FROM iteevent_vw
WHERE iteevent_vw.company_id = 'AT-PREFA'
    AND iteevent_vw.itechgcodid = 'CR'
    AND iteevent_vw.empid LIKE 'Logimat%'
    AND TRUNC(event_dtm) BETWEEN TRUNC(SYSDATE)-1 AND TRUNC(SYSDATE)