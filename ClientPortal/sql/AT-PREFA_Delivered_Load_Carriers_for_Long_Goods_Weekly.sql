/* Formatted on 25.10.2022 14:53:11 (QP5 v5.336) */
/******************************************************************************
NAME: AT-PREFA - Delivered Load Carriers for Long Goods.rpt
DESC: 
REVISIONS:

Ver 		Date      Player       SN/JIRA 		Description

 
1.0 	2021/12/02	 DAROGALS     PREFA-61 		initial version
1.1 	2021/12/21	 UJABCZYS     PREFA-61 		Customer Order sorting added, removed PLO, PLP area filter
1.2 	2022/09/14	 AQEASLAM     IMI-2556 		Two additional columns Country and Load Carrier: EUR-PAL (X05) added 
1.3     2022/10/25   DAKORNHA     IMI-2462      Corrected load carrier calculation
                                                Refactoring based on style guide

******************************************************************************/

SELECT *
FROM (
    SELECT 
        ROW_NUMBER() OVER(PARTITION BY coid ORDER BY coid DESC)    ranking
        , loadfinishdtm                                              "Delivery Date"
        , TO_CHAR(loadfinishdtm, 'IW')                               "Delivery Week"
        , coid                                                       "Customer Order Number"
        , co_ref                                                     "Customer Order Number Customer"
        , name                                                       "Ship-to Customer Name"
        , adr1                                                       "Ship-to Customer Address"
        , postcode                                                   "Ship-to Customer Zip Code"
        , city                                                       "Ship-to Customer City"
        , country                                                    "Country"
        , SUM(x01) OVER (PARTITION BY coid)                          "G01 (X01)"
        , SUM(x02) OVER (PARTITION BY coid)                          "G02 (X02)"
        , SUM(x03) OVER (PARTITION BY coid)                          "G03 (X03)"
        , SUM(x04) OVER (PARTITION BY coid)                          "G04 (X04)"
        , SUM(x05) OVER (PARTITION BY coid)                          "G06 (X05)"
        /*  IMI-2556 */
        , SUM(x04) OVER (PARTITION BY coid)                          "G05 (X07)"
    FROM (
        SELECT 
            coref.coid
            , coref.co_ref
            , loadfdtm.loadfinishdtm
            , p.name1 || p.name2       name
            , p.postcode
            , p.adr1
            , p.city
            , p.country
            , COUNT( 
                DISTINCT
                CASE 
                    WHEN cartrc.cartypid = 'X01' 
                        THEN nvl(cartrc.carcarid, cartrc.carid)
                    ELSE NULL
                END
            ) AS x01
            , COUNT( 
                DISTINCT
                CASE 
                    WHEN cartrc.cartypid = 'X02' 
                        THEN nvl(cartrc.carcarid, cartrc.carid)
                    ELSE NULL
                END
            ) AS x02
            , COUNT( 
                DISTINCT
                CASE 
                    WHEN cartrc.cartypid = 'X03' 
                        THEN nvl(cartrc.carcarid, cartrc.carid)
                    ELSE NULL
                END
            ) AS x03
            , COUNT( 
                DISTINCT
                CASE 
                    WHEN cartrc.cartypid = 'X04' 
                        THEN nvl(cartrc.carcarid, cartrc.carid)
                    ELSE NULL
                END
            ) AS x04
            , COUNT( 
                DISTINCT
                CASE 
                    WHEN cartrc.cartypid = 'X05' 
                        THEN nvl(cartrc.carcarid, cartrc.carid)
                    ELSE NULL
                END
            ) AS x05
            , COUNT( 
                DISTINCT
                CASE 
                    WHEN cartrc.cartypid = 'X07' 
                        THEN nvl(cartrc.carcarid, cartrc.carid)
                    ELSE NULL
                END
            ) AS x07
        FROM pbcarlog  pbcl
            INNER JOIN pbrowlog pbrl ON
                pbrl.company_id = pbcl.company_id
                AND pbrl.pbcarid = pbcl.pbcarid
                AND pbrl.pbtype <> 'G' --AND PBRL.WSID IN ('PLO', 'PLP')
            INNER JOIN cartrc ON
                pbcl.carid = cartrc.carid
                AND pbcl.company_id = cartrc.company_id
            LEFT JOIN party p ON
                p.company_id = pbcl.company_id
                AND p.party_qualifier = pbcl.shiptopartyqualifier
                AND p.party_id = pbcl.shiptopartyid
            LEFT JOIN (
                SELECT 
                    coid 
                    , company_id, co_ref
                FROM co
                WHERE company_id = 'AT-PREFA'
            UNION
                SELECT 
                    coid
                    , company_id, co_ref
                FROM cotrc
                WHERE company_id = 'AT-PREFA'
            ) coref  ON
                coref.company_id = pbrl.company_id
                AND coref.coid = pbrl.coid
            INNER JOIN (
                SELECT 
                    departure_id
                    , whid
                    , loadfinishdtm
                FROM dep
                WHERE whid = 'PRE1'
                    AND loadfinishdtm IS NOT NULL
                    AND loadfinishdtm >= TRUNC(SYSDATE) - 7
                --AND LOADFINISHDTM  >= TO_DATE('2021-11-01','YYYY-MM-DD')
                --AND LOADFINISHDTM  <  TO_DATE('2021-12-21','YYYY-MM-DD')+1
            UNION
                SELECT 
                    departure_id
                    , whid
                    , loadfinishdtm
                FROM deplog
                WHERE whid = 'PRE1'
                    AND loadfinishdtm >= TRUNC(SYSDATE) - 7
--                            AND LOADFINISHDTM  >= TO_DATE('2021-11-01','YYYY-MM-DD')
--                            AND LOADFINISHDTM  <  TO_DATE('2022-12-21','YYYY-MM-DD')+1
            ) loadfdtm ON
                loadfdtm.whid = pbcl.whid
                AND loadfdtm.departure_id = pbcl.departure_id
        WHERE pbcl.company_id = 'AT-PREFA'
            AND pbcl.pikweight <> 0
            AND pbcl.pbtype IN ('C', 'P')
        GROUP BY 
                coref.coid
                , coref.co_ref
                , loadfdtm.loadfinishdtm
                , p.name1 || p.name2       
                , p.postcode
                , p.adr1
                , p.city
                , p.country
        )
    )
ORDER BY 4, 2 ASC