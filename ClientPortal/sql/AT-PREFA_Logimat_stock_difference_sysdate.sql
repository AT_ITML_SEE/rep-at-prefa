/******************************************************************************
NAME: AT-PREFA_Logimat_stock_difference_sysdate.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

with b as (select
a.itemno,
a.amount_baseqty as amountMAPI
from
MAPI_IN_12_STOCKREPORT_ACK a
where to_char(a.upddtm, 'DD.MM.YYYY') = to_char(sysdate, 'DD.MM.YYYY')
)
select
nvl(a.artid,b.itemno) as artid,
a.amountite,
nvl(b.amountmapi,0) as amountmapi,
nvl(a.amountITE,0) - nvl(b.amountMAPI,0) as dif_ITE_TO_MAPI
from
(
select
sum (storqty) as amountITE,
artid,
ownid
from ite
where supid = 'AT-PREFA-LM'
group by artid, ownid) a
FULL OUTER JOIN b on a.artid =  b.itemno
WHERE nvl(a.amountITE, 0) <> nvl(b.amountMAPI, 0)
order by 1 asc