/******************************************************************************
NAME: AT-PREFA_Stock_Report.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0         xx.xx.xx                                    created
1.1 		29.07.2022 	 	AQEASLAM    IMI-2443        added WEIGHT and Alternative_Amount
1.2         01.08.2022      AQEASLAM    IMI-2443        added WEIGHT in processedQuantity and Rol as kg
1.3         02.08.2022      AQEASLAM    IMI-2443        Doubled Qty error corrected
1.4         06.09.2022      DAKORNHA    IMI-2489        rework stock report
1.5         10.10.2022      DAKORNHA    IMI-2704        fix recovered item loads (doubled qty)
1.6         02.11.2022      DAKORNHA    IMI-2807        correct qty when picked item load is included in both,
                                                        CO where PREASN has been sent
                                                        and CO where PREASN has not been sent 
1.7         03.11.2022      DAKORNHA    IMI-2807        added window function for count of item loads
                                                        OVER (PARTITION BY pbrow.iteid) 
1.8         18.04.2023      PBRUGGER    INC006717747    An ITEID from Item Load Split has no rcvcarlog record
                                                        Join changed to LEFT JOIN instead INNER JOIN
******************************************************************************/

WITH 
    ite_info AS (
        SELECT ite.iteid
            , ite.artid
            , ite.pakid
            , ite.company_id
            , ite.itestat
            , ite.storqty
            , ite.prodlot
            , ite.sernumb
            , ite.carid
            , ite.whid
            , 'ITE' AS tab
            , ite.FIFODATE
        FROM ite
        INNER JOIN art ON -- art due TO performance (better indexing)
            ite.artid = art.artid
            AND ite.company_id = art.company_id
        WHERE 
            ite.company_id = 'AT-PREFA'
            AND ite.whid = 'PRE1'
        UNION
        SELECT iteid
            , artid
            , pakid
            , company_id
            , NULL AS itestat
            , storqty
            , prodlot
            , sernumb
            , carid
            , whid
            , 'ITETRC' AS tab
            , FIFODATE
        FROM itetrc
        WHERE 
            company_id = 'AT-PREFA'
            AND whid = 'PRE1'
            AND insertdtm >= SYSDATE-7
            /* exclude recovered item loads */
            AND NOT EXISTS (
                SELECT 1
                FROM ite
                WHERE itetrc.iteid = iteid)
        ORDER BY carid
    )
    , car_info AS (
        SELECT carid
            , company_id
            , whid
            , wsid
            , coid
            , coseq
            , 'CAR' AS tab
        FROM car
        WHERE 
            company_id = 'AT-PREFA'
            AND whid = 'PRE1'
        UNION
        SELECT carid
            , company_id
            , whid
            , wsid
            , coid
            , coseq
            , 'CARTRC' AS tab
        FROM cartrc
        WHERE 
            company_id = 'AT-PREFA'
            AND whid = 'PRE1'
            AND insertdtm >= SYSDATE-7
            /* exclude recovered item loads */
            AND NOT EXISTS (    
                SELECT 1
                FROM car
                WHERE cartrc.carid = carid)
        ORDER BY
            carid
    )
    , pbrow_info AS (
        SELECT pbrow.iteid
            , SUM(pbrow.pickqty) AS pickqty
            , pbrow.artid
            , pbrow.pakid
            , pbrow.company_id
            , pbrow.pbtype
            , LISTAGG(pbrow.coid, ', ') WITHIN GROUP (ORDER BY pbrow.coid) AS coid
            , LISTAGG(coworklog.co_log_type, ', ') 
                WITHIN GROUP (ORDER BY coworklog.co_log_type) AS co_log_type
            , COUNT(pbrow.iteid) OVER (PARTITION BY pbrow.iteid) count_iteid
        FROM pbrow
        LEFT JOIN coworklog ON
            coworklog.coid = pbrow.coid
            AND coworklog.coseq = pbrow.coseq
            AND coworklog.company_id = pbrow.company_id
            AND coworklog.co_log_type = 'ReportEvent' -- pre asn
            AND coworklog.upddtm = (
                /*  show only first ReportEvent */
                SELECT MIN(upddtm)
                FROM coworklog colog
                WHERE colog.coid = coworklog.coid
                    AND colog.coseq = coworklog.coseq
                    AND colog.company_id = coworklog.company_id
                    AND colog.co_log_type = coworklog.co_log_type
                )
        WHERE pbrow.company_id = 'AT-PREFA'
              AND pbrow.pbtype NOT IN ('G')
        GROUP BY 
            pbrow.iteid
            , pbrow.artid
            , pbrow.pakid
            , pbrow.company_id
            , pbrow.pbtype
            , coworklog.co_log_type
    )
/*  SELECT starts here */
SELECT 
    NULL                                            AS "wmslocation",
    NULL                                            AS "handlingUnit",
    CAR_INFO.WHID                                   AS "whid",
    SUM(
        CASE
            /*  item load has been fully booked out and preasn has been sent */
            WHEN pbrow_info.co_log_type IS NOT NULL
                    AND ite_info.storqty = 0
            THEN 0
            WHEN pbrow_info.co_log_type IS NOT NULL
                    AND ite_info.storqty > 0
                    AND pbrow_info.pbtype = 'P'
            /*  item load has been pallet picked (=still on IMI stock) 
                and preasn has been sent */
            THEN ite_info.storqty-nvl(pbrow_info.pickqty, 0)
            /*  when item load has been picked for other CO where no PREASN
                has been sent this one is 0, as qty will be count on
                row where PREASN has not yet been sent 
                IMI-2807 */
            WHEN pbrow_info.co_log_type IS NOT NULL
                    AND ite_info.storqty > 0
                    AND count_iteid > 1
            THEN 0
            /*  item load has been partly picked (rest still on stock),
                and PREASN has not been sent yet.
                add already picked (reduced qty) back to the item load 
                IMI-2807 */
            WHEN pbrow_info.co_log_type IS NULL
                    AND ite_info.storqty > 0
                    AND count_iteid > 1
            THEN ite_info.storqty+pbrow_info.pickqty
            WHEN pbrow_info.co_log_type IS NOT NULL
                    AND ite_info.storqty > 0
            /*  added AND count_iteid = 1 here due to IMI-2807 */
                    AND count_iteid = 1
            THEN ite_info.storqty
            WHEN ite_info.itestat IS NULL
                    AND pbrow_info.coid IS NULL
                THEN 0
            /*  for 'Rol' products always take weight from inbound */
            WHEN ite_info.pakid = 'Rol'
                THEN rcvcarlog.totwgt
            /*  pallet pick qty is stored qty until departure is closed */
            WHEN pbrow_info.pbtype = 'P'
                THEN ite_info.storqty
            /* case pick qty needs to be added */
            ELSE ite_info.storqty+nvl(pbrow_info.pickqty, 0)
        END 
    )                                                       AS "processedQuantity",
    art.artid                                               AS "Item",
    ite_info.prodlot                                        AS "Lot",
    DECODE(ite_info.pakid, 'Rol', 'kg', ite_info.pakid)     AS "Unit",
    ite_info.sernumb                                        AS "serialNumber",
    SYSDATE                                                 AS "TransactionDate",
    0                                                       AS "BlockStatus",
    NULL                                                    AS "Balanced"
    /*  leave this here for analyzes */
--    , pbrow_info.PBTYPE
--    , pbrow_info.COID
--    , DECODE(ite_info.PAKID, 'Rol', rcvcarlog.totwgt, ite_info.STORQTY) AS STORQTY
--    , pbrow_info.PICKQTY
--    , pbrow_info.CO_LOG_TYPE
--    , ite_info.ITEID
--    , COUNT(ite_info.ITEID) count_iteid
--    , ite_info.fifodate
FROM art
LEFT JOIN ite_info ON
    art.artid = ite_info.artid
    AND art.company_id = ite_info.company_id 
INNER JOIN car_info ON car_info.carid = ite_info.carid
LEFT JOIN rcvcarlog ON 
    rcvcarlog.carid = ite_info.iteid
    AND car_info.whid = rcvcarlog.whid
    AND rcvcarlog.delid IS NOT NULL -- exclude not received
    AND rcvcarlog.rcvcarstat NOT IN ('90') -- 90 = Rejected
LEFT JOIN pbrow_info ON 
    art.artid = pbrow_info.artid
    AND art.company_id = pbrow_info.company_id
    AND ite_info.pakid = pbrow_info.pakid
    AND ite_info.iteid = pbrow_info.iteid 
WHERE art.company_id = 'AT-PREFA'
    AND car_info.whid = 'PRE1'
    AND car_info.wsid NOT IN ('PIN')
    /*  leave this here for analyzes */
--    AND art.artid IN (
--        '200000'
--        )
GROUP BY 
    car_info.whid,
    art.artid,
    ite_info.pakid,
    ite_info.prodlot,
    DECODE(ite_info.pakid, 'Rol', 'kg', ite_info.pakid),
    ite_info.sernumb
    /*  leave this here for analyzes */
--    , pbrow_info.PBTYPE
--    , pbrow_info.COID
--    , DECODE(ite_info.PAKID, 'Rol', RCVCARLOG.TOTWGT, ite_info.STORQTY)
--    , pbrow_info.PICKQTY
--    , pbrow_info.CO_LOG_TYPE
--    , ite_info.ITEID
--    , ite_info.fifodate
ORDER BY art.artid