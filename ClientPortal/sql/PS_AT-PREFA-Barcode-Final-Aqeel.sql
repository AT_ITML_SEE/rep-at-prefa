/******************************************************************************
NAME: .rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT distinct   --Fetch time 1.86 sec
    CO.COID||'-'||CO.COSEQ as COID, 
    ART.ARTID, 
    ART.ARTNAME1,    
    ART.ARTNAME2,

    CO.COSEQ, 
    COWORK.COSUBSEQ,
    SUBSTR(CO.COID,4) Auftrag2,
   
    pbROW.COPOS,     
/*
   (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN (PBROW.PICKQTY)  
    WHEN PBROW.PBTYPE = 'C'
    THEN (PBROW.PICKQTY)
    END ) 
    *
    (MAX (CASE
    WHEN corowftxAMT.FTX_QUALIFIER = 'AMT' 
    THEN to_number(corowftxAMT.TEXT,'9999.999999')
    END))
    as PICKQTY,

   (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN (PBROW.ORDQTY)    
    WHEN PBROW.PBTYPE = 'C'
    THEN (PBROW.ORDQTY) 
    END ) 
    *
    (MAX (CASE
    WHEN corowftxAMT.FTX_QUALIFIER = 'AMT' 
    THEN to_number(corowftxAMT.TEXT,'9999.999999')
    END))
    as ORDQTY,
*/
DECODE(PBROW.PAKID, 'Rol', 
   (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN (PBROW.PICKQTY)  
    WHEN PBROW.PBTYPE = 'C'
    THEN (PBROW.PICKQTY)
    END),
    (
   (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN (PBROW.PICKQTY)  
    WHEN PBROW.PBTYPE = 'C'
    THEN (PBROW.PICKQTY)
    END ) 
    *
    (MAX (CASE
    WHEN corowftxAMT.FTX_QUALIFIER = 'AMT' 
    THEN to_number(corowftxAMT.TEXT,'9999.999999')
    END))
    
    ) ) as PICKQTY,   

DECODE(PBROW.PAKID, 'Rol', 
   (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN (PBROW.ORDQTY)  
    WHEN PBROW.PBTYPE = 'C'
    THEN (PBROW.ORDQTY)
    END),
    (
   (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN (PBROW.ORDQTY)  
    WHEN PBROW.PBTYPE = 'C'
    THEN (PBROW.ORDQTY)
    END ) 
    *
    (MAX (CASE
    WHEN corowftxAMT.FTX_QUALIFIER = 'AMT' 
    THEN to_number(corowftxAMT.TEXT,'9999.999999')
    END))
    
    ) ) as ORDQTY, 
    
DECODE(PBROW.PAKID, 'Rol', 
    (CASE 
    WHEN PBROW.PBTYPE = 'C'
    THEN (RCVCARLOG.TOTWGT) * (PBROW.PICKQTY)
    ELSE (RCVCARLOG.TOTWGT)
    END),
    (CASE 
    WHEN PBROW.PBTYPE = 'P'
    THEN PBROW.WEIGHT    
    WHEN PBROW.PBTYPE = 'C'
    THEN PBROW.WEIGHT * (PBROW.PICKQTY) 
    END )) as Weight,   
    
   MAX (CASE
    WHEN corowftxAMT.FTX_QUALIFIER = 'AMT' 
    THEN to_number(corowftxAMT.TEXT,'9999.999999')
    END) amt,
/*
    MAX(CASE
    WHEN corowftxUNL.FTX_QUALIFIER = 'UNL' 
    THEN corowftxUNL.TEXT
    END) unl,
*/
    DECODE(PBROW.PAKID, 'Rol',
    (PBROW.PAKID),
    (MAX(CASE
    WHEN corowftxUNL.FTX_QUALIFIER = 'UNL' 
    THEN corowftxUNL.TEXT
    END)) )unl,
    
    substr(party_id,1,INSTR(party_id, '_',1)-1) PARTY_ID, 
    PARTY.NAME1, 
    PARTY.ADR1, 
    PARTY.ADR2,
    PARTY.PHONE,
    PARTY.POSTCODE, 
    PARTY.CITY, 

    CASE 
        WHEN PARTY.COUNTRYCODE = 'AT'
        THEN '�sterreich'
        ELSE PARTY.COUNTRYCODE
    END COUNTRYCODE,
    
    '' "Abs_Konto",
    'PREFA Aluminiumprodukte GmbH' "Abs_Name1",
    '' "Abs_Name2",    
    'Lamarrstrasse 2' "Abs_Stra�e",
    '3151' "Abs_Plz",
    'St. Georgen' "Abs_Ort",
    '�sterreich' "Abs_Land",
    '' "Abs_Referenz",
    COROW.FPSHIPDTM, 
    CASE WHEN CO.TERMS_OF_DELIVERY LIKE 'DAP%' 
    THEN 'DAP ' || PARTY.CITY
    WHEN CO.TERMS_OF_DELIVERY LIKE 'DDU%' 
    THEN 'DDU ' || PARTY.CITY
    ELSE SUBSTR(CO.TERMS_OF_DELIVERY,1,3) ||' '|| PARTY.CITY    
    END TERMS_OF_DELIVERY,
    
    ARTCOD.ARTCODE,

    (CASE WHEN COWORK.DLVRYMETH_ID = 'AT-PREFA-PICKUP'
    THEN NULL
    WHEN COWORK.DLVRYMETH_ID = 'AT-PREFA'
    THEN 'Lieferung Schenker'
    ELSE COWORK.DLVRYMETH_ID
    END) Speditur,
    
    nvl(LOC_PS_LANGUAGE_AT_PREFA.LNGCODE,'ENG') LNGCODE,    
    nvl(LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ADRESS,'GB') CUSTOMER_ADRESS,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.REPRINT,'Reprint') REPRINT,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.PAGE,'Page') PAGE,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.ORIGNAL,'Orignal') ORIGNAL,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.DELIVERY_NOTE_NO,'Delivery note no.') DELIVERY_NOTE_NO,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.SHIPED_TO,'Shipped To') SHIPED_TO,
--    nvl(LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ADRESS,'Customer address') CUSTOMER_ADRESS,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.SHIPMENT_FROM,'Shipment From') SHIPMENT_FROM,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.AVISO,'Aviso') AVISO_H,    
    nvl(LOC_PS_LANGUAGE_AT_PREFA.DATE1,'Date') DATE1,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.FORWARDING_AGENT,'Carrier/LSP') FORWARDING_AGENT,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ORDER,'Customer Order') CUSTOMER_ORDER,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ORDER_POS,'Line Order Information') CUSTOMER_ORDER_POS,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.WEIGHT,'Weight') WEIGHT_H,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.COMM_CODE,'CommCode') COMM_CODE,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.QUANTITY_SHIPPED,'Qty Shipped') QUANTITY_SHIPPED,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.QUANTITY_ORDERED,'Qty Ordered') QUANTITY_ORDERED,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.UNIT,'Unit') UNIT,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.DELIVERY_TERMS,'Delivery terms') DELIVERY_TERMS,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.INSPECTION,'Inspection') INSPECTION,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.GOODS_ACCEPTED_IN_ORDER,'Goods accepted in order') GOODS_ACCEPTED_IN_ORDER,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.PRINTED_NAME,'Printed Name') PRINTED_NAME,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT1,'') RF_TEXT1,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT2,'') RF_TEXT2,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT3,'') RF_TEXT3,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT4,'') RF_TEXT4,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT5,'') RF_TEXT5,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT1,'Delivery date:') PF_TEXT1,	
    nvl(LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT2,'Scheduled delivery date:') PF_TEXT2,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT3,'') PF_TEXT3,
    nvl(LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT4,'') PF_TEXT4,

    substr(cowork.PROID,length(cowork.PROID)-4) proid,

    max(DEPTRP.SHIPWSID)||' - '||max(DEPTRP.SHIPWPADR) as "ShipAreaAndAddress",
--  max(COROWFTX_DO.TEXT) Orig_TEXT,      
    SUBSTR(lof_get_corowftx_prefa (
        company_id_i      =>'AT-PREFA',
        co_i              =>COROW.COID,
        coseq_i           =>COROW.COSEQ,
        copos_i           =>COROW.COPOS,
        cosubpos_i        =>COROW.COSUBPOS,
        ftx_qualifier_i   =>'DO'), 1, 
        INSTR(lof_get_corowftx_prefa (
            company_id_i      =>'AT-PREFA',
            co_i              =>COROW.COID,
            coseq_i           =>COROW.COSEQ,
            copos_i           =>COROW.COPOS,
            cosubpos_i        =>COROW.COSUBPOS,
            ftx_qualifier_i   =>'DO'), '###', 1, 1)-1) SHORT_TEXT,

    SUBSTR(lof_get_corowftx_prefa (
        company_id_i      =>'AT-PREFA',
        co_i              =>COROW.COID,
        coseq_i           =>COROW.COSEQ,
        copos_i           =>COROW.COPOS,
        cosubpos_i        =>COROW.COSUBPOS,
        ftx_qualifier_i   =>'DO'), 
        INSTR(lof_get_corowftx_prefa (
            company_id_i      =>'AT-PREFA',
            co_i              =>COROW.COID,
            coseq_i           =>COROW.COSEQ,
            copos_i           =>COROW.COPOS,
            cosubpos_i        =>COROW.COSUBPOS,
            ftx_qualifier_i   =>'DO'),'###', 1, 1)+3, 
            INSTR(lof_get_corowftx_prefa (
                company_id_i      =>'AT-PREFA',
                co_i              =>COROW.COID,
                coseq_i           =>COROW.COSEQ,
                copos_i           =>COROW.COPOS,
                cosubpos_i        =>COROW.COSUBPOS,
                ftx_qualifier_i   =>'DO'),'###', 1, 2)-
                INSTR(lof_get_corowftx_prefa (
                    company_id_i      =>'AT-PREFA',
                    co_i              =>COROW.COID,
                    coseq_i           =>COROW.COSEQ,
                    copos_i           =>COROW.COPOS,
                    cosubpos_i        =>COROW.COSUBPOS,
                    ftx_qualifier_i   =>'DO'),'###', 1, 1)-3)SHORT1,

    SUBSTR(lof_get_corowftx_prefa (
        company_id_i      =>'AT-PREFA',
        co_i              =>COROW.COID,
        coseq_i           =>COROW.COSEQ,
        copos_i           =>COROW.COPOS,
        cosubpos_i        =>COROW.COSUBPOS,
        ftx_qualifier_i   =>'DO'), 
        INSTR(lof_get_corowftx_prefa (
            company_id_i      =>'AT-PREFA',
            co_i              =>COROW.COID,
            coseq_i           =>COROW.COSEQ,
            copos_i           =>COROW.COPOS,
            cosubpos_i        =>COROW.COSUBPOS,
            ftx_qualifier_i   =>'DO'),'###', 1, 2)+3, 
            INSTR(lof_get_corowftx_prefa (
                company_id_i      =>'AT-PREFA',
                co_i              =>COROW.COID,
                coseq_i           =>COROW.COSEQ,
                copos_i           =>COROW.COPOS,
                cosubpos_i        =>COROW.COSUBPOS,
                ftx_qualifier_i   =>'DO'),'###', 1, 3)-
                INSTR(lof_get_corowftx_prefa (
                    company_id_i      =>'AT-PREFA',
                    co_i              =>COROW.COID,
                    coseq_i           =>COROW.COSEQ,
                    copos_i           =>COROW.COPOS,
                    cosubpos_i        =>COROW.COSUBPOS,
                    ftx_qualifier_i   =>'DO'),'###', 1, 2)-3)SHORT2,     
        SUBSTR(lof_get_corowftx_prefa (
        company_id_i      =>'AT-PREFA',
        co_i              =>COROW.COID,
        coseq_i           =>COROW.COSEQ,
        copos_i           =>COROW.COPOS,
        cosubpos_i        =>COROW.COSUBPOS,
        ftx_qualifier_i   =>'DO'), 
            INSTR(lof_get_corowftx_prefa (
            company_id_i      =>'AT-PREFA',
            co_i              =>COROW.COID,
            coseq_i           =>COROW.COSEQ,
            copos_i           =>COROW.COPOS,
            cosubpos_i        =>COROW.COSUBPOS,
            ftx_qualifier_i   =>'DO'),'###', 1, 3)+3, 
            INSTR(lof_get_corowftx_prefa (
                    company_id_i      =>'AT-PREFA',
                    co_i              =>COROW.COID,
                    coseq_i           =>COROW.COSEQ,
                    copos_i           =>COROW.COPOS,
                    cosubpos_i        =>COROW.COSUBPOS,
                    ftx_qualifier_i   =>'DO'),'###', 1, 4)-
                    INSTR(lof_get_corowftx_prefa (
                    company_id_i      =>'AT-PREFA',
                    co_i              =>COROW.COID,
                    coseq_i           =>COROW.COSEQ,
                    copos_i           =>COROW.COPOS,
                    cosubpos_i        =>COROW.COSUBPOS,
                    ftx_qualifier_i   =>'DO'),'###', 1, 3)-3) SHORT3,

--    COFTX_DO.text,    
    COFTX_DO.AVISO1,
    COFTX_DO.AVISO2,    
    COFTX_DO.AVISO3,    
    COFTX_DO.AVISO4,
    
    pbrow.PBROWID,
    to_char(cowork.DLVRYNOTE_PRTDTM,'YYYY/MM/DD') "Print Date"    

FROM   
    PARTY,
    CO,
    COWORK,
    COROW COROW,
    ART,
    CONSIGNMENT CONSIGNMENT,
    PBROW PBROW,
    cartyp,
    ARTCOD,
    DEP,
    DEPTRP,
    corowftx corowftxAMT,
    corowftx corowftxUNL,   
    LOC_PS_LANGUAGE_AT_PREFA,
    RCVCARLOG,
    (SELECT coid,
           coseq,
           company_id,
           text,
           NVL (REGEXP_SUBSTR (LTRIM (text), '^[^###]*'), '') AVISO1,
           NVL (REPLACE (REGEXP_SUBSTR (LTRIM (text),'[^###]+',1,2),'###'),'')AVISO2,
           NVL (REPLACE (REGEXP_SUBSTR (LTRIM (text),'[^###]+',1,3),'###'),'')AVISO3,
           NVL (REPLACE (REGEXP_SUBSTR (LTRIM (text),'[^###]+',1,4),'###'),'')AVISO4                      
      FROM coftx
     WHERE ftx_qualifier = 'DO' AND company_id = 'AT-PREFA') COFTX_DO
         
WHERE  
    COWORK.COID=CO.COID AND 
    COWORK.COSEQ=CO.COSEQ and
    COWORK.COMPANY_ID=CO.COMPANY_ID AND 
	
    COWORK.CONSIGNMENT_ID=CONSIGNMENT.CONSIGNMENT_ID AND 
    COWORK.COMPANY_ID=CONSIGNMENT.COMPANY_ID and

    cowork.WHID = dep.WHID(+) and 
    cowork.DEPARTURE_ID = dep.DEPARTURE_ID(+) and

    dep.WHID = DEPTRP.WHID(+) and 
    dep.DEPARTURE_ID = DEPTRP.DEPARTURE_ID(+) and
	
    COWORK.COID=COROW.COID AND
    COWORK.COMPANY_ID=COROW.COMPANY_ID AND 
    COWORK.COSEQ=COROW.COSEQ and
    COWORK.COSUBSEQ=COROW.COSUBSEQ and    

    COROW.ARTID=ART.ARTID AND 
    COROW.COMPANY_ID=ART.COMPANY_ID and

    COROW.COID=PBROW.COID AND 
    COROW.COMPANY_ID=PBROW.COMPANY_ID AND 
	
    COROW.COPOS=PBROW.COPOS AND 
    COROW.COSEQ=PBROW.COSEQ AND 
    COROW.COSUBPOS=PBROW.COSUBPOS AND 
    COROW.COSUBSEQ=PBROW.COSUBSEQ and
    corow.whid=pbrow.whid and

    COWORK.SHIPTOPARTYID=PARTY.PARTY_ID and
    cowork.SHIPTOPARTYQUALIFIER=PARTY.PARTY_QUALIFIER and    
    COWORK.COMPANY_ID=PARTY.COMPANY_ID and
    PARTY.PARTY_QUALIFIER='CU' and 
    PBROW.PICKQTY != 0 and 

    COWORK.COID=COFTX_DO.COID(+)  AND 
    COWORK.COSEQ=COFTX_DO.COSEQ(+)  AND    
    COWORK.COMPANY_ID=COFTX_DO.COMPANY_ID(+)  AND 
--    COFTX.FTX_QUALIFIER='DO' AND

    COROW.ARTID=ARTCOD.ARTID AND 
    COROW.COMPANY_ID=ARTCOD.COMPANY_ID and
    ARTCOD.olacod='HSC' AND

--TABLE corowftx COID, COSEQ, COPOS, COSUBPOS, COROWKITPOS
    COROW.COID=corowftxAMT.COID(+) AND 
    COROW.COSEQ=corowftxAMT.COSEQ(+) AND 
    COROW.COPOS=corowftxAMT.COPOS(+) AND 
    COROW.COSUBPOS=corowftxAMT.COSUBPOS(+) AND 
    COROW.COMPANY_ID=corowftxAMT.COMPANY_ID(+) AND    
    corowftxAMT.FTX_QUALIFIER ='AMT' AND            

    COROW.COID=corowftxUNL.COID(+) AND 
    COROW.COSEQ=corowftxUNL.COSEQ(+) AND 
    COROW.COPOS=corowftxUNL.COPOS(+) AND 
    COROW.COSUBPOS=corowftxUNL.COSUBPOS(+) AND 
    COROW.COMPANY_ID=corowftxUNL.COMPANY_ID(+) AND    
    corowftxUNL.FTX_QUALIFIER ='UNL' AND            

    PBROW.ITEID=RCVCARLOG.CARID(+) and
    PBROW.whid=RCVCARLOG.whid(+) and
    PBROW.COMPANY_ID=RCVCARLOG.COMPANY_ID(+) and

--  	PARTY.LNGID=LOC_PS_LANGUAGE_AT_PREFA.LNGCODE(+) and
    PARTY.COUNTRYCODE=LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ADRESS(+) and
--test 01-307057624-01 1    4
/*    COWORK.COID = '01-375024697-01' and -- '01-301054892-02' and 
    COWORK.COMPANY_ID='AT-PREFA'  and
    COWORK.COSEQ = 1 AND 
    COWORK.COSUBSEQ = 1  
*/
   COWORK.COID='{?COID_I}' AND 
    COWORK.COMPANY_ID='{?COMPANY_ID_I}'  and
    COWORK.COSEQ={?COSEQ_I} AND 
    COWORK.COSUBSEQ={?COSUBSEQ_I} 
   
    
GROUP BY
    CO.COID, 
    ART.ARTID, 
    ART.ARTNAME1,    
    ART.ARTNAME2, 

    CO.COSEQ, 
    COWORK.COSUBSEQ,
    pbROW.COPOS, 
    
    PBROW.PBTYPE,
    PBROW.PICKQTY,
    PBROW.ORDQTY ,
    
    PBROW.PAKID,
    
    RCVCARLOG.TOTWGT,
    PBROW.WEIGHT,
    
    PARTY.PARTY_ID,
    PARTY.NAME1, 
    PARTY.ADR1, 
    PARTY.ADR2,
    PARTY.PHONE,
    PARTY.POSTCODE, 
    PARTY.CITY, 
    PARTY.COUNTRYCODE,
    COROW.FPSHIPDTM, 
    CO.TERMS_OF_DELIVERY,
    
    ARTCOD.ARTCODE,
    COWORK.DLVRYMETH_ID,
    
    LOC_PS_LANGUAGE_AT_PREFA.LNGCODE,
    LOC_PS_LANGUAGE_AT_PREFA.REPRINT,	
    LOC_PS_LANGUAGE_AT_PREFA.PAGE,
    LOC_PS_LANGUAGE_AT_PREFA.ORIGNAL,
    LOC_PS_LANGUAGE_AT_PREFA.DELIVERY_NOTE_NO,
    LOC_PS_LANGUAGE_AT_PREFA.SHIPED_TO,
    LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ADRESS,
    LOC_PS_LANGUAGE_AT_PREFA.SHIPMENT_FROM,
    LOC_PS_LANGUAGE_AT_PREFA.AVISO,        
    LOC_PS_LANGUAGE_AT_PREFA.DATE1,
    LOC_PS_LANGUAGE_AT_PREFA.FORWARDING_AGENT,
    LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ORDER,
    LOC_PS_LANGUAGE_AT_PREFA.CUSTOMER_ORDER_POS,
    LOC_PS_LANGUAGE_AT_PREFA.WEIGHT,
    LOC_PS_LANGUAGE_AT_PREFA.COMM_CODE,
    LOC_PS_LANGUAGE_AT_PREFA.QUANTITY_SHIPPED,
    LOC_PS_LANGUAGE_AT_PREFA.QUANTITY_ORDERED,
    LOC_PS_LANGUAGE_AT_PREFA.UNIT,
    LOC_PS_LANGUAGE_AT_PREFA.DELIVERY_TERMS,
    LOC_PS_LANGUAGE_AT_PREFA.INSPECTION,
    LOC_PS_LANGUAGE_AT_PREFA.GOODS_ACCEPTED_IN_ORDER,
    LOC_PS_LANGUAGE_AT_PREFA.PRINTED_NAME,
    LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT1,
    LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT2,
    LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT3,
    LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT4,
    LOC_PS_LANGUAGE_AT_PREFA.RF_TEXT5,
    LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT1,
    LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT2,
    LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT3,
    LOC_PS_LANGUAGE_AT_PREFA.PF_TEXT4,
    cowork.PROID,
    
--    COFTX_DO.text,
    COFTX_DO.AVISO1,
    COFTX_DO.AVISO2,    
    COFTX_DO.AVISO3,    
    COFTX_DO.AVISO4,
    
 
    COROW.COID,
    COROW.COSEQ,
    COROW.COPOS,
    COROW.COSUBPOS,

    pbrow.PBROWID,
    cowork.DLVRYNOTE_PRTDTM

    
order by PBrow.copos
