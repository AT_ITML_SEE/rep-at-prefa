/******************************************************************************
NAME: AT-PREFA-Picking_report_DROCKENB.rpt
DESC: Pick calculation
    Picks will be calculated only for orders with status 50 (Pick Finished) or higher
    
    Storage type = ART.ARTGROUP
        PAL columns: PAL, TRA, GFG
        PLG columns: PLG
        KRT columns: KRT [no full pallet picks]
        ZUB columns: PZUB

    Column calculation:
        CO              - at least 1 piece picked
        PALLETPICKS_PAL - only full pallets - PICKQTY/BASEQTY of 'PAL', 'Rol' ('Rol' when highest PAKID)
        CASEPICKS_PAL   - PICKQTY-PALLETPICKS_PAL/BASEQTY of PAKIDs 'Rol' ('Rol' when not highest PAKID), 'KRT', 'Bnd', 'Mag', 'Tfl'
        PIECEPICKS_PAL  - PICKQTY-PALLETPICKS_PAL-CASEPICKS_PAL
        PALLETPICKS_PLG - only full pallets - PICKQTY/BASEQTY of highest PAKID
        CASEPICKS_PLG   - PICKQTY-PALLETPICKS_PLG/BASEQTY of PAKIDs 'KRT', 'Bnd', 'Tfl'
        PIECEPICKS_PLG  - PICKQTY-PALLETPICKS_PLG-PIECEPICKS_PLG
        PICKLINES_KRT   - Picklines (PBROWID)
        CASEPICKS_KRT   - PICKQTY/BASEQTY of PAKID 'KRT' 
        PIECEPICKS_KRT  - PICKQTY-CASEPICKS_KRT
        PALLETPICKS_ZUB - only full pallets - PICKQTY/BASEQTY of highest PAKID
        CASEPICKS_ZUB   - PICKQTY-PALLETPICKS_ZUB/BASEQTY of PAKIDs 'KRT', 'Mag'
        PIECEPICKS_ZUB  - PICKQTY-PALLETPICKS_ZUB-PIECEPICKS_ZUB
        PICKLINES_MM    - Picklines (PBROWID) where any PAKID = 'mm'
        PICKLINES_G     - Picklines (PBROWID) where any PAKID = 'g'
    
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description


1.0 		14.03.2022 		DAKORNHA            	    created
1.1 		24.03.2022 		DAKORNHA            	    fixed piece picks calcucation for (PAL, PLG, KRT, ZUB)
1.2 		01.04.2022 		DAKORNHA            	    change WHERE clause in PAK_LVL - make KDV available for ARTGROUP = 'ZUB' 
1.3 		04.04.2022 		DAKORNHA            	    added PICKLINES_ZUB and make KDV available for all ARTGROUPs
1.4 		04.04.2022 		DAKORNHA            	    exclude PAKs when PAKID = 'KDV' and BASPAKID = 'mm'
1.5         27.04.2022      DAKORNHA    PREFA-113       child CAR haven't been taken into account. Only PBTYPE = 'C' CARCARID IS NULL, PBTYPE IN ('P', 'S') CARCARID IS NOT NULL

******************************************************************************/

WITH
/***************
** DATE_TABLE
***************/
DATE_TABLE AS  
    (SELECT TO_CHAR(SYSDATE+1 - ROWNUM, 'DD.MM.YYYY') AS DTM
        FROM DUAL
        CONNECT BY ROWNUM < 180
    )
/***************
** PAK_LVL
***************/
, PAK_LVL AS
    (SELECT  PAK.ARTID,
        ART.ARTGROUP STORAGE_TYPE,
        DECODE(PAK.PAKID, 'Pkg', 'KRT'
                        , 'K06', 'KRT'
                        , 'K11', 'KRT'
                        , 'K20', 'KRT'
                        , 'K10', 'KRT'
                        , 'K16', 'KRT'
                        , 'K07', 'KRT'
                        , 'K13', 'KRT'
                        , 'K30', 'KRT'
                        , 'K03', 'KRT'
                        , 'K04', 'KRT'
                        , 'K52', 'KRT'
                        , 'K37', 'KRT'
                        , 'K35', 'KRT'
                        , 'K36', 'KRT'
                        , 'K17', 'KRT'
                        , 'K53', 'KRT'
                        , 'K57', 'KRT'
                        , 'K23', 'KRT'
                        , 'KDV', 'KRT'
                        , 'K51', 'KRT'
                        , 'K39', 'KRT'
                        , 'K18', 'KRT'
                        , 'K05', 'KRT'
                        , 'K19', 'KRT'
                        , '110', 'KRT'
                        , '109', 'KRT'
                        , 'K54', 'KRT'
                        , 'K7G', 'KRT'
                        , 'K33', 'KRT'
                        , 'Krt', 'KRT'
                        ,  PAK.PAKID) PAKID,
        PAK.PAKNAME,
        PAK.PAKNAME2,
        PAK.COMPANY_ID,
        RANK () OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) LVL,
        PAK.ARTID || RANK () OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) PACKID,
        PAK.ARTID || CASE WHEN RANK () OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) = 1
                        THEN NULL
                        ELSE RANK () OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) - 1
                    END LOWER_PACKID,
        PAK.BASEQTY                                                                  BASEQTY_LVL_1,
        PAK.PAKID                                                                PAKID_LVL_1,
        LEAD (PAK.BASEQTY) OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC)       BASEQTY_LVL_2,
        DECODE(LEAD (PAK.PAKID) OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC)
                        , 'Pkg', 'KRT'
                        , 'K06', 'KRT'
                        , 'K11', 'KRT'
                        , 'K20', 'KRT'
                        , 'K10', 'KRT'
                        , 'K16', 'KRT'
                        , 'K07', 'KRT'
                        , 'K13', 'KRT'
                        , 'K30', 'KRT'
                        , 'K03', 'KRT'
                        , 'K04', 'KRT'
                        , 'K52', 'KRT'
                        , 'K37', 'KRT'
                        , 'K35', 'KRT'
                        , 'K36', 'KRT'
                        , 'K17', 'KRT'
                        , 'K53', 'KRT'
                        , 'K57', 'KRT'
                        , 'K23', 'KRT'
                        , 'KDV', 'KRT'
                        , 'K51', 'KRT'
                        , 'K39', 'KRT'
                        , 'K18', 'KRT'
                        , 'K05', 'KRT'
                        , 'K19', 'KRT'
                        , '110', 'KRT'
                        , '109', 'KRT'
                        , 'K54', 'KRT'
                        , 'K7G', 'KRT'
                        , 'K33', 'KRT'
                        , 'Krt', 'KRT'
                        ,  LEAD (PAK.PAKID) OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC)) PAKID_LVL_2,
        LEAD (PAK.BASEQTY, 2) OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC)    BASEQTY_LVL_3,
        LEAD (PAK.PAKID, 2) OVER (PARTITION BY PAK.ARTID ORDER BY PAK.BASEQTY DESC) PAKID_LVL_3
        FROM PAK
            JOIN ART 
                ON ART.ARTID = PAK.ARTID
                AND ART.COMPANY_ID = PAK.COMPANY_ID
        WHERE PAK.COMPANY_ID = 'AT-PREFA'
            AND UPPER(PAK.PAKID) NOT IN ('BOX')
            AND NOT (PAK.PAKID = 'KDV' AND PAK.BASPAKID = 'mm')
    )
/***************
** PICK_ROWS
***************/
, PICK_ROWS AS
    (SELECT NVL(PBROW.COID, CAR.COID) COID,
            NVL(PBROW.COSEQ, CAR.COSEQ) COSEQ,
            NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ) COSUBSEQ,
            NVL(PBROW.ARTID, COROW.ARTID) ARTID,
            SUM(NVL(PBROW.PICKQTY, 0)) PICKQTY,
            COUNT(DISTINCT PBROW.PBROWID) PICKROW,
            TO_CHAR(PBROW.PICKDTM, 'DD.MM.YYYY') PICKDTM          
        FROM PBCAR
        JOIN 
            (SELECT CARID,
                    CARCARID,
                    CARTYPID,
                    COMPANY_ID,
                    COID,
                    COSEQ,
                    COSUBSEQ
                FROM CAR
                WHERE COMPANY_ID = 'AT-PREFA'
            UNION
            SELECT CARID,
                    CARCARID,
                    CARTYPID,
                    COMPANY_ID,
                    COID,
                    COSEQ,
                    COSUBSEQ
                FROM CARTRC CAR
                WHERE COMPANY_ID = 'AT-PREFA') CAR
            ON CAR.CARID = PBCAR.CARID
                AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
        JOIN CARTYP
            ON CARTYP.CARTYPID = CAR.CARTYPID
        JOIN PARTY
               ON PARTY.PARTY_QUALIFIER = PBCAR.SHIPTOPARTYQUALIFIER
                AND PARTY.PARTY_ID = PBCAR.SHIPTOPARTYID                    
                AND PARTY.COMPANY_ID = PBCAR.COMPANY_ID
        LEFT JOIN PBROW
            ON PBROW.PBCARID = PBCAR.PBCARID
                AND PBROW.COMPANY_ID = PBCAR.COMPANY_ID
                AND PBROW.PICKDTM IS NOT NULL
        LEFT JOIN COWORK -- NVL due to bug with Parent-Child LC
            ON COWORK.COID = NVL(PBROW.COID, CAR.COID)
                AND COWORK.COSEQ = NVL(PBROW.COSEQ, CAR.COSEQ)
                AND COWORK.COSUBSEQ = NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ)
                AND COWORK.COMPANY_ID = NVL(PBROW.COMPANY_ID, CAR.COMPANY_ID)
                AND COWORK.COSTATID >= 50
        LEFT JOIN COROW -- NVL due to bug with Parent-Child LC
            ON COROW.COID = NVL(PBROW.COID, CAR.COID)
                AND COROW.COSEQ = NVL(PBROW.COSEQ, CAR.COSEQ)
                AND COROW.COSUBSEQ = NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ)
                AND COROW.COPOS = PBROW.COPOS
                AND COROW.COMPANY_ID = NVL(PBROW.COMPANY_ID, CAR.COMPANY_ID)      
        WHERE ((PBCAR.PBTYPE IN ('C') 
                AND CAR.CARCARID IS NULL)
                OR (PBCAR.PBTYPE IN ('P', 'S')
                AND CAR.CARCARID IS NOT NULL)) 
            AND PBCAR.COMPANY_ID = 'AT-PREFA'
        GROUP BY NVL(PBROW.COID, CAR.COID),
            NVL(PBROW.COSEQ, CAR.COSEQ),
            NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ),
            NVL(PBROW.ARTID, COROW.ARTID),
            TO_CHAR(PBROW.PICKDTM, 'DD.MM.YYYY')   
    /**************
    ** HISTORY
    **************/
    UNION
    SELECT NVL(PBROW.COID, CAR.COID) COID,
            NVL(PBROW.COSEQ, CAR.COSEQ) COSEQ,
            NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ) COSUBSEQ,
            NVL(PBROW.ARTID, COROW.ARTID) ARTID,
            SUM(NVL(PBROW.PICKQTY, 0)) PICKQTY,
            COUNT(DISTINCT PBROW.PBROWID) PICKROW,
            TO_CHAR(PBROW.PICKDTM, 'DD.MM.YYYY') PICKDTM          
        FROM PBCARLOG PBCAR
        JOIN 
            (SELECT CARID,
                    CARCARID,
                    CARTYPID,
                    COMPANY_ID,
                    COID,
                    COSEQ,
                    COSUBSEQ
                FROM CAR
                WHERE COMPANY_ID = 'AT-PREFA'
            UNION
            SELECT CARID,
                    CARCARID,
                    CARTYPID,
                    COMPANY_ID,
                    COID,
                    COSEQ,
                    COSUBSEQ
                FROM CARTRC CAR
                WHERE COMPANY_ID = 'AT-PREFA') CAR
            ON CAR.CARID = PBCAR.CARID
                AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
        JOIN CARTYP
            ON CARTYP.CARTYPID = CAR.CARTYPID
        JOIN PARTY
               ON PARTY.PARTY_QUALIFIER = PBCAR.SHIPTOPARTYQUALIFIER
                AND PARTY.PARTY_ID = PBCAR.SHIPTOPARTYID                    
                AND PARTY.COMPANY_ID = PBCAR.COMPANY_ID
        LEFT JOIN PBROWLOG PBROW
            ON PBROW.PBCARID = PBCAR.PBCARID
                AND PBROW.COMPANY_ID = PBCAR.COMPANY_ID
                AND PBROW.PICKDTM IS NOT NULL
        LEFT JOIN COROWTRC COROW -- NVL due to bug with Parent-Child LC
            ON COROW.COID = NVL(PBROW.COID, CAR.COID)
                AND COROW.COSEQ = NVL(PBROW.COSEQ, CAR.COSEQ)
                AND COROW.COSUBSEQ = NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ)
                AND COROW.COPOS = PBROW.COPOS
                AND COROW.COMPANY_ID = NVL(PBROW.COMPANY_ID, CAR.COMPANY_ID)      
        WHERE ((PBCAR.PBTYPE IN ('C') 
                AND CAR.CARCARID IS NULL)
                OR (PBCAR.PBTYPE IN ('P', 'S')
                AND CAR.CARCARID IS NOT NULL))
            AND PBCAR.COMPANY_ID = 'AT-PREFA'
        GROUP BY NVL(PBROW.COID, CAR.COID),
            NVL(PBROW.COSEQ, CAR.COSEQ),
            NVL(PBROW.COSUBSEQ, CAR.COSUBSEQ),
            NVL(PBROW.ARTID, COROW.ARTID),
            TO_CHAR(PBROW.PICKDTM, 'DD.MM.YYYY') 
    )
SELECT
    TO_DATE(DATE_TABLE.DTM, 'DD.MM.YYYY') DTM,
    COUNT(DISTINCT PICK_ROWS.COID||PICK_ROWS.COSEQ) CO,
    /********
    ** PAL
    ********/
    SUM(NVL(CASE WHEN PAL.PAKID_LVL_1 IN ('PAL', 'Rol')
            THEN FLOOR(PICK_ROWS.PICKQTY/PAL.BASEQTY_LVL_1)
            ELSE 0
        END, 0)) PALLETPICKS_PAL,
    SUM(NVL(CASE WHEN PAL.PAKID_LVL_2 IN ('Rol', 'KRT', 'Bnd', 'Mag', 'Tfl')
            THEN FLOOR(MOD(PICK_ROWS.PICKQTY, PAL.BASEQTY_LVL_1)/PAL.BASEQTY_LVL_2)
            ELSE 0
        END, 0)) CASEPICKS_PAL,
    SUM(NVL(CASE WHEN (NVL(PAL.PAKID_LVL_3, 'X') NOT IN ('mm', 'g') 
                    AND NVL(PAL.PAKID_LVL_2, 'X') NOT IN ('mm', 'g') 
                    AND NVL(PAL.PAKID_LVL_1, 'X') NOT IN ('mm', 'g'))
                THEN CASE WHEN PAL.PAKID_LVL_3 IS NULL
                            THEN CASE WHEN NVL(PAL.PAKID_LVL_2, 'X') IN ('Rol', 'KRT', 'Bnd', 'Mag', 'Tfl') OR PAL.PAKID_LVL_2 IS NULL
                                    THEN CASE WHEN NVL(PAL.PAKID_LVL_1, 'X') IN ('PAL', 'Rol') OR PAL.PAKID_LVL_1 IS NULL
                                            THEN 0
                                            ELSE PICK_ROWS.PICKQTY
                                        END
                                    -- NVL(PAL.PAKID_LVL_2, 'X') IN ('Rol', 'KRT', 'Bnd', 'Mag', 'Tfl') OR PAL.PAKID_LVL_2 IS NULL
                                    ELSE FLOOR(NVL(MOD(PICK_ROWS.PICKQTY, PAL.BASEQTY_LVL_1), PICK_ROWS.PICKQTY))
                                END
                            -- PAL.PAKID_LVL_3 IS NULL
                            ELSE FLOOR(MOD(NVL(MOD(PICK_ROWS.PICKQTY, PAL.BASEQTY_LVL_1), PICK_ROWS.PICKQTY), PAL.BASEQTY_LVL_2))
                        END
                ELSE 0
            END, 0)) PIECEPICKS_PAL,
    /********
    ** PLG
    ********/
    SUM(NVL(FLOOR(PICK_ROWS.PICKQTY/PLG.BASEQTY_LVL_1), 0)) PALLETPICKS_PLG,
    SUM(NVL(CASE WHEN PLG.PAKID_LVL_2 IN ('KRT', 'Bnd', 'Tfl')
            THEN FLOOR(MOD(PICK_ROWS.PICKQTY, PLG.BASEQTY_LVL_1)/PLG.BASEQTY_LVL_2)
            ELSE 0
        END, 0)) CASEPICKS_PLG,
    -- use NVL(..., 'X'), otherwise would return FALSE on null values 
    SUM(NVL(CASE WHEN (NVL(PLG.PAKID_LVL_3, 'X') NOT IN ('mm', 'g') 
                AND NVL(PLG.PAKID_LVL_2, 'X') NOT IN ('mm', 'g') 
                AND NVL(PLG.PAKID_LVL_1, 'X') NOT IN ('mm', 'g'))
            THEN CASE WHEN PLG.PAKID_LVL_3 IS NULL 
                        THEN CASE WHEN NVL(PLG.PAKID_LVL_2, 'X') IN ('KRT', 'Bnd', 'Tfl') OR PLG.PAKID_LVL_2 IS NULL
                                THEN CASE WHEN PLG.PAKID_LVL_1 IS NULL
                                        THEN 0
                                        ELSE PICK_ROWS.PICKQTY
                                    END
                                -- NVL(PLG.PAKID_LVL_2, 'X') IN ('KRT', 'Bnd', 'Tfl') OR PLG.PAKID_LVL_2 IS NULL
                                ELSE FLOOR(NVL(MOD(PICK_ROWS.PICKQTY, PLG.BASEQTY_LVL_1), PICK_ROWS.PICKQTY))
                            END
                        -- PLG.PAKID_LVL_3 IS NULL 
                        ELSE FLOOR(MOD(NVL(MOD(PICK_ROWS.PICKQTY, PLG.BASEQTY_LVL_1), PICK_ROWS.PICKQTY), PLG.BASEQTY_LVL_2))
                    END
            ELSE 0
        END, 0)) PIECEPICKS_PLG,
    /********
    ** KRT
    ********/
    SUM(CASE WHEN KRT.STORAGE_TYPE IN ('KRT')
                THEN PICK_ROWS.PICKROW 
                ELSE 0 
            END) PICKLINES_KRT,
    SUM(NVL(CASE WHEN KRT.PAKID_LVL_2 IN ('KRT')
            THEN FLOOR(MOD(PICK_ROWS.PICKQTY, KRT.BASEQTY_LVL_1)/KRT.BASEQTY_LVL_2)
            ELSE 0
        END, 0)) CASEPICKS_KRT,
    -- use NVL(..., 'X'), otherwise would return FALSE on null values 
    SUM(NVL(CASE WHEN (NVL(KRT.PAKID_LVL_3, 'X') NOT IN ('mm', 'g') 
                AND NVL(KRT.PAKID_LVL_2, 'X') NOT IN ('mm', 'g') 
                AND NVL(KRT.PAKID_LVL_1, 'X') NOT IN ('mm', 'g'))
            THEN CASE WHEN KRT.PAKID_LVL_3 IS NULL 
                        THEN CASE WHEN KRT.PAKID_LVL_2 IS NULL
                                THEN CASE WHEN KRT.PAKID_LVL_1 IS NULL
                                        THEN 0
                                        ELSE PICK_ROWS.PICKQTY
                                    END
                                -- KRT.PAKID_LVL_3 IS NULL 
                                ELSE FLOOR(NVL(MOD(PICK_ROWS.PICKQTY, KRT.BASEQTY_LVL_1), PICK_ROWS.PICKQTY))
                            END
                        -- KRT.PAKID_LVL_2 IS NULL
                        ELSE FLOOR(MOD(NVL(MOD(PICK_ROWS.PICKQTY, KRT.BASEQTY_LVL_1), PICK_ROWS.PICKQTY), KRT.BASEQTY_LVL_2))
                    END
            ELSE 0
        END, 0)) PIECEPICKS_KRT,
    /********
    ** ZUB
    ********/
    SUM(NVL(FLOOR(PICK_ROWS.PICKQTY/ZUB.BASEQTY_LVL_1), 0)) PALLETPICKS_ZUB,
    SUM(NVL(CASE WHEN ZUB.PAKID_LVL_2 IN ('KRT', 'Mag')
            THEN FLOOR(MOD(PICK_ROWS.PICKQTY, ZUB.BASEQTY_LVL_1)/ZUB.BASEQTY_LVL_2)
            ELSE 0
        END, 0)) CASEPICKS_ZUB,
    -- use NVL(..., 'X'), otherwise would return FALSE on null values 
    SUM(NVL(CASE WHEN (NVL(ZUB.PAKID_LVL_3, 'X') NOT IN ('mm', 'g') 
                AND NVL(ZUB.PAKID_LVL_2, 'X') NOT IN ('mm', 'g') 
                AND NVL(ZUB.PAKID_LVL_1, 'X') NOT IN ('mm', 'g'))
            THEN CASE WHEN ZUB.PAKID_LVL_3 IS NULL 
                        THEN CASE WHEN NVL(ZUB.PAKID_LVL_2, 'X') IN ('KRT', 'Mag') OR ZUB.PAKID_LVL_2 IS NULL
                                THEN CASE WHEN ZUB.PAKID_LVL_1 IS NULL
                                        THEN 0
                                        ELSE PICK_ROWS.PICKQTY
                                    END
                                -- NVL(ZUB.PAKID_LVL_2, 'X') IN ('KRT', 'Mag') OR ZUB.PAKID_LVL_2 IS NULL
                                ELSE FLOOR(NVL(MOD(PICK_ROWS.PICKQTY, ZUB.BASEQTY_LVL_1), PICK_ROWS.PICKQTY))
                            END
                        -- ZUB.PAKID_LVL_3 IS NULL 
                        ELSE FLOOR(MOD(NVL(MOD(PICK_ROWS.PICKQTY, ZUB.BASEQTY_LVL_1), PICK_ROWS.PICKQTY), ZUB.BASEQTY_LVL_2))
                    END
            ELSE 0
        END, 0)) PIECEPICKS_ZUB,
    SUM(CASE WHEN ZUB.STORAGE_TYPE IN ('ZUB')
                THEN PICK_ROWS.PICKROW 
                ELSE 0 
            END) PICKLINES_ZUB,
    /********
    ** mm
    ********/
    SUM(CASE WHEN NVL(PAL.PAKID_LVL_3, 'X') IN ('mm') 
                    OR NVL(PAL.PAKID_LVL_2, 'X') IN ('mm') 
                    OR NVL(PAL.PAKID_LVL_1, 'X') IN ('mm')
                    OR NVL(PLG.PAKID_LVL_3, 'X') IN ('mm') 
                    OR NVL(PLG.PAKID_LVL_2, 'X') IN ('mm') 
                    OR NVL(PLG.PAKID_LVL_1, 'X') IN ('mm')
                    OR NVL(KRT.PAKID_LVL_3, 'X') IN ('mm') 
                    OR NVL(KRT.PAKID_LVL_2, 'X') IN ('mm') 
                    OR NVL(KRT.PAKID_LVL_1, 'X') IN ('mm')
                    OR NVL(ZUB.PAKID_LVL_3, 'X') IN ('mm') 
                    OR NVL(ZUB.PAKID_LVL_2, 'X') IN ('mm') 
                    OR NVL(ZUB.PAKID_LVL_1, 'X') IN ('mm')
                THEN PICK_ROWS.PICKROW 
                ELSE 0 
            END) PICKLINES_MM,
    /********
    ** g
    ********/
    SUM(CASE WHEN NVL(PAL.PAKID_LVL_3, 'X') IN ('g') 
                    OR NVL(PAL.PAKID_LVL_2, 'X') IN ('g') 
                    OR NVL(PAL.PAKID_LVL_1, 'X') IN ('g')
                    OR NVL(PLG.PAKID_LVL_3, 'X') IN ('g') 
                    OR NVL(PLG.PAKID_LVL_2, 'X') IN ('g') 
                    OR NVL(PLG.PAKID_LVL_1, 'X') IN ('g')
                    OR NVL(KRT.PAKID_LVL_3, 'X') IN ('g') 
                    OR NVL(KRT.PAKID_LVL_2, 'X') IN ('g') 
                    OR NVL(KRT.PAKID_LVL_1, 'X') IN ('g')
                    OR NVL(ZUB.PAKID_LVL_3, 'X') IN ('g') 
                    OR NVL(ZUB.PAKID_LVL_2, 'X') IN ('g') 
                    OR NVL(ZUB.PAKID_LVL_1, 'X') IN ('g')
                THEN PICK_ROWS.PICKROW  
                ELSE 0 
            END) PICKLINES_G
FROM DATE_TABLE
JOIN PICK_ROWS
    ON DATE_TABLE.DTM = PICK_ROWS.PICKDTM
LEFT JOIN PAK_LVL PAL
    ON PAL.ARTID = PICK_ROWS.ARTID
    AND PAL.LVL = 1
    AND PAL.STORAGE_TYPE IN ('PAL', 'TRA', 'GFG')  
LEFT JOIN PAK_LVL PLG
    ON PLG.ARTID = PICK_ROWS.ARTID
    AND PLG.LVL = 1
    AND PLG.STORAGE_TYPE IN ('PLG')  
LEFT JOIN PAK_LVL KRT
    ON KRT.ARTID = PICK_ROWS.ARTID
    AND KRT.LVL = 1
    AND KRT.STORAGE_TYPE IN ('KRT')  
LEFT JOIN PAK_LVL ZUB
    ON ZUB.ARTID = PICK_ROWS.ARTID
    AND ZUB.LVL = 1
    AND ZUB.STORAGE_TYPE IN ('ZUB')  
WHERE TO_DATE(DATE_TABLE.DTM, 'DD.MM.YYYY') 
    BETWEEN  {?DATE_FROM}
    AND  {?DATE_TO} 
GROUP BY 
    TO_DATE(DATE_TABLE.DTM, 'DD.MM.YYYY')
--    PICK_ROWS.COID,
--    PICK_ROWS.COSEQ,
--    --PICK_ROWS.PICKQTY,
--    PICK_ROWS.ARTID
    /********
    ** Keep this here, in case of analyzes
    ********
    PICK_ROWS.ARTID,
    PICK_ROWS.PICKQTY,
    PAL.PAKID_LVL_3,
    PAL.PAKID_LVL_2,
    PAL.PAKID_LVL_1,
    PLG.PAKID_LVL_3,
    PLG.PAKID_LVL_2,
    PLG.PAKID_LVL_1,
    KRT.PAKID_LVL_3,
    KRT.PAKID_LVL_2,
    KRT.PAKID_LVL_1,
    ZUB.PAKID_LVL_3,
    ZUB.PAKID_LVL_2,
    ZUB.PAKID_LVL_1,
    PAL.BASEQTY_LVL_1,
    PAL.BASEQTY_LVL_2,
    PAL.BASEQTY_LVL_3,
    PLG.BASEQTY_LVL_1,
    PLG.BASEQTY_LVL_2,
    PLG.BASEQTY_LVL_3,
    KRT.BASEQTY_LVL_1,
    KRT.BASEQTY_LVL_2,
    KRT.BASEQTY_LVL_3,
    ZUB.BASEQTY_LVL_1,
    ZUB.BASEQTY_LVL_2,
    ZUB.BASEQTY_LVL_3,
    PICK_ROWS.COID,
    PICK_ROWS.COSEQ*/
ORDER BY TO_DATE(DATE_TABLE.DTM, 'DD.MM.YYYY')


