/* Formatted on 01/03/2022 13:02:21 (QP5 v5.336) */
/******************************************************************************
NAME: Loading_report_AT-PREFA.rpt
DESC: shows loaded and closed load carriers details, to each customer order
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description


1.0 		01.03.2022 		DAKORNHA            	    created
1.1 		02.03.2022 		DAKORNHA            	    added PBCAR.PBCARSTAT "R" and "H"
1.2 		04.03.2022 		DAKORNHA            	    UNION history tables
1.3 		17.03.2022 		DAKORNHA            	    Sorting by customer (name, zip, city)
                                                        added DECODE() for weight, if PBCAR.WEIGHT/PBCARLOG.TOTWGT = 0 or NULL
                                                        added UNION for CAR/CARTRC table
1.4 		21.03.2022 		DAKORNHA            	    JOIN CONSIGNMENT for COID and COSEQ - in some cases that values are NULL in PBCAR and CAR
                                                        PBCAR may be in live AND history tables. Excluded PBCAR in history query, which is still in live tables (NOT EXISTS)
1.5 		23.03.2022 		DAKORNHA            	    added PBCAR.PBCARSTAT = 'S'
1.6 		05.04.2022 		DAKORNHA            	    exclude merged loadcarriers (PBCAR.MERGE_TO_CARID IS NULL)
1.7 		06.04.2022 		DAKORNHA            	    changed sorting - ZIP before NAME
1.8 		14.04.2022 		DAKORNHA    PREFA-108       adding PBCAR.PBCARSTAT = 'O' for pallet picks
1.9 		04.05.2022 		DAKORNHA    PREFA-119       adding SAUMRINNE when ARTNAME2 like '%SRI%'
2.0 		27.05.2022 		DAKORNHA    IMI-2234        add Departure Route ID
2.1         28.06.2022      DAKORNHA                    BRUTTO was not consistent calculated. Added NETTO and renewed BRUTTO calculation
2.2         29.08.2022      DAKORNHA    IMI-2521        new calculation for 
                                                        - live tables: NETTO (CAR.TOTWGT-CARTYP.WEIGHT) and BRUTTO (CAR.TOTWGT BRUTTO)
                                                        - history tables: NETTO (PBCAR.TOTWGT-CARTYP.WEIGHT) and BRUTTO (PBCAR.TOTWGT BRUTTO)
2.3         22.12.2022      DAKORNHA    IMI-2320        corrected weight calculation to be the same on PS, CPL, eSchenker and Loading Report
2.4         17.01.2023      DAKORNHA    IMI-2320        mixed up BRUTTO and NETTO. Fixed it.
2.5         23.01.2023      DAKORNHA    IMI-2320        added join PBCAR.DEPARTURE_ID = CAR.DEPARTURE_ID (in case of returns) 
2.6         17-02-2023      PBRUGGER	INC006507228 	history tables AND CAR.COID=PBCAR.COID_SINGLE         AND CAR.COSEQ=PBCAR.COSEQ_SINGLE   -- AQEEL Joined with COID     
                                                         Report shows duplicate CARID from CARTRC table from last year
2.7         17-02-2023      PBRUGGER    IMI-2320        added function to determine brutto/netto wgt   
2.8         18-10-2023      DAKORNHA    INC007262949    removed group by of ART.ARTGROUP (was uneccessary) and ART.ARTNAME2
                                                        MAX(ART.ARTNAME2) added
                                         
******************************************************************************/

SELECT DEP.DEPARTURE_ID,
    DEP.ROUTID,
    DECODE(DEP.DEPARTURE_PICKSTAT, 'C', 'Not started',
                                'O', 'Open',
                                'F', 'Finished',
                                'H', 'On Hold',
                                'R', 'Ready',
                            DEP.DEPARTURE_PICKSTAT) DEP_PICKSTAT,
    NVL(NVL(PBROW.COID, CAR.COID), CONSIGNMENT.COID) COID,
    NVL(NVL(COWORK.COSEQ, CAR.COSEQ), CONSIGNMENT.COSEQ) COSEQ,
    DECODE (COWORK.COSTATID, '00', 'Registered',
                            '05', 'Departure Requested',
                            '10', 'Departure Connected',
                            '19', 'Departure Connection Failure',
                            '20', 'Pick Generated',
                            '28', 'Error at Balance Check',
                            '29', 'Pick Generation Failure',
                            '30', 'Pick Started',
                            '40', 'Pick Completed',
                            '50', 'Pick Finished',
                            '80', 'Pick Reported',
                        COWORK.COSTATID) CUSTOMER_ORDER_STATUS,
    PARTY.NAME1 SHIP_TO_PARTY_NAME,
    PARTY.ADR1 SHIP_TO_PARTY_ADR1,
    PARTY.ADR2 SHIP_TO_PARTY_ADR2,
    PARTY.POSTCODE SHIP_TO_PARTY_ZIP,
    PARTY.CITY SHIP_TO_PARTY_CITY,
    LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'N',DEP.DEPARTURE_ID) AS NETTO,
    LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'Y',DEP.DEPARTURE_ID) AS BRUTTO,
    NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID) AS CARID,
    CARTYP.CARTYPID,
    CARTYP.CARNAME,
    DECODE (PBCAR.PBCARSTAT, 'C', 'Closed',
                            'H', 'Hold',
                            'K', 'Packing',
                            'L', 'Loaded',
                            'O', 'Open',
                            'R', 'Released',
                            'P', 'Pick Load Carrier Check',
                            'T', 'Processed',
                            'S', 'Consolidate',
                        PBCAR.PBCARSTAT) LC_Status,
    PBCAR.TOWPADR               "Outbound Gate Location",
    PBCAR.WWS_DROPCODE_SHIP     "Drop location",
    MAX(CASE WHEN ART.ARTNAME2 LIKE '%SRI%' 
        THEN 'SAUMRINNE'
        ELSE NULL 
    END) INFO
FROM DEP
JOIN PBCAR
    ON PBCAR.WHID = DEP.WHID 
        AND PBCAR.DEPARTURE_ID = DEP.DEPARTURE_ID 
        AND ((PBCAR.PBTYPE IN ('C', 'S') 
            AND PBCAR.PBCARSTAT in ('C', 'L', 'R', 'H', 'S'))
        OR (PBCAR.PBTYPE IN ('P') 
            AND PBCAR.PBCARSTAT in ('C', 'L', 'R', 'H', 'S', 'O')))
        AND PBCAR.COMPANY_ID = 'AT-PREFA'
        AND PBCAR.MERGE_TO_CARID IS NULL
JOIN (SELECT CAR.COID,
            CAR.COSEQ,
            CAR.CARID,
            CAR.CARCARID,
            CAR.CARTYPID,
            CAR.TOTWGT,
            CAR.COMPANY_ID,
            CAR.DEPARTURE_ID
        FROM CAR
    UNION
    SELECT CARTRC.COID,
            CARTRC.COSEQ,
            CARTRC.CARID,
            CARTRC.CARCARID,
            CARTRC.CARTYPID,
            CARTRC.TOTWGT,
            CARTRC.COMPANY_ID,
            CARTRC.DEPARTURE_ID
        FROM CARTRC) CAR
    ON CAR.CARID = NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)
        AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
        AND CAR.DEPARTURE_ID = PBCAR.DEPARTURE_ID
		AND CAR.COID=PBCAR.COID_SINGLE         AND CAR.COSEQ=PBCAR.COSEQ_SINGLE   -- AQEEL Joined with COID  
        AND CAR.CARCARID IS NULL
JOIN CARTYP
    ON CARTYP.CARTYPID = CAR.CARTYPID
JOIN PARTY
       ON PARTY.PARTY_QUALIFIER = PBCAR.SHIPTOPARTYQUALIFIER
        AND PARTY.PARTY_ID = PBCAR.SHIPTOPARTYID                    
        AND PARTY.COMPANY_ID = PBCAR.COMPANY_ID
LEFT JOIN PBROW
    ON PBROW.PBCARID = PBCAR.PBCARID
        AND PBROW.COMPANY_ID = PBCAR.COMPANY_ID
LEFT JOIN PAK 
    ON PAK.ARTID = PBROW.ARTID
    AND PAK.COMPANY_ID = PBROW.COMPANY_ID 
    AND PAK.PAKID = PBROW.PAKID
LEFT JOIN ART
    ON ART.ARTID = PBROW.ARTID
    AND ART.COMPANY_ID = PBROW.COMPANY_ID
    AND UPPER(ART.ARTNAME2) = 'SRI'
LEFT JOIN CONSIGNMENT
    ON CONSIGNMENT.CONSIGNMENT_ID = PBCAR.CONSIGNMENT_ID
    AND CONSIGNMENT.COMPANY_ID = PBCAR.COMPANY_ID
LEFT JOIN COWORK -- NVL due to bug with Parent-Child LC
    ON COWORK.COID = NVL(NVL(PBROW.COID, CAR.COID), CONSIGNMENT.COID)
        AND COWORK.COSEQ = NVL(NVL(PBROW.COSEQ, CAR.COSEQ), CONSIGNMENT.COSEQ)
        AND COWORK.COMPANY_ID = NVL(PBROW.COMPANY_ID, CAR.COMPANY_ID)
LEFT OUTER JOIN RCVCARLOG ON
    PBROW.ITEID = RCVCARLOG.CARID
    AND PBROW.WHID = RCVCARLOG.WHID
    AND PBROW.COMPANY_ID = RCVCARLOG.COMPANY_ID
    AND RCVCARLOG.RCVCARSTAT IN ('40', '50') -- 40 = Received 50 = Processed
WHERE DEP.DEPARTURE_ID = '{?DEPARTURE_ID}'
GROUP BY DEP.DEPARTURE_ID, -- GROUP BY due to multiple PBROWs when Pallet pick (Parent-Child LC)
    DEP.ROUTID,
    DEP.DEPARTURE_PICKSTAT,
    NVL(NVL(PBROW.COID, CAR.COID), CONSIGNMENT.COID),
    NVL(NVL(COWORK.COSEQ, CAR.COSEQ), CONSIGNMENT.COSEQ),
    COWORK.COSTATID, 
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.POSTCODE,
    PARTY.CITY,
    CARTYP.WEIGHT,
    CAR.TOTWGT,
    NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID),
    CARTYP.CARTYPID,
    CARTYP.CARNAME,
    PBCAR.PBCARSTAT,
    PBCAR.TOWPADR,
    PBCAR.WWS_DROPCODE_SHIP
/********************************************
** HISTORY
********************************************/
UNION
SELECT DEP.DEPARTURE_ID,
    DEP.ROUTID,
    'Finished' DEP_PICKSTAT,
    NVL(NVL(PBROW.COID, CAR.COID), CONSIGNMENT.COID) COID,
    NVL(NVL(COWORK.COSEQ, CAR.COSEQ), CONSIGNMENT.COSEQ) COSEQ,
   'Pick Reported' CUSTOMER_ORDER_STATUS,
    PARTY.NAME1 SHIP_TO_PARTY_NAME,
    PARTY.ADR1 SHIP_TO_PARTY_ADR1,
    PARTY.ADR2 SHIP_TO_PARTY_ADR2,
    PARTY.POSTCODE SHIP_TO_PARTY_ZIP,
    PARTY.CITY SHIP_TO_PARTY_CITY,
    LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'N',DEP.DEPARTURE_ID) AS NETTO,
    LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'Y',DEP.DEPARTURE_ID) AS BRUTTO,
    NVL(pbcar.consolidation_to_carid, pbcar.carid) AS CARID,
    CARTYP.CARTYPID,
    CARTYP.CARNAME,
   'Loaded' LC_Status,
    PBCAR.TOWPADR               "Outbound Gate Location",
    PBCAR.WWS_DROPCODE_SHIP     "Drop location",
    MAX(CASE WHEN ART.ARTNAME2 LIKE '%SRI%' 
        THEN 'SAUMRINNE'
        ELSE NULL
    END) INFO
FROM DEPLOG DEP
JOIN PBCARLOG PBCAR
    ON PBCAR.WHID = DEP.WHID 
        AND PBCAR.DEPARTURE_ID = DEP.DEPARTURE_ID 
        AND PBCAR.PBTYPE IN ('C', 'P', 'S') 
        AND PBCAR.COMPANY_ID = 'AT-PREFA'
        AND PBCAR.MERGE_TO_CARID IS NULL
JOIN (SELECT CAR.COID,
            CAR.COSEQ,
            CAR.CARID,
            CAR.CARCARID,
            CAR.CARTYPID,
            CAR.TOTWGT,
            CAR.COMPANY_ID,
            CAR.DEPARTURE_ID
        FROM CAR
    UNION
    SELECT CARTRC.COID,
            CARTRC.COSEQ,
            CARTRC.CARID,
            CARTRC.CARCARID,
            CARTRC.CARTYPID,
            CARTRC.TOTWGT,
            CARTRC.COMPANY_ID,
            CARTRC.DEPARTURE_ID
        FROM CARTRC) CAR
    ON CAR.CARID = NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)
        AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
		AND CAR.COID=PBCAR.COID_SINGLE         AND CAR.COSEQ=PBCAR.COSEQ_SINGLE   -- AQEEL Joined with COID 
        AND CAR.DEPARTURE_ID = PBCAR.DEPARTURE_ID
        AND CAR.CARCARID IS NULL
JOIN CARTYP
    ON CARTYP.CARTYPID = CAR.CARTYPID
JOIN PARTY
       ON PARTY.PARTY_QUALIFIER = PBCAR.SHIPTOPARTYQUALIFIER
        AND PARTY.PARTY_ID = PBCAR.SHIPTOPARTYID                    
        AND PARTY.COMPANY_ID = PBCAR.COMPANY_ID
LEFT JOIN PBROWLOG PBROW
    ON PBROW.PBCARID = PBCAR.PBCARID
        AND PBROW.COMPANY_ID = PBCAR.COMPANY_ID
LEFT JOIN PAK 
    ON PAK.ARTID = PBROW.ARTID
    AND PAK.COMPANY_ID = PBROW.COMPANY_ID 
    AND PAK.PAKID = PBROW.PAKID
LEFT JOIN ART
    ON ART.ARTID = PBROW.ARTID
    AND ART.COMPANY_ID = PBROW.COMPANY_ID
    AND UPPER(ART.ARTNAME2) = 'SRI'
LEFT JOIN CONSIGNMENTTRC CONSIGNMENT
    ON CONSIGNMENT.CONSIGNMENT_ID = PBCAR.CONSIGNMENT_ID
    AND CONSIGNMENT.COMPANY_ID = PBCAR.COMPANY_ID
LEFT JOIN COTRC COWORK -- NVL due to bug with Parent-Child LC
    ON COWORK.COID = NVL(NVL(PBROW.COID, CAR.COID), CONSIGNMENT.COID)
        AND COWORK.COSEQ = NVL(NVL(PBROW.COSEQ, CAR.COSEQ), CONSIGNMENT.COSEQ)
        AND COWORK.COMPANY_ID = NVL(PBROW.COMPANY_ID, CAR.COMPANY_ID)
LEFT OUTER JOIN RCVCARLOG ON
    PBROW.ITEID = RCVCARLOG.CARID
    AND PBROW.WHID = RCVCARLOG.WHID
    AND PBROW.COMPANY_ID = RCVCARLOG.COMPANY_ID
    AND RCVCARLOG.RCVCARSTAT IN ('40', '50') -- 40 = Received 50 = Processed
WHERE DEP.DEPARTURE_ID = '{?DEPARTURE_ID}'
    AND NOT EXISTS 
        (SELECT 'X' 
            FROM PBCAR PB 
            WHERE PB.COMPANY_ID = 'AT-PREFA' 
                AND PB.PBCARID = PBCAR.PBCARID)
GROUP BY DEP.DEPARTURE_ID, -- GROUP BY due to multiple PBROWs when Pallet pick (Parent-Child LC)
    DEP.ROUTID,
    NVL(NVL(PBROW.COID, CAR.COID), CONSIGNMENT.COID),
    NVL(NVL(COWORK.COSEQ, CAR.COSEQ), CONSIGNMENT.COSEQ),
    PARTY.NAME1,
    PARTY.ADR1,
    PARTY.ADR2,
    PARTY.POSTCODE,
    PARTY.CITY,
    CARTYP.WEIGHT,
    PBCAR.TOTWGT,
    NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID),
    CARTYP.CARTYPID,
    CARTYP.CARNAME,
    PBCAR.TOWPADR,
    PBCAR.WWS_DROPCODE_SHIP
ORDER BY 1, 
    SHIP_TO_PARTY_ZIP,
    SHIP_TO_PARTY_NAME,
    SHIP_TO_PARTY_CITY,
    3,
    4,
    CARTYPID,
    CARNAME,
    CARID
	
/* Formatted on 01/03/2022 13:02:21 (QP5 v5.336) */
/******************************************************************************
NAME: Loading_report_AT-PREFA.rpt
SUBREPORT: DEP_HEAD_LC
DESC: shows loaded and closed load carriers details, to each customer order
REVISIONS: 

Ver 		Date      		Player       SN/JIRA 		Description


1.0 		01.03.2022 		DAKORNHA            	    created
1.1 		02.03.2022 		DAKORNHA            	    added PBCAR.PBCARSTAT "R" and "H"
1.2 		04.03.2022 		DAKORNHA            	    UNION history tables
1.3 		11.03.2022 		DAKORNHA            	    Weight will be taken from PBCAR instead of CAR
1.4 		17.03.2022 		DAKORNHA            	    added DECODE() for weight, if PBCAR.WEIGHT/PBCARLOG.TOTWGT = 0 or NULL
                                                        added UNION for CAR/CARTRC table
1.5 		21.03.2022 		DAKORNHA            	    PBCAR may be in live and history tables. excluded PBCAR in history query, which is still in live tables (NOT EXISTS)
1.6 		23.03.2022 		DAKORNHA            	    added PBCAR.PBCARSTAT = 'S'
1.7 		05.04.2022 		DAKORNHA            	    exclude merged loadcarriers (PBCAR.MERGE_TO_CARID IS NULL)
1.8 		14.04.2022 		DAKORNHA    PREFA-108       adding PBCAR.PBCARSTAT = 'O' for pallet picks
1.9         28.06.2022      DAKORNHA                    BRUTTO was not consistent calculated. Added NETTO and renewed BRUTTO calculation
2.0         29.08.2022      DAKORNHA    IMI-2521        new calculation for 
                                                        - live tables: NETTO (CAR.TOTWGT-CARTYP.WEIGHT) and BRUTTO (CAR.TOTWGT BRUTTO)
                                                        - history tables: NETTO (PBCAR.TOTWGT-CARTYP.WEIGHT) and BRUTTO (PBCAR.TOTWGT BRUTTO)
2.0         22.12.2022      DAKORNHA    IMI-2320        corrected weight calculation to be the same on PS, CPL, eSchenker and Loading Report 
2.1         23.01.2023      DAKORNHA    IMI-2320        corrected weight caluclation again + added join PBCAR.DEPARTURE_ID = CAR.DEPARTURE_ID (in case of returns)
2.2         30.01.2023      DAKORNHA    IMI-2320        corrected weight caluclation again  
2.3         27-02-2023      PBRUGGER    INC006507228 	history tables AND CAR.COID=PBCAR.COID_SINGLE         AND CAR.COSEQ=PBCAR.COSEQ_SINGLE   -- AQEEL Joined with COID     
2.4         17-02-2023      PBRUGGER    IMI-2320        added function to determine brutto/netto wgt                                                           Report shows duplicate CARID from CARTRC table from last year
******************************************************************************/

SELECT 
    DEPARTURE_ID,
    CARTYPID,
    CARNAME,
    COUNT(DISTINCT CARID) AS COUNT_CARID,
    SUM(NETTO) AS NETTO,
    SUM(BRUTTO) AS BRUTTO
FROM 
    (
    SELECT
        DISTINCT
        PBCAR.DEPARTURE_ID,
        CARTYP.CARTYPID,
        CARTYP.CARNAME,
        LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'N',PBCAR.DEPARTURE_ID) AS NETTO,
        LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'Y',PBCAR.DEPARTURE_ID) AS BRUTTO,
        NVL(pbcar.consolidation_to_carid, pbcar.carid) AS CARID
    FROM PBCAR
    JOIN (SELECT CAR.COID,
                CAR.COSEQ,
                CAR.CARID,
                CAR.CARCARID,
                CAR.CARTYPID,
                CAR.TOTWGT,
                CAR.COMPANY_ID,
                CAR.DEPARTURE_ID
            FROM CAR
        UNION
        SELECT CARTRC.COID,
                CARTRC.COSEQ,
                CARTRC.CARID,
                CARTRC.CARCARID,
                CARTRC.CARTYPID,
                CARTRC.TOTWGT,
                CARTRC.COMPANY_ID,
                CARTRC.DEPARTURE_ID
            FROM CARTRC) CAR 
        ON CAR.CARID = NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)
            AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
            AND CAR.COID=PBCAR.COID_SINGLE         AND CAR.COSEQ=PBCAR.COSEQ_SINGLE   -- AQEEL Joined with COID  
            AND CAR.DEPARTURE_ID = PBCAR.DEPARTURE_ID
    JOIN CARTYP ON CARTYP.CARTYPID = CAR.CARTYPID
    LEFT JOIN PBROW PBROW
        ON PBROW.PBCARID = PBCAR.PBCARID
            AND PBROW.COMPANY_ID = PBCAR.COMPANY_ID
    LEFT JOIN PAK 
        ON PAK.ARTID = PBROW.ARTID
        AND PAK.COMPANY_ID = PBROW.COMPANY_ID 
        AND PAK.PAKID = PBROW.PAKID
    LEFT OUTER JOIN RCVCARLOG ON
        PBROW.ITEID = RCVCARLOG.CARID
        AND PBROW.WHID = RCVCARLOG.WHID
        AND PBROW.COMPANY_ID = RCVCARLOG.COMPANY_ID
        AND RCVCARLOG.RCVCARSTAT IN ('40', '50') -- 40 = Received 50 = Processed
    WHERE PBCAR.COMPANY_ID = 'AT-PREFA'
        AND CAR.CARCARID IS NULL
        AND PBCAR.MERGE_TO_CARID IS NULL
            AND ((PBCAR.PBTYPE IN ('C', 'S') 
                AND PBCAR.PBCARSTAT in ('C', 'L', 'R', 'H', 'S'))
            OR (PBCAR.PBTYPE IN ('P') 
                AND PBCAR.PBCARSTAT in ('C', 'L', 'R', 'H', 'S', 'O')))
        AND PBCAR.DEPARTURE_ID = '{?DEPARTURE_ID}'
    /********************************************
    ** HISTORY
    ********************************************/
    UNION
    SELECT
        DISTINCT
        PBCAR.DEPARTURE_ID,
        CARTYP.CARTYPID,
        CARTYP.CARNAME,
        LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'N',PBCAR.DEPARTURE_ID) AS NETTO,
        LOP_AT_PREFA.CALC_CARWGT(NVL(pbcar.consolidation_to_carid, pbcar.carid),'Y',PBCAR.DEPARTURE_ID) AS BRUTTO,
        NVL(pbcar.consolidation_to_carid, pbcar.carid) AS CARID
    FROM PBCARLOG PBCAR
    JOIN (SELECT CAR.COID,
                CAR.COSEQ,
                CAR.CARID,
                CAR.CARCARID,
                CAR.CARTYPID,
                CAR.TOTWGT,
                CAR.COMPANY_ID,
                CAR.DEPARTURE_ID
            FROM CAR
        UNION
        SELECT CARTRC.COID,
                CARTRC.COSEQ,
                CARTRC.CARID,
                CARTRC.CARCARID,
                CARTRC.CARTYPID,
                CARTRC.TOTWGT,
                CARTRC.COMPANY_ID,
                CARTRC.DEPARTURE_ID
            FROM CARTRC) CAR
        ON CAR.CARID = NVL(PBCAR.CONSOLIDATION_TO_CARID, PBCAR.CARID)
            AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
            AND CAR.COID=PBCAR.COID_SINGLE         AND CAR.COSEQ=PBCAR.COSEQ_SINGLE   -- AQEEL Joined with COID
            AND CAR.DEPARTURE_ID = PBCAR.DEPARTURE_ID
    --        AND PBCAR.CONSOLIDATION_TO_CARID IS NULL
    JOIN CARTYP ON CARTYP.CARTYPID = CAR.CARTYPID
    LEFT JOIN PBROWLOG PBROW
        ON PBROW.PBCARID = PBCAR.PBCARID
            AND PBROW.COMPANY_ID = PBCAR.COMPANY_ID
    LEFT JOIN PAK 
        ON PAK.ARTID = PBROW.ARTID
        AND PAK.COMPANY_ID = PBROW.COMPANY_ID 
        AND PAK.PAKID = PBROW.PAKID
    LEFT OUTER JOIN RCVCARLOG ON
        PBROW.ITEID = RCVCARLOG.CARID
        AND PBROW.WHID = RCVCARLOG.WHID
        AND PBROW.COMPANY_ID = RCVCARLOG.COMPANY_ID
        AND RCVCARLOG.RCVCARSTAT IN ('40', '50') -- 40 = Received 50 = Processed
    WHERE PBCAR.COMPANY_ID = 'AT-PREFA'
        AND CAR.CARCARID IS NULL
        AND PBCAR.MERGE_TO_CARID IS NULL
        AND PBCAR.PBTYPE IN ('C', 'P', 'S')
        AND NOT EXISTS 
            (SELECT 'X' 
                FROM PBCAR PB 
                WHERE PB.COMPANY_ID = 'AT-PREFA' 
                    AND PB.PBCARID = PBCAR.PBCARID)
        AND PBCAR.DEPARTURE_ID = '{?DEPARTURE_ID}'    
    )
GROUP BY 
DEPARTURE_ID,
CARTYPID,
CARNAME