/***************************************************************************************************************
   NAME: AT-PREFA_Backlog-Recovery_KPIs.rpt
   DESC: KPIs for Customer Orders, Customer Order Lines and Picks in the 3 Sections: OPEN, FINISHED and BACKLOG 
   REVISIONS:
	Ver			Date			Player			Source						Description
	---------	----------		------------	---------------------		---------------------------------
	1.0			2022/01/21		MARIEMEL		implementation				initial version (Request & Definition by Peter Seelhofer)
    2.0         2022/02/05      MARIEMEL        Bugfix                      Date-Comparisons (where-clauses) were not correct implemented, new solution using 'YYYYMMDD' Format in where-clauses
   
****************************************************************************************************************/


/***************

RECOVERY BACKLOG KPIs REPORTING
-------------------------------

SQL1 = ACTUAL TOTAL ORDERS PER DAY
SQL2 = ACTUAL TOTAL ORDERS LINES PER DAY
SQL3 = ACTUAL OUTPUT ORDERLINES PER DAY
SQL4 = ACTUAL OUTPUT PICKS PER DAY
SQL5 = ACTUAL BACKLOG ORDERS
SQL6 = ACTUAL BACKLOG ORDER LINES

****************/



with date_s as 
(
select 
to_char(to_date({?Datum}), 'YYYYMMDD') as datum
-- use next line to run SELECT in SQL Developer / Toad / ...
-- to_char(to_date('31.01.2022'), 'YYYYMMDD') as datum
from dual
)
select


/*********************** 
SQL 1 = ACTUAL TOTAL ORDERS PER DAY 
            --> Amount of CUSTOMER ORDERS connected to a Depature with Depature-Date EQUALS Date used as parameter. (no matter which status Customer Orders or Departures have)
***********************/

(	 select
    count(*) AS SQL1
    from (
            select coid || '-' || coseq from cowork where DEPARTURE_ID in (
                        select DEPARTURE_ID from dep 
                            where dep.whid = 'PRE1' 
                                    and to_char(dep.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
                        UNION           
                        select DEPARTURE_ID from deplog
                            where deplog.whid = 'PRE1' 
                                    and to_char(deplog.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
                        ) group by coid || '-' || coseq
        UNION
            select coid || '-' || coseq from cotrc where DEPARTURE_ID in (
                        select DEPARTURE_ID from dep 
                            where dep.whid = 'PRE1' 
                                    and to_char(dep.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
                        UNION           
                        select DEPARTURE_ID from deplog
                            where deplog.whid = 'PRE1' 
                                    and to_char(deplog.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
                        ) group by coid || '-' || coseq
            )
) as SQL1,



/*********************** 
SQL 2 = ACTUAL TOTAL ORDERS LINES PER DAY 
            --> Amount of Customer Order LINES connected to a Depature with Depature-Date EQUALS Date used as parameter. (no matter which status Customer Orders or Departures have)
***********************/

(	 select 
	count(*) AS SQL2
	from (
			select coid || '-' || coseq || '-' || copos from corow where 
				coid || '-' || coseq 
				in (
						select coid || '-' || coseq from cowork where DEPARTURE_ID in (
									select DEPARTURE_ID from dep 
										where dep.whid = 'PRE1' 
												and to_char(dep.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
								UNION           
									select DEPARTURE_ID from deplog
										where deplog.whid = 'PRE1' 
												and to_char(deplog.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
									) group by coid || '-' || coseq
					UNION
						select coid || '-' || coseq from cotrc where DEPARTURE_ID in (
									select DEPARTURE_ID from dep 
										where dep.whid = 'PRE1' 
												and to_char(dep.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
								UNION           
									select DEPARTURE_ID from deplog
										where deplog.whid = 'PRE1' 
												and to_char(deplog.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
									) group by coid || '-' || coseq
						) group by coid || '-' || coseq || '-' || copos
			UNION
			
			select coid || '-' || coseq || '-' || copos from corowtrc where 
				coid || '-' || coseq 
				in (
						select coid || '-' || coseq from cowork where DEPARTURE_ID in (
									select DEPARTURE_ID from dep 
										where dep.whid = 'PRE1' 
												and to_char(dep.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
								UNION           
									select DEPARTURE_ID from deplog
										where deplog.whid = 'PRE1' 
												and to_char(deplog.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
									) group by coid || '-' || coseq
					UNION
						select coid || '-' || coseq from cotrc where DEPARTURE_ID in (
									select DEPARTURE_ID from dep 
										where dep.whid = 'PRE1' 
												and to_char(dep.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
								UNION           
									select DEPARTURE_ID from deplog
										where deplog.whid = 'PRE1' 
												and to_char(deplog.DEPARTURE_DTM, 'YYYYMMDD') = date_s.datum
									) group by coid || '-' || coseq
						) group by coid || '-' || coseq || '-' || copos
			)
) as SQL2,



/*********************** 
SQL 3 = ACTUAL OUTPUT ORDERLINES PER DAY
            --> Amount of PICKED Customer Order LINES for a specific day (Pick-Date EQUALS Date used as parameter)
***********************/

(    select 
    count(*) AS SQL3
    from 
    (select 
    P.coid || '-' || P.coseq || '-' || P.cosubseq || '-' || P.copos as COROWUNIQUE
    from 
    PBROW P
    where 
    P.company_id = 'AT-PREFA'
    and P.pickqty <> '0'
    and to_char(P.pickdtm, 'YYYYMMDD') = date_s.datum
    group by P.coid || '-' || P.coseq || '-' || P.cosubseq || '-' || P.copos
    UNION
    select 
    PL.coid || '-' || PL.coseq || '-' || PL.cosubseq || '-' || PL.copos
    from 
    PBROWLOG PL
    where 
    PL.company_id = 'AT-PREFA'
    and PL.pickqty <> '0'
    and to_char(PL.pickdtm, 'YYYYMMDD') = date_s.datum
    group by PL.coid || '-' || PL.coseq || '-' || PL.cosubseq || '-' || PL.copos)
) as SQL3,



/*********************** 
SQL 4 = ACTUAL OUTPUT PICKS PER DAY 
            --> Amount of PICKS (Picks = each single SSCC Scan) for a specific day (Pick-Date EQUALS Date used as parameter)
***********************/

(    select 
    count(*) AS SQL4
    from 
    (select 
        PBROWID
    from 
        PBROW P
    where 
        P.company_id = 'AT-PREFA'
        and P.pickqty <> '0'
        and to_char(P.pickdtm, 'YYYYMMDD') = date_s.datum    
    UNION
    select 
        PBROWID
    from 
        PBROWLOG PL
    where 
        PL.company_id = 'AT-PREFA'
        and PL.pickqty <> '0'
        and to_char(PL.pickdtm, 'YYYYMMDD') = date_s.datum
        )
) as SQL4,



/***********************
SQL 5 = ACTUAL BACKLOG ORDERS
            --> Amount of CUSTOMER ORDERS connected to a Depature with Depature-Date EQUAL OR YOUNGER than Date used as parameter. (Customer Orders with status NOT "Pick Completed", "Pick Finshed" or "Pick Reported")
***********************/

(	 select 
	count(*) as SQL5
	from (
		select * from cowork where 
		COMPANY_ID = 'AT-PREFA' 
		and COSTATID < '40'
		and DEPARTURE_ID in 
			(
			select DEPARTURE_ID from dep where 
				whid = 'PRE1' 
				and DEPARTURE_PICKSTAT != 'F'
				and to_char(DEPARTURE_DTM, 'YYYYMMDD') <= date_s.datum
			)
		)
) as SQL5,			



/***********************
SQL 6 = ACTUAL BACKLOG ORDER LINES
            --> Amount of Customer Order LINES connected to a Depature with Depature-Date EQUAL OR YOUNGER than Date used as parameter. (Customer Order LINES with status NOT "Pick Finished" or "Pick Reported")
***********************/

(	 select 
	count(*) as SQL6
	from (
		select * from corow where
		COMPANY_ID = 'AT-PREFA'
		and corowstat not in ('5','8')
		and coid || '-' || coseq || '-' || cosubseq in 
			(
			select   
			coid || '-' || coseq || '-' || cosubseq 
			from cowork where 
			COMPANY_ID = 'AT-PREFA' 
			and COSTATID < '40'
			and DEPARTURE_ID in 
					(
					select DEPARTURE_ID from dep where 
					whid = 'PRE1' 
					and DEPARTURE_PICKSTAT != 'F'
					and to_char(DEPARTURE_DTM, 'YYYYMMDD') <= date_s.datum
					)
			)
		)
) as SQL6

from 
date_s
