/******************************************************************************
NAME: AT-PREFA-Pick_statistics.rpt
DESC: Pick calculation
    ZUB = Accessories - stored physically loose (no pallet) on a location within LVL 2 PAKID 
    
        ARTGROUP <> ZUB:
        - Pallet pick count based on distinct NVL (CAR.CARCARID, CAR.CARID)
        - Case pick counts each PBROWID
        ARTGROUP = ZUB:
        -calculates picks based on PAK levels. Max. 3 levels
         (PAL is always level 3, when existing)

  
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		01.03.2022 		DAKORNHA            	    created
1.1         02.03.2022      DAKORNHA                    UNION tables CAR and CARTRC in all SELECTs

******************************************************************************/

WITH
/***************
** DATE_TABLE
***************/
DATE_TABLE AS
        (SELECT TO_CHAR(SYSDATE+1 - ROWNUM, 'YYYY-MM-DD') AS DTM
            FROM DUAL
            CONNECT BY ROWNUM < 180
        )
/***************
** PICKDATA_EXCL_ZUB
***************/
, PICKDATA_EXCL_ZUB
    AS
        (SELECT PBROW.PBROWID,
            PBROW.WHID,
            PBROW.WSID,
            PBROW.WPADR,
            NVL (CAR.CARCARID, CAR.CARID) CARID,
            TO_CHAR(PBROW.PICKDTM, 'YYYY-MM-DD') PICKDTM,
            ART.ARTID
        FROM COWORK  COWORK
            JOIN PBROW PBROW
                ON     PBROW.COID = COWORK.COID
                   AND PBROW.COSEQ = COWORK.COSEQ
                   AND PBROW.COMPANY_ID = COWORK.COMPANY_ID
                   AND PBROW.PICKDTM IS NOT NULL
            JOIN PBCAR PBCAR 
                ON PBCAR.PBCARID = PBROW.PBCARID
            JOIN (SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CAR
                    UNION
                    SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CARTRC) CAR
                ON CAR.CARID = PBCAR.CARID
                    AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
            JOIN ART 
                ON ART.ARTID = PBROW.ARTID
                AND ART.COMPANY_ID = PBROW.COMPANY_ID
                AND ART.ARTGROUP != 'ZUB'           --ZUB products will be calculated via PAK
        WHERE COWORK.COMPANY_ID = 'AT-PREFA'
        GROUP BY PBROW.PBROWID,
            PBROW.WHID,
            PBROW.WSID,
            PBROW.WPADR,
            NVL (CAR.CARCARID, CAR.CARID),
            TO_CHAR(PBROW.PICKDTM, 'YYYY-MM-DD'),
            ART.ARTID
        UNION
        SELECT PBROWL.PBROWID,
            PBROWL.WHID,
            PBROWL.WSID,
            PBROWL.WPADR,
            NVL (CAR.CARCARID, CAR.CARID) CARID,
            TO_CHAR(PBROWL.PICKDTM, 'YYYY-MM-DD') PICKDTM,
                ART.ARTID
        FROM COWORKLOG  COWORKL
            JOIN PBROWLOG PBROWL
                ON     PBROWL.COID = COWORKL.COID
                   AND PBROWL.COSEQ = COWORKL.COSEQ
                   AND PBROWL.COMPANY_ID = COWORKL.COMPANY_ID
                   AND PBROWL.PICKDTM IS NOT NULL
            JOIN PBCARLOG PBCARL 
                ON PBCARL.PBCARID = PBROWL.PBCARID
            JOIN (SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CAR
                    UNION
                    SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CARTRC) CAR
                ON PBCARL.CARID = CAR.CARID
            JOIN ART 
                ON ART.ARTID = PBROWL.ARTID
                AND ART.COMPANY_ID = PBROWL.COMPANY_ID
                AND ART.ARTGROUP != 'ZUB'           --ZUB products will be calculated via PAK
        WHERE COWORKL.COMPANY_ID = 'AT-PREFA'
        GROUP BY PBROWL.PBROWID,
            PBROWL.WHID,
            PBROWL.WSID,
            PBROWL.WPADR,
            NVL (CAR.CARCARID, CAR.CARID),
            TO_CHAR(PBROWL.PICKDTM, 'YYYY-MM-DD'),
            ART.ARTID
        )
 /***************************************************************************
**                  START 
**        ARTGROUP='ZUB' Picks Calculation
*****************************************************************************/               
/***************
** PAK_LVL
** Get PAK levels
**
** PAKID = 'PAL' always LVL 3 for ARTGROUP = 'ZUB' 
***************/
, PAK_LVL 
    AS
        (SELECT RANK () OVER (PARTITION BY ART.ARTID ORDER BY BASEQTY) AS LVL,
            ART.ARTID 
                || RANK () OVER (PARTITION BY ART.ARTID ORDER BY BASEQTY) PACKID,
            ART.ARTID 
                || CASE
                    WHEN RANK () OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) = 1
                        THEN NULL
                        ELSE RANK () OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) - 1
                    END LOWER_PACKID,
            PAK.BASEQTY BASEQTY_LVL_1,
            PAK.PAKID PAKID_LVL_1,
            CASE
                WHEN LEAD (PAK.PAKID) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) = 'PAL' 
                    THEN NULL
                    ELSE LEAD (PAK.BASEQTY) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) 
                END    BASEQTY_LVL_2,
            CASE 
                WHEN LEAD (PAK.PAKID) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) = 'PAL' 
                    THEN NULL
                    ELSE LEAD (PAK.PAKID) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) 
                END               PAKID_LVL_2,
            CASE 
                WHEN LEAD (PAK.PAKID) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) = 'PAL' 
                    THEN LEAD (PAK.BASEQTY) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY)
                    ELSE LEAD (PAK.BASEQTY, 2) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) 
                END    BASEQTY_LVL_3,
            CASE 
                WHEN LEAD (PAK.PAKID) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) = 'PAL' 
                    THEN LEAD (PAK.PAKID) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY)
                    ELSE LEAD (PAK.PAKID, 2) OVER (PARTITION BY ART.ARTID ORDER BY PAK.BASEQTY) 
                END               PAKID_LVL_3,
            PAK.ARTID,
            PAK.PAKID,
            PAK.PAKNAME,
            PAK.PAKNAME2,
            PAK.WEIGHT,
            PAK.HEIGHT,
            PAK.LENGTH,
            PAK.WIDTH,
            PAK.VOLUME,
            PAK.COMPANY_ID
        FROM PAK
        JOIN ART 
            ON ART.ARTID = PAK.ARTID
                AND ART.COMPANY_ID = PAK.COMPANY_ID
                AND ART.ARTGROUP = 'ZUB'
        WHERE PAK.COMPANY_ID = 'AT-PREFA'
        )
/***************
** PICKDATA_INCL_ZUB
** Get pickrows and quantity
***************/
, PICKDATA_INCL_ZUB
    AS
        (SELECT PBROW.PBROWID,
                PBROW.WHID,
                PBROW.WSID,
                PBROW.WPADR,
                NVL (CAR.CARCARID, CAR.CARID) CARID,
                TO_CHAR(PBROW.PICKDTM, 'YYYY-MM-DD') PICKDTM,
                ART.ARTID,
                PBROW.PICKQTY
        FROM COWORK  COWORK
            JOIN PBROW PBROW
                ON     PBROW.COID = COWORK.COID
                   AND PBROW.COSEQ = COWORK.COSEQ
                   AND PBROW.COMPANY_ID = COWORK.COMPANY_ID
                   AND PBROW.PICKDTM IS NOT NULL
            JOIN PBCAR PBCAR 
                ON PBCAR.PBCARID = PBROW.PBCARID
            JOIN (SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CAR
                    UNION
                    SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CARTRC) CAR
                ON CAR.CARID = PBCAR.CARID
                    AND CAR.COMPANY_ID = PBCAR.COMPANY_ID
            JOIN ART 
                ON ART.ARTID = PBROW.ARTID
                AND ART.COMPANY_ID = PBROW.COMPANY_ID
                AND ART.ARTGROUP = 'ZUB'           --ZUB products will be calculated via PAK
        WHERE COWORK.COMPANY_ID = 'AT-PREFA'
        GROUP BY PBROW.PBROWID,
            PBROW.WHID,
            PBROW.WSID,
            PBROW.WPADR,
            NVL (CAR.CARCARID, CAR.CARID),
            TO_CHAR(PBROW.PICKDTM, 'YYYY-MM-DD'),
            ART.ARTID,
            PBROW.PICKQTY
        UNION
        SELECT PBROWL.PBROWID,
            PBROWL.WHID,
            PBROWL.WSID,
            PBROWL.WPADR,
            NVL (CAR.CARCARID, CAR.CARID) CARID,
            TO_CHAR(PBROWL.PICKDTM, 'YYYY-MM-DD') PICKDTM,
            ART.ARTID,
            PBROWL.PICKQTY
        FROM COWORKLOG  COWORKL
            JOIN PBROWLOG PBROWL
                ON     PBROWL.COID = COWORKL.COID
                   AND PBROWL.COSEQ = COWORKL.COSEQ
                   AND PBROWL.COMPANY_ID = COWORKL.COMPANY_ID
                   AND PBROWL.PICKDTM IS NOT NULL
            JOIN PBCARLOG PBCARL 
                ON PBCARL.PBCARID = PBROWL.PBCARID
            JOIN (SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CAR
                    UNION
                    SELECT CARID, 
                        CARCARID,
                        COMPANY_ID
                    FROM CARTRC) CAR
                ON PBCARL.CARID = CAR.CARID
            JOIN ART 
                ON ART.ARTID = PBROWL.ARTID
                AND ART.COMPANY_ID = PBROWL.COMPANY_ID
                AND ART.ARTGROUP = 'ZUB'           --ZUB products will be calculated via PAK
        WHERE COWORKL.COMPANY_ID = 'AT-PREFA'
        GROUP BY PBROWL.PBROWID,
            PBROWL.WHID,
            PBROWL.WSID,
            PBROWL.WPADR,
            NVL (CAR.CARCARID, CAR.CARID),
            TO_CHAR(PBROWL.PICKDTM, 'YYYY-MM-DD'),
            ART.ARTID,
            PBROWL.PICKQTY
        )
/***************
** ZUB_PICKS
** Connect PAK levels to pick rows/quantity
***************/
, ZUB_PICKS 
    AS
        (SELECT WHID,
            WSID,
            PICKDTM,
            SUM(CALC) CALC
        FROM
            (SELECT PICKDATA_INCL_ZUB.WHID,
                PICKDATA_INCL_ZUB.WSID,
                PICKDATA_INCL_ZUB.PICKDTM,
                PAK_LVL.ARTID,
                PAK_LVL.BASEQTY_LVL_3,
                PAK_LVL.PAKID_LVL_3,
                PAK_LVL.BASEQTY_LVL_2,
                PAK_LVL.PAKID_LVL_2,
                PAK_LVL.BASEQTY_LVL_1,
                PAK_LVL.PAKID_LVL_1,
                NVL(FLOOR(NVL(SUM(PICKDATA_INCL_ZUB.PICKQTY),0)/PAK_LVL.BASEQTY_LVL_3), 0) PICKS_LVL3,
                NVL(NVL(MOD(SUM(PICKDATA_INCL_ZUB.PICKQTY), PAK_LVL.BASEQTY_LVL_3), SUM(PICKDATA_INCL_ZUB.PICKQTY)), 0) REST_LVL3,
                NVL(FLOOR(NVL(MOD(SUM(PICKDATA_INCL_ZUB.PICKQTY), PAK_LVL.BASEQTY_LVL_3), SUM(PICKDATA_INCL_ZUB.PICKQTY))/PAK_LVL.BASEQTY_LVL_2), 0) PICKS_LVL2,
                NVL(MOD(NVL(MOD(SUM(PICKDATA_INCL_ZUB.PICKQTY), PAK_LVL.BASEQTY_LVL_3), SUM(PICKDATA_INCL_ZUB.PICKQTY)), NVL(PAK_LVL.BASEQTY_LVL_2, 0)), 0) REST_LVL2,
                NVL(MOD(NVL(MOD(SUM(PICKDATA_INCL_ZUB.PICKQTY), PAK_LVL.BASEQTY_LVL_3), SUM(PICKDATA_INCL_ZUB.PICKQTY)), NVL(PAK_LVL.BASEQTY_LVL_2, 0))/PAK_LVL.BASEQTY_LVL_1, 0) PICKS_LVL1,
                NVL(FLOOR(NVL(SUM(PICKDATA_INCL_ZUB.PICKQTY),0)/PAK_LVL.BASEQTY_LVL_3), 0) --LVL3
                    + NVL(FLOOR(NVL(MOD(SUM(PICKDATA_INCL_ZUB.PICKQTY), PAK_LVL.BASEQTY_LVL_3), SUM(PICKDATA_INCL_ZUB.PICKQTY))/PAK_LVL.BASEQTY_LVL_2), 0) --LVL2
                    + NVL(NVL(MOD(SUM(PICKDATA_INCL_ZUB.PICKQTY), PAK_LVL.BASEQTY_LVL_3), SUM(PICKDATA_INCL_ZUB.PICKQTY))/PAK_LVL.BASEQTY_LVL_1, 0) CALC --LVL1
            FROM PICKDATA_INCL_ZUB
            LEFT JOIN PAK_LVL
                ON PAK_LVL.ARTID = PICKDATA_INCL_ZUB.ARTID
                AND PAK_LVL.LVL = 1 --LVL1 also holds information for LVL2 and LVL3
            GROUP BY PICKDATA_INCL_ZUB.WHID,
                PICKDATA_INCL_ZUB.WSID,
                PICKDATA_INCL_ZUB.PICKDTM,
                PAK_LVL.ARTID,
                PAK_LVL.BASEQTY_LVL_3,
                PAK_LVL.PAKID_LVL_3,
                PAK_LVL.BASEQTY_LVL_2,
                PAK_LVL.PAKID_LVL_2,
                PAK_LVL.BASEQTY_LVL_1,
                PAK_LVL.PAKID_LVL_1
            )
        GROUP BY WHID,
            WSID,
            PICKDTM
        )
/***************************************************************************
**                  END 
**        ARTGROUP='ZUB' Picks Calculation
*****************************************************************************/      
/***************************************************************************
**  JOIN Tables
*****************************************************************************/  
SELECT  DECODE (WS.WSID, 'PRP', '# Picks Hochregal ['||WS.WSID||']',
                        'PLP', '# Picks Langgut ['||WS.WSID||']',
                        'PLM', '# Picks Kartons ['||WS.WSID||']',
                        'PRA', '# Picks Voll-PAL (Hochregal) ['||WS.WSID||']',
                        'PLO', '# Picks Voll-PLG (Langgut) ['||WS.WSID||']',
                    'Sonstige') AREA,
        COUNT (DISTINCT 
            CASE WHEN NVL (PICKDATA_EXCL_ZUB.WSID, WS.WSID) in ('PRP', 'PLP', 'PLM')
                THEN PICKDATA_EXCL_ZUB.CARID||PICKDATA_EXCL_ZUB.WSID||PICKDATA_EXCL_ZUB.WPADR||PICKDATA_EXCL_ZUB.PBROWID       --PICKS INCL. CHILD
                ELSE PICKDATA_EXCL_ZUB.CARID||PICKDATA_EXCL_ZUB.WSID||PICKDATA_EXCL_ZUB.WPADR                                  --PICKS EXCL. CHILD
            END) PICK_EXCL_ZUB,
        NVL(ZUB_PICKS.CALC, 0) PICK_INCL_ZUB,
        DATE_TABLE.DTM,
        TO_CHAR(TO_DATE(DATE_TABLE.DTM, 'YYYY-MM-DD'), 'Day') WEEKDAY
FROM WS
    CROSS JOIN DATE_TABLE
    LEFT JOIN PICKDATA_EXCL_ZUB
        ON PICKDATA_EXCL_ZUB.WHID = WS.WHID 
            AND PICKDATA_EXCL_ZUB.WSID = WS.WSID
            AND DATE_TABLE.DTM = PICKDATA_EXCL_ZUB.PICKDTM
    LEFT JOIN ZUB_PICKS
        ON ZUB_PICKS.WHID = WS.WHID 
            AND ZUB_PICKS.WSID = WS.WSID
            AND DATE_TABLE.DTM = ZUB_PICKS.PICKDTM
WHERE WS.WHID = 'PRE1'
        AND DATE_TABLE.DTM <= TO_CHAR ({?DATE_TO}, 'YYYY-MM-DD')
        AND DATE_TABLE.DTM >= TO_CHAR ({?DATE_FROM}, 'YYYY-MM-DD')
GROUP BY DECODE (WS.WSID, 'PRP', '# Picks Hochregal ['||WS.WSID||']',
                        'PLP', '# Picks Langgut ['||WS.WSID||']',
                        'PLM', '# Picks Kartons ['||WS.WSID||']',
                        'PRA', '# Picks Voll-PAL (Hochregal) ['||WS.WSID||']',
                        'PLO', '# Picks Voll-PLG (Langgut) ['||WS.WSID||']',
                    'Sonstige'),
        NVL(ZUB_PICKS.CALC, 0),
        DATE_TABLE.DTM,
        TO_CHAR(TO_DATE(DATE_TABLE.DTM, 'YYYY-MM-DD'), 'Day')
ORDER BY 4, 1