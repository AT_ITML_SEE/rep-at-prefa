/******************************************************************************
NAME: AT-PREFA_Trace_Item_Load_Inventory.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player      SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		21.12.2022      DAKORNHA    IMI-3015        created

******************************************************************************/

SELECT   itechgrf.event_dtm
    , itechgrf.empid
    , wp.whid
    , wp.wsid
    , wp.wpadr
    , ite.artid
    , ite.company_id
    , ite.pakid                                        AS pakid
    , ite.iteid
    , itechgrf.storqty + (itechgrf.event_qty * -1)     storqty
    , itechgrf.storqty                                 AS auditedqty
    , itechgrf.event_qty                               AS diffqty
    , DECODE(ite.iteid, NULL, 'Yes', 'No')             AS empty_loc
FROM wp
LEFT JOIN car
    ON car.whid = wp.whid
    AND car.wsid = wp.wsid
    AND car.wpadr = wp.wpadr
LEFT JOIN ite
    ON ite.carid = car.carid
    AND wp.whid = ite.whid
    AND car.company_id = ite.company_id
LEFT JOIN itechg_iteevent_vw itechgrf
    ON itechgrf.iteid = ite.iteid
    AND itechgrf.whid = wp.whid
    AND itechgrf.itechgcodid = 'RF'
    AND TRUNC(itechgrf.event_dtm) BETWEEN TRUNC({?DATE_FROM}) AND TRUNC({?DATE_TO})+1
WHERE    wp.whid = 'PRE1'
ORDER BY itechgrf.event_dtm DESC NULLS LAST
