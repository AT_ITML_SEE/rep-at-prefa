--OLD
/******************************************************************************
NAME: {?Client}_KPI.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created
1.1 		24.01.2023      DAKORNHA    IMI-2966        full refactoring
1.2         16.02.2023      PBRUGGER    IMI-2966        ^-> continue

******************************************************************************/
 
WITH
/*******************************************************************************
** RECEIVING
*******************************************************************************/
    inbounds AS (
        SELECT
        TRUNC(DEL.ENDDTM) as ENDDTM
        , COUNT(DISTINCT 
                        CASE 
                            WHEN ARTGRP.ARTGROUP NOT IN ('KRT', 'PLG')
                            THEN NVL(RCVCAR.carcarid,RCVCAR.carid)
                            ELSE NULL 
                        END) AS sum_pal
        , COUNT(DISTINCT 
                        CASE 
                            WHEN ARTGRP.ARTGROUP = 'KRT' 
                            THEN NVL(RCVCAR.CARCARID,RCVCAR.CARID)
                            ELSE NULL 
                        END) AS sum_krt
        , COUNT(DISTINCT 
                        CASE 
                            WHEN ARTGRP.ARTGROUP = 'PLG' 
                            THEN NVL(RCVCAR.CARCARID,RCVCAR.CARID)
                            ELSE NULL 
                        END) AS sum_plg   
        FROM RCVROWLOG RCVROW
        INNER JOIN DELLOG DEL ON RCVROW.COMPANY_ID = DEL.COMPANY_ID AND RCVROW.DELID = DEL.DELID
        INNER JOIN RCVCARLOG RCVCAR ON RCVCAR.COMPANY_ID = RCVROW.COMPANY_ID AND RCVCAR.RCVCARID = RCVROW.RCVCARID
        INNER JOIN ART ON RCVROW.ARTID = ART.ARTID
            AND RCVROW.COMPANY_ID = art.COMPANY_ID
        INNER JOIN ARTGRP
                ON ART.ARTGROUP = ARTGRP.ARTGROUP
                AND ART.COMPANY_ID = ARTGRP.COMPANY_ID
        WHERE RCVROW.company_id = '{?Client}'
        AND RCVROW.WHID = 'PRE1'
        AND trunc(DEL.ENDDTM) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
        GROUP BY trunc(DEL.ENDDTM)
        )        
/*******************************************************************************
** STOCK
*******************************************************************************/
    , stock AS (
        /* Formatted on 23/01/2023 17:10:07 (QP5 v5.381) */
        SELECT
            COUNT(DISTINCT
                CASE 
                    WHEN car.wsid IN ('PRA', 'PRP', 'PRX') 
                    THEN car.wpadr 
                    ELSE NULL 
                END) AS num_PAL
            , COUNT(DISTINCT
                CASE 
                    WHEN car.wsid IN ('PLM') 
                    THEN car.carid 
                    ELSE NULL  
                END) AS num_KRT
            , COUNT(DISTINCT
                CASE 
                    WHEN car.wsid IN ('PLO', 'PLP')
                    THEN car.wpadr 
                    ELSE NULL  
                END) AS num_PLG
            , COUNT(CASE 
                    WHEN car.wsid NOT IN ('PRA', 'PRP', 'PRX', 'PLM', 'PLO', 'PLP')
                    THEN car.wpadr 
                    ELSE NULL 
                END) AS num_others
        FROM ite
        INNER JOIN car
             ON car.carid = ite.carid
             AND car.company_id = ite.company_id
        WHERE ite.company_id = '{?Client}'
        ORDER BY 1)      
/*******************************************************************************
** PICKING
*******************************************************************************/
    , picks AS (
        SELECT 					  
        TRUNC(pickdtm) AS pick_dtm
        , COUNT(DISTINCT (DECODE(pzid, 'PPR', pbrowid, NULL)))    AS num_pal
        , COUNT(DISTINCT (DECODE(pzid, 'PPW', pbrowid, NULL)))    AS num_krt
        , COUNT(DISTINCT (DECODE(pzid, 'PPL', pbrowid, NULL)))    AS num_longgoods
        FROM
            (SELECT 
                pbrow.pickdtm
                , pbrow.pzid
                , pbrow.wsid
                , pbrow.pbtype
                , pbrow.pbrowid
            FROM pbrow
            WHERE pbrow.whid = 'PRE1'
                AND pbrow.company_id = '{?Client}'
                AND pbrow.pbtype = 'C'
                AND TRUNC(pbrow.pickdtm) 								   
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            UNION
            SELECT 
                pbrowlog.pickdtm
                , pbrowlog.pzid
                , pbrowlog.wsid
                , pbrowlog.pbtype
                , pbrowlog.pbrowid
            FROM pbrowlog
            WHERE pbrowlog.whid = 'PRE1'
                AND pbrowlog.company_id = '{?Client}'
                AND pbrowlog.pbtype = 'C'
                AND TRUNC(pbrowlog.pickdtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
                )
        GROUP BY TRUNC(pickdtm)
        )      
/*******************************************************************************
** LOADING
*******************************************************************************/
    , loading AS (
        SELECT 
            TRUNC(load_upddtm) AS load_dtm
            , COUNT(DISTINCT co) AS num_co 
            , COUNT(DISTINCT carid) AS num_carid
        FROM
            (SELECT  
                DISTINCT
                pbrow.coid||pbrow.coseq AS co
                , pbcar.load_upddtm
                , car.carid
            FROM pbrow
            INNER JOIN pbcar
                ON pbrow.pbcarid = pbcar.pbcarid
                AND pbrow.whid = pbcar.whid
                AND pbrow.company_id = pbcar.company_id
                AND pbrow.departure_ID = pbcar.departure_ID
            INNER JOIN
                (SELECT carid
                    , carcarid
                    , cartypid
                    , coid
                    , coseq
                    , company_id
                    , totwgt
                    , totvol
                    , departure_id
                FROM   car
                WHERE  company_id = '{?Client}'
                UNION
                SELECT carid
                    , carcarid
                    , cartypid
                    , coid
                    , coseq
                    , company_id
                    , totwgt
                    , totvol
                    , departure_id
                FROM   cartrc
                WHERE  company_id = '{?Client}') car
                    ON car.carid = pbcar.carid
                    AND car.company_id = pbcar.company_id
                    AND car.departure_id = pbcar.departure_id
                    AND car.carcarid IS NULL
            WHERE pbcar.whid = 'PRE1'
                AND pbcar.pbtype IN ('C', 'P', 'S') 
                AND pbcar.company_id = '{?Client}'
                AND TRUNC(pbcar.load_upddtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            UNION ALL
            SELECT 
                DISTINCT
                pbrowlog.coid||pbrowlog.coseq AS co
                , pbcarlog.load_upddtm
                , car.carid
            FROM pbrowlog
            INNER JOIN pbcarlog
                ON pbrowlog.pbcarid = pbcarlog.pbcarid
                AND pbrowlog.whid = pbcarlog.whid
                AND pbrowlog.company_id = pbcarlog.company_id
                AND pbrowlog.departure_ID = pbcarlog.departure_ID
            INNER JOIN
                (SELECT carid
                    , carcarid
                    , cartypid
                    , coid
                    , coseq
                    , company_id
                    , totwgt
                    , totvol
                    , departure_id
                FROM   car
                WHERE  company_id = '{?Client}'
                UNION
                SELECT carid
                    , carcarid
                    , cartypid
                    , coid
                    , coseq
                    , company_id
                    , totwgt
                    , totvol
                    , departure_id
                FROM   cartrc
                WHERE  company_id = '{?Client}') car
                    ON car.carid = pbcarlog.carid
                    AND car.company_id = pbcarlog.company_id
                    AND car.departure_id = pbcarlog.departure_id
                    AND car.carcarid IS NULL
            WHERE pbcarlog.whid = 'PRE1'
                AND pbcarlog.pbtype IN ('C', 'P', 'S') 
                AND pbcarlog.company_id = '{?Client}'
                AND NOT EXISTS (
                    SELECT 1
                    FROM pbrow
                    WHERE pbrowlog.pbrowid = pbrow.pbrowid
                        AND pbrowlog.whid = pbrow.whid
                        AND pbrowlog.company_id = pbrow.company_id)
                AND TRUNC(pbcarlog.load_upddtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to}))
        GROUP BY TRUNC(load_upddtm)
        )
/*******************************************************************************
** INVOICING
*******************************************************************************/
/* RECEIVED_ORDERS_PER_DATE */
    , RECEIVED_ORDERS_PER_DATE AS (
        SELECT TRUNC(insertdtm) AS dtm
            , SUM(orders) orders
        FROM 
            (SELECT DISTINCT
                   co.insertdtm AS insertdtm
                 , COUNT(DISTINCT co.coid || co.coseq) orders
            FROM co
            WHERE co.company_id = '{?Client}'
                AND TRUNC(co.insertdtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            GROUP BY co.insertdtm
            UNION
            SELECT DISTINCT
                cotrc.insertdtm AS insertdtm
                , COUNT(DISTINCT cotrc.coid || cotrc.coseq)
            FROM cotrc
            WHERE cotrc.whid = 'PRE1'
                AND cotrc.company_id = '{?Client}'
                AND TRUNC(cotrc.insertdtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            GROUP BY cotrc.insertdtm)
        GROUP BY TRUNC(insertdtm)        
        )
/* DONE ORDERS PER DATE */
    , DONE_ORDERS_PER_DATE AS (
        SELECT TRUNC(coworklog.insertdtm)            dtm
            , COUNT(DISTINCT coworklog.coid || coworklog.coseq)     orders
        FROM coworklog
        WHERE coworklog.company_id = '{?Client}'
            AND coworklog.text = 'New Status: 40'
            AND TRUNC(coworklog.insertdtm)
                BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
        GROUP BY TRUNC(coworklog.insertdtm))
/* OPEN ORDERS PER DATE */
    , OPEN_ORDERS_PER_DATE AS (
        SELECT TRUNC({?date_to})     AS dtm
            , COUNT(DISTINCT cowork.coid||cowork.coseq) AS orders
        FROM cowork
            WHERE cowork.whid = 'PRE1'
                AND cowork.company_id = '{?Client}'
                AND cowork.costatid < 30
                AND TRUNC(cowork.insertdtm) <= TRUNC({?date_to})
            ),
/* RECEIVED_ROWS_PER_DATE */
    RECEIVED_ROWS_PER_DATE AS (
        SELECT 
            TRUNC(insertdtm) AS dtm
            , SUM (order_rows) AS order_rows
        FROM 
            (SELECT DISTINCT
                co.insertdtm AS insertdtm
                , COUNT(DISTINCT co.coid||co.coseq||corow.copos) AS order_rows
            FROM co
            INNER JOIN corow
                ON corow.coid = co.coid
                AND corow.coseq = co.coseq
            WHERE co.company_id = '{?Client}'
                AND TRUNC(co.insertdtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            GROUP BY co.insertdtm
            UNION
            SELECT DISTINCT
                cotrc.insertdtm AS insertdtm
                , COUNT(DISTINCT cotrc.coid||cotrc.coseq||corow.copos) AS pick_rows
            FROM cotrc
            INNER JOIN corowtrc corow
                ON corow.coid = cotrc.coid
                AND corow.coseq = cotrc.coseq
                AND corow.cosubseq = cotrc.cosubseq
            WHERE cotrc.company_id = '{?Client}'
                AND TRUNC(cotrc.insertdtm) 
                    BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            GROUP BY cotrc.insertdtm)
        GROUP BY TRUNC(insertdtm)),
/* DONE ROWS PER DATE */
    DONE_ROWS_PER_DATE AS (
        SELECT   
            pickdtm AS DTM
            , SUM(order_line_comp) AS order_rows
        FROM     (
             SELECT TRUNC(pbrow.pickdtm)    AS pickdtm
                  , art.artid               AS artid
                  , pbrow.pbtype
                  --, pbrow.pbrowid
                  , pbrow.coid
                  , pbrow.copos
                  , pbrow.coseq
                  , NVL(
                        CASE
                            WHEN corowstat >= 5 THEN
                                CASE
                                    WHEN (SELECT MAX(pickdtm)
                                          FROM   pbrow pbrow2
                                          WHERE  pbrow2.coid = corow.coid
                                          AND    pbrow2.copos = corow.copos
                                          AND    pbrow2.coseq = corow.coseq
                                          AND    pbrow2.company_id =
                                                 art.company_id
                                          AND    pbrow2.artid = art.artid) =
                                         pbrow.pickdtm THEN
                                        1
                                END
                        END
                      , 0
                    )                       AS order_line_comp
             FROM   pbrow
                    INNER JOIN art
                        ON art.artid = pbrow.artid
                        AND art.company_id = pbrow.company_id
                    INNER JOIN corow
                        ON corow.coid = pbrow.coid
                        AND corow.copos = pbrow.copos
                        AND corow.company_id = art.company_id
             WHERE  art.company_id = '{?Client}'
             AND    TRUNC(pbrow.pickdtm) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
             AND    pbrow.whid = 'PRE1'
             AND    pbrow.shortage = 0
             AND    pbrow.pbtype IN ('C', 'P')
             AND    pbrow.pbrowstat = 4
             /*UNION pbrow & pbrowlog to avoid duplicates*/
             UNION
             SELECT TRUNC(pbrow.pickdtm)    AS pickdtm
                  , art.artid               AS artid
                  , pbrow.pbtype
                  -- , pbrow.pbrowid
                  , pbrow.coid
                  , pbrow.copos
                  , pbrow.coseq
                  , NVL(
                        CASE
                            WHEN (SELECT MAX(pickdtm)
                                  FROM   pbrowlog pbrow2
                                  WHERE  pbrow2.coid = corow.coid
                                  AND    pbrow2.copos = corow.copos
                                  AND    pbrow2.coseq = corow.coseq
                                  AND    pbrow2.company_id = art.company_id
                                  AND    pbrow2.artid = art.artid) =
                                 pbrow.pickdtm THEN
                                1
                        END
                      , 0
                    )                       AS order_line_comp
             FROM   pbrowlog pbrow
                    INNER JOIN art
                        ON art.artid = pbrow.artid
                        AND art.company_id = pbrow.company_id
                    INNER JOIN corowtrc corow
                        ON corow.coid = pbrow.coid
                        AND corow.copos = pbrow.copos
                        AND corow.company_id = art.company_id
             WHERE  art.company_id = '{?Client}'
             AND    pbrow.whid = 'PRE1'
             AND    TRUNC(pbrow.pickdtm) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
         )
        GROUP BY pickdtm)
/* OPEN ROWS PER DATE */
    , OPEN_ROWS_PER_DATE AS (
        SELECT TRUNC(insertdtm)     AS dtm
            , SUM(order_rows)
                OVER(
                    ORDER BY TRUNC(insertdtm)
                    ROWS UNBOUNDED PRECEDING)       AS order_rows
        FROM 
            (SELECT DISTINCT
                TRUNC(cowork.insertdtm) insertdtm
                , COUNT(DISTINCT cowork.coid||cowork.coseq||pbrow.copos) AS order_rows
            FROM cowork
            LEFT JOIN pbrow
                ON pbrow.coid = cowork.coid
                   AND pbrow.coseq = cowork.coseq
                   AND pbrow.company_id = '{?Client}'
                   AND pbrow.pickdtm IS NULL
            WHERE cowork.whid = 'PRE1'
                AND cowork.company_id = '{?Client}'
                AND cowork.costatid < 30
                AND TRUNC(cowork.insertdtm) <= TRUNC({?date_to})
            GROUP BY TRUNC(cowork.insertdtm)))
/* DONE ROWS PER DATE PAL W/O PALLET PICKS */
    , DONE_ROWS_PER_DATE_PAL AS (
        SELECT 
            DTM
            , SUM(CASE_PICK_ROWS_PAL) AS CASE_PICK_ROWS_PAL
            , SUM(CASE_PICK_ROWS_KRT) AS CASE_PICK_ROWS_KRT
            , SUM(CASE_PICK_ROWS_LONGGOODS) AS CASE_PICK_ROWS_LONGGOODS
			, SUM(DECODE(PAL_PICK_ROWS_PAL, 0, 0, pal_picks))          AS PAL_PICK_ROWS_PAL
			, SUM(DECODE(PAL_PICK_ROWS_LONGGOODS, 0, 0, pal_picks))    AS PAL_PICK_ROWS_LONGGOODS
        FROM (
            SELECT
                DISTINCT
                TRUNC(PBROW.PICKDTM) AS DTM
				, CASE
                     WHEN pal_case_pick.wsid IN ('PRA', 'PRP', 'PRX')
						AND	pal_case_pick.pal_picks = 0
						AND pal_case_pick.case_picks != 0 THEN
                         1
                     ELSE
                         0
                 END                     AS case_pick_rows_pal
               , CASE
                     WHEN pal_case_pick.wsid = 'PLM'
						AND pal_case_pick.pal_picks = 0
						AND pal_case_pick.case_picks != 0 THEN
                         1
                     ELSE
                         0
                 END                     AS case_pick_rows_krt
               , CASE
                     WHEN pal_case_pick.wsid IN ('PLO', 'PLP')
						AND pal_case_pick.pal_picks = 0
						AND pal_case_pick.case_picks != 0 THEN
                         1
                     ELSE
                         0
                 END                     AS case_pick_rows_longgoods
               , CASE
                     WHEN pal_case_pick.wsid IN ('PRA', 'PRP', 'PRX')									
						AND             pal_picks != 0 THEN
                        pal_picks
                     ELSE
                         0
                 END                     AS pal_pick_rows_pal
               , CASE
                     WHEN pal_case_pick.wsid IN ('PLO', 'PLP')									
						AND             pal_picks != 0 THEN
                        pal_picks
                     ELSE
                         0
                 END                     AS pal_pick_rows_longgoods
               , pal_case_pick.*						
            FROM PBROW              
            INNER JOIN
                    (SELECT DISTINCT
                        PBROW.COID||PBROW.COSEQ||PBROW.COPOS AS PICK_ROW,
                        PBROW.WSID,
                        COUNT(DISTINCT CASE WHEN PBROW.PBTYPE = 'P' THEN NVL (CAR.CARCARID, CAR.CARID) END) AS PAL_PICKS,
                        SUM(CASE WHEN PBROW.PBTYPE = 'C' THEN 1 ELSE 0 END) AS CASE_PICKS
                    FROM PBROW
                    INNER JOIN PBCAR 
                        ON PBCAR.PBCARID = PBROW.PBCARID
                    INNER JOIN CAR ON PBCAR.CARID = CAR.CARID
                    WHERE PBROW.WHID = 'PRE1'
                        AND PBROW.COMPANY_ID = 'AT-PREFA'
                        AND TRUNC(PBROW.PICKDTM) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
                    GROUP BY PBROW.COID||PBROW.COSEQ||PBROW.COPOS
                        , PBROW.WSID
                    ORDER BY PBROW.COID||PBROW.COSEQ||PBROW.COPOS) PAL_CASE_PICK
                ON PBROW.COID||PBROW.COSEQ||PBROW.COPOS = PAL_CASE_PICK.PICK_ROW							  
			INNER JOIN corow
				ON COROW.COID = PBROW.COID
					AND COROW.COPOS = PBROW.COPOS
                    AND COROW.COMPANY_ID = PBROW.COMPANY_ID
                    AND COROW.COSEQ = PBROW.COSEQ									  
            WHERE PBROW.WHID = 'PRE1'
                AND PBROW.COMPANY_ID = '{?Client}'
                AND TRUNC(pbrow.pickdtm) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            /* check if the orderline is/was completed */
			AND    NVL(
                     CASE
                         WHEN corow.corowstat >= 5 THEN
                             CASE
                                 WHEN (SELECT MAX(pickdtm)
                                       FROM   pbrow pbrow2
                                       WHERE  pbrow2.coid = corow.coid
                                       AND    pbrow2.copos = corow.copos
                                       AND    pbrow2.coseq = corow.coseq
                                       AND    pbrow2.company_id =
                                              pbrow.company_id
                                       AND    pbrow2.artid = pbrow.artid) =
                                      pbrow.pickdtm THEN
                                     1
                             END
                     END
                   , 0
                 ) =
                 1
            UNION ALL
            SELECT
                DISTINCT
                TRUNC(PICKDTM) AS DTM
                , CASE
                     WHEN pal_case_pick.wsid IN ('PRA', 'PRP', 'PRX')
						AND pal_case_pick.pal_picks = 0
						AND pal_case_pick.case_picks != 0 THEN
                         1
                     ELSE
                         0
                 END                        AS case_pick_rows_pal
               , CASE
                     WHEN pal_case_pick.wsid = 'PLM'
						AND pal_case_pick.pal_picks = 0
						AND pal_case_pick.case_picks != 0 THEN
                         1
                     ELSE
                         0
                 END                        AS case_pick_rows_krt
               , CASE
                     WHEN pal_case_pick.wsid IN ('PLO', 'PLP')
						AND pal_case_pick.pal_picks = 0
						AND pal_case_pick.case_picks != 0 THEN
                         1
                     ELSE
                         0
                 END                        AS case_pick_rows_longgoods
               , CASE
                     WHEN pal_case_pick.wsid IN ('PRA', 'PRP', 'PRX')					
						AND pal_case_pick.pal_picks != 0 THEN
                         pal_picks
                     ELSE
                         0
                 END                        AS pal_pick_rows_pal
               , CASE
                     WHEN pal_case_pick.wsid IN ('PLO', 'PLP')												
						AND pal_case_pick.pal_picks != 0 THEN
                         pal_picks
                     ELSE
                         0
                 END                        AS pal_pick_rows_longgoods
               , pal_case_pick.*
            FROM PBROWLOG              					 
            INNER JOIN
                    (SELECT DISTINCT
                        PBROWLOG.COID||PBROWLOG.COSEQ||PBROWLOG.COPOS AS PICK_ROW,
                        PBROWLOG.WSID,
                        COUNT(DISTINCT CASE WHEN PBROWLOG.PBTYPE = 'P' THEN NVL (CAR.CARCARID, CAR.CARID) END) AS PAL_PICKS,
                        SUM(CASE WHEN PBROWLOG.PBTYPE = 'C' THEN 1 ELSE 0 END) AS CASE_PICKS
                    FROM PBROWLOG
                    INNER JOIN PBCARLOG  PBCAR 
                        ON PBCAR.PBCARID = PBROWLOG.PBCARID
                    INNER JOIN CARTRC CAR ON PBROWLOG.CARID = CAR.CARID
                    WHERE PBROWLOG.WHID = 'PRE1'
                        AND TRUNC(PBROWLOG.PICKDTM) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
                        AND PBROWLOG.COMPANY_ID = '{?Client}'                       
                        AND NOT EXISTS (
                            SELECT 1
                            FROM pbrow
                            WHERE pbrowlog.pbrowid = pbrow.pbrowid
                                AND pbrowlog.whid = pbrow.whid
                                AND pbrowlog.company_id = pbrow.company_id 
                            )
                    GROUP BY PBROWLOG.COID||PBROWLOG.COSEQ||PBROWLOG.COPOS
                        , PBROWLOG.WSID
                    ORDER BY PBROWLOG.COID||PBROWLOG.COSEQ||PBROWLOG.COPOS) PAL_CASE_PICK
                ON PBROWLOG.COID||PBROWLOG.COSEQ||PBROWLOG.COPOS = PAL_CASE_PICK.PICK_ROW
			INNER JOIN corowtrc
                     ON corowtrc.coid = pbrowlog.coid
                     AND corowtrc.copos = pbrowlog.copos
                     AND corowtrc.company_id = pbrowlog.company_id
                     AND corowtrc.coseq = pbrowlog.coseq																	
            WHERE PBROWLOG.WHID = 'PRE1'
                AND PBROWLOG.COMPANY_ID = '{?Client}'
                AND TRUNC(pbrowlog.pickdtm) BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
            /* check if the orderline is/was completed */
			AND    NVL(
                     CASE
                         WHEN (SELECT MAX(pickdtm)
                               FROM   pbrowlog pbrow2
                               WHERE  pbrow2.coid = corowtrc.coid
                               AND    pbrow2.copos = corowtrc.copos
                               AND    pbrow2.coseq = corowtrc.coseq
                               AND    pbrow2.company_id = pbrowlog.company_id
                               AND    pbrow2.artid = pbrowlog.artid) =
                              pbrowlog.pickdtm THEN
                             1
                     END
                   , 0
                 ) =
                 1
			)   
        GROUP BY DTM
        ORDER BY DTM DESC)
    , date_table AS (
        SELECT TRUNC(sysdate+1 - ROWNUM)     DTM
            FROM DUAL
        CONNECT BY ROWNUM < 180)    
SELECT date_table.dtm
    , NVL(inbounds.sum_pal, 0)                                  AS rcv_pal
    , NVL(inbounds.sum_krt, 0)                                  AS rcv_krt
    , NVL(inbounds.sum_plg, 0)                                  AS rcv_plg
    , NVL(picks.num_pal, 0)                                     AS picked_rows_pal
    , NVL(picks.num_krt, 0)                                     AS picked_rows_krt
    , NVL(picks.num_longgoods, 0)                               AS picked_rows_longgoods
    , NVL(loading.num_co, 0)                                    AS loaded_co
    , NVL(loading.num_carid, 0)                                 AS loaded_carid
    , NVL(received_orders_per_date.orders, 0)                   AS received_orders_per_date
    , NVL(done_orders_per_date.orders, 0)                       AS done_orders_per_date
    , NVL(open_orders_per_date.orders, 0)                       AS open_orders_per_date
    , NVL(received_rows_per_date.order_rows, 0)                 AS received_rows_per_date
    , NVL(done_rows_per_date.order_rows, 0)                     AS done_rows_per_date
    , NVL(open_rows_per_date.order_rows, 0)                     AS open_rows_per_date
    , NVL(done_rows_per_date_pal.case_pick_rows_pal, 0)         AS case_pick_rows_pal
    , NVL(done_rows_per_date_pal.case_pick_rows_krt, 0)         AS case_pick_rows_krt
    , NVL(done_rows_per_date_pal.case_pick_rows_longgoods, 0)   AS case_pick_rows_longgoods
    , NVL(done_rows_per_date_pal.pal_pick_rows_pal, 0)          AS pal_pick_rows_pal
    , NVL(done_rows_per_date_pal.pal_pick_rows_longgoods, 0)    AS pal_pick_rows_longgoods
    , stock.num_PAL                                             AS stock_pal
    , stock.num_KRT                                             AS stock_krt
    , stock.num_PLG                                             AS stock_plg
    , stock.num_others                                          AS stock_others
FROM date_table
LEFT JOIN inbounds
    ON date_table.dtm = inbounds.enddtm
LEFT JOIN picks
    ON date_table.dtm = picks.pick_dtm
LEFT JOIN loading
    ON date_table.dtm = loading.load_dtm
LEFT JOIN received_orders_per_date 
    ON received_orders_per_date.dtm = date_table.dtm
LEFT JOIN done_orders_per_date
    ON done_orders_per_date.dtm = date_table.dtm
LEFT JOIN open_orders_per_date
    ON open_orders_per_date.dtm = date_table.dtm
LEFT JOIN received_rows_per_date 
    ON received_rows_per_date.dtm = date_table.dtm
LEFT JOIN done_rows_per_date
    ON done_rows_per_date.dtm = date_table.dtm
LEFT JOIN open_rows_per_date
    ON open_rows_per_date.dtm = date_table.dtm
LEFT JOIN done_rows_per_date_pal
    ON done_rows_per_date_pal.dtm = date_table.dtm
CROSS JOIN stock
WHERE date_table.dtm BETWEEN TRUNC({?date_from}) AND TRUNC({?date_to})
ORDER BY 1 DESC