/******************************************************************************
NAME: AT-PREFA_Outbound.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

SELECT cotrc.coid,
         coworklog.insertdtm,
         olacodnl.olacodtxt,
         cotrc.ackshipdate,
         cotrc.shiptopartyid,
         deplog.departure_dtm,
         deplog.departure_id,
         party.name1,
         party.countrycode,
         deplog.loadfinishdtm,
         SUBSTR (coftxtrcdtm.text, 1, 8)     ReqDelDate,
         deplog.dlvrymeth_name,
         SUM (corowtrc.ordqty)
    FROM cotrc
         INNER JOIN deplog ON deplog.departure_id = cotrc.departure_id
         INNER JOIN party
             ON     party.company_id = cotrc.company_id
                AND party.party_id = cotrc.shiptopartyid
                AND party.party_qualifier = cotrc.shiptopartyqualifier
         INNER JOIN olacodnl
             ON     olacodnl.nlangcod = 'ENU'
                AND olacodnl.olaid = 'COSTATID'
                AND olacodnl.olacod = '80'
         INNER JOIN coworklog
             ON     coworklog.empid = 'CusOrdComp'
                AND coworklog.co_log_type = 'OrderCreation'
                AND coworklog.company_id = cotrc.company_id
                AND coworklog.coid = cotrc.coid
                AND coworklog.coseq = cotrc.coseq
         INNER JOIN corowtrc
             ON     corowtrc.coid = cotrc.coid
                AND corowtrc.company_id = cotrc.company_id
                AND corowtrc.coseq = cotrc.coseq
         LEFT JOIN coftxtrc coftxtrcdtm
             ON     coftxtrcdtm.company_id = cotrc.company_id
                AND coftxtrcdtm.coid = cotrc.coid
                AND coftxtrcdtm.coseq = cotrc.coseq
                AND coftxtrcdtm.ftx_qualifier = 'DTM'
   WHERE cotrc.company_id = 'AT-PREFA'
--AND deplog.departure_dtm > {?datefrom}
--AND deplog.departure_dtm < {?dateto}
--AND coworklog.insertdtm > {?insdatefr}
--AND coworklog.insertdtm < {?insdateto}
GROUP BY cotrc.coid,
         coworklog.insertdtm,
         olacodnl.olacodtxt,
         cotrc.ackshipdate,
         cotrc.shiptopartyid,
         deplog.departure_dtm,
         deplog.departure_id,
         party.name1,
         party.countrycode,
         deplog.loadfinishdtm,
         SUBSTR (coftxtrcdtm.text, 1, 8),
         deplog.dlvrymeth_name
UNION
  SELECT cowork.coid,
         coworklog.insertdtm,
         olacodnl.olacodtxt,
         cowork.ackshipdate,
         cowork.shiptopartyid,
         dep.departure_dtm,
         dep.departure_id,
         party.name1,
         party.countrycode,
         dep.loadfinishdtm,
         SUBSTR (coftxdtm.text, 1, 8)     ReqDelDate,
         dep.dlvrymeth_name,
         SUM (corow.ordqty)
    FROM cowork
         INNER JOIN dep ON dep.departure_id = cowork.departure_id
         INNER JOIN party
             ON     party.company_id = cowork.company_id
                AND party.party_id = cowork.shiptopartyid
                AND party.party_qualifier = cowork.shiptopartyqualifier
         INNER JOIN olacodnl
             ON     olacodnl.nlangcod = 'ENU'
                AND olacodnl.olaid = 'COSTATID'
                AND olacodnl.olacod = cowork.costatid
         INNER JOIN coworklog
             ON     coworklog.empid = 'CusOrdComp'
                AND coworklog.co_log_type = 'OrderCreation'
                AND coworklog.company_id = cowork.company_id
                AND coworklog.coid = cowork.coid
                AND coworklog.coseq = cowork.coseq
         INNER JOIN corow
             ON     corow.coid = cowork.coid
                AND corow.company_id = cowork.company_id
                AND corow.coseq = cowork.coseq
         LEFT JOIN coftx coftxdtm
             ON     coftxdtm.company_id = cowork.company_id
                AND coftxdtm.coid = cowork.coid
                AND coftxdtm.coseq = cowork.coseq
                AND coftxdtm.ftx_qualifier = 'DTM'
   WHERE cowork.company_id = 'AT-PREFA'
--AND dep.departure_dtm > {?datefrom}
--AND dep.departure_dtm < {?dateto}
--AND coworklog.insertdtm > {?insdatefr}
--AND coworklog.insertdtm < {?insdateto}
GROUP BY cowork.coid,
         coworklog.insertdtm,
         olacodnl.olacodtxt,
         cowork.ackshipdate,
         cowork.shiptopartyid,
         dep.departure_dtm,
         dep.departure_id,
         party.name1,
         party.countrycode,
         dep.loadfinishdtm,
         SUBSTR (coftxdtm.text, 1, 8),
         dep.dlvrymeth_name
