/******************************************************************************
NAME: AT-PREFA_Too_low_balance_pickarea.rpt
SUBREPORT: -
DESC: 

   
REVISIONS:

Ver 		Date      		Player       SN/JIRA 		Description
-------------------------------------------------------------------------------
1.0 		xx.xx.xx 		                            created

******************************************************************************/

select artid, sum(storqty), sum(ord) from( 
  
  SELECT ite.artid, ite.storqty, 0 ord
    FROM ite
         INNER JOIN car ON car.carid = ite.carid                
   WHERE car.whid = 'PRE1' AND car.company_id = 'AT-PREFA' and car.wsid = 'PRP'
union all
select pbrow.artid, 0 storqty, pbrow.ordqty ord from pbrow where pbrow.company_id = 'AT-PREFA' and pbrow.pbrowstat < 4 and pzid = 'PPR')
group by artid
having sum(ord)>sum(storqty)